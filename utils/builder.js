"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.regexp.exec.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.splice.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _utils = require("./utils");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
var _default = {
  /**
   * Appends a number to a component.key to keep it unique
   *
   * @param {Object} form
   *   The components parent form.
   * @param {Object} component
   *   The component to uniquify
   * @param prefix
   * @param emptyRelations
   * @param transform
   * @param idMapping
   */
  uniquify: function uniquify(form, component, prefix, emptyRelations, transform, idMapping) {
    var formKeys = {};
    var oldToNew = {};
    var deletionArray = [];
    var editgridPresent = false;
    if (typeof prefix !== 'string') {
      prefix = '';
    }
    (0, _utils.eachComponent)(form.components, function (comp) {
      formKeys[comp.key] = true;
    }, true);

    // Recurse into all child components.
    // eslint-disable-next-line max-statements
    (0, _utils.eachComponent)([component], function (component, newPath, components, index) {
      // Skip key uniquification if this component doesn't have a key.
      if (!component.key) {
        return;
      }
      if (emptyRelations) {
        if (!transform) {
          delete component.determinantIds;
          delete component.formDeterminantId;
          delete component.effectsIds;
          delete component.behaviourId;
          delete component.tabItemIds;
          delete component.componentTabId;
        }
        delete component.formulaRowIds;
        delete component.actionRowIds;
        delete component.validationRowIds;
        delete component.blacklistRowIds;
        delete component.componentFormulaId;
        delete component.componentActionId;
        delete component.componentValidationId;
        delete component.numberOfConditions;
        delete component.numberOfRelatedDeterminants;
        delete component.numberOfFormulas;
        delete component.numberOfActions;
        delete component.numberOfRegistrations;
        delete component.copyValueFrom;
        delete component.registrations;
        delete component.linkedWithRequirement;
        delete component.linkingRequirement;
        delete component.linkedWithResult;
        delete component.linkingResult;
        delete component.serviceId;
        delete component.dynamicValidations;
      }
      if (transform) {
        var _component$html, _component$html2;
        if (component.type === 'button' || component.type === 'file') {
          // delete the component object
          deletionArray.push({
            array: components,
            index: index
          });
          return;
        }
        if (component.type === 'content' && !((_component$html = component.html) !== null && _component$html !== void 0 && _component$html.includes('{{') && (_component$html2 = component.html) !== null && _component$html2 !== void 0 && _component$html2.includes('}}'))) {
          // delete the component object
          deletionArray.push({
            array: components,
            index: index
          });
          return;
        }
        if (component.type === 'catalog' || component.type === 'select') {
          component.copyValueFrom = [component.key];
          component.validate = {};
          component.dataSrc = 'none';
          component.template = '<span className="value">{{t(item.value)}}</span>';
          delete component.catalog;
          delete component.catalogueDefaultValue;
          delete component.defaultValue;
          delete component.parentCatalogField;
        } else if (!(0, _utils.isExtendedLayoutComponent)(component)) {
          component.copyValueFrom = [component.key];
          component.validate = {};
        } else if (component.type === 'editgrid') {
          component.copyValueFrom = [component.key];
          component.disableAddingRows = true;
          component.disableRemovingRows = true;
          component.validate = {};
          editgridPresent = true;
        } else if (component.type === 'datagrid') {
          component.copyValueFrom = [component.key];
          component.disableAddingRows = true;
          component.disableRemovingRows = true;
          component.validate = {};
        } else if (component.type === 'tabs') {
          component.type = 'panel';
        } else if ((!component.type || component.type === 'tab') && Array.isArray(component.components)) {
          if (component.condition > 0 && idMapping) {
            component.formDeterminantId = idMapping[component.id].id;
            component.determinantIds = _toConsumableArray(idMapping[component.id].determinantIds);
          }
          component.type = 'panel';
          component.title = component.label;
          delete component.label;
          delete component.condition;
        }
        if (component.type === 'panel') {
          component.collapsibleDS = false;
          component.collapsedDS = false;
          component.collapsed = false;
          delete component.tabItemIds;
          delete component.componentTabId;
        }
      }
      var oldKey = component.key;
      component.key = _lodash.default.camelCase(prefix.concat(component.label || component.placeholder || component.type));
      var newKey = (0, _utils.uniqueKey)(formKeys, component.key, prefix);
      if (oldKey !== newKey) {
        oldToNew[oldKey] = newKey;
        component.key = newKey;
        formKeys[newKey] = true;
      } else {
        component.key = oldKey;
      }
    }, true);
    deletionArray.reverse().forEach(function (obj) {
      obj.array.splice(obj.index, 1);
    });
    if (editgridPresent) {
      (0, _utils.eachComponent)([component], function (comp) {
        var _comp$fieldsShownInGr, _comp$totalColumns;
        if (comp.type !== 'editgrid') return;
        var fieldsShowInGridArray = [];
        var totalColumnsArray = [];
        (_comp$fieldsShownInGr = comp.fieldsShownInGrid) === null || _comp$fieldsShownInGr === void 0 ? void 0 : _comp$fieldsShownInGr.forEach(function (field) {
          fieldsShowInGridArray.push(oldToNew[field]);
        });
        comp.fieldsShownInGrid = fieldsShowInGridArray;
        (_comp$totalColumns = comp.totalColumns) === null || _comp$totalColumns === void 0 ? void 0 : _comp$totalColumns.forEach(function (field) {
          totalColumnsArray.push(oldToNew[field]);
        });
        comp.totalColumns = totalColumnsArray;
        if (comp.percentageColumn) comp.percentageColumn = oldToNew[comp.percentageColumn];
      }, true);
    }
    return oldToNew;
  },
  additionalShortcuts: {
    button: ['Enter', 'Esc']
  },
  extractNecessaryProperties: function extractNecessaryProperties(obj) {
    var _this = this;
    var necessaryProps = ['components', 'columns', 'rows', 'type', 'key', 'formDeterminantId'];
    if (Array.isArray(obj)) {
      return obj.map(function (item) {
        return _this.extractNecessaryProperties(item);
      });
    } else if (_typeof(obj) === 'object' && obj !== null) {
      var newObj = {};
      for (var prop in obj) {
        if (necessaryProps.includes(prop)) {
          newObj[prop] = this.extractNecessaryProperties(obj[prop]);
        }
      }
      return newObj;
    } else {
      return obj;
    }
  },
  getAlphaShortcuts: function getAlphaShortcuts() {
    return _lodash.default.range('A'.charCodeAt(), 'Z'.charCodeAt() + 1).map(function (charCode) {
      return String.fromCharCode(charCode);
    });
  },
  getAdditionalShortcuts: function getAdditionalShortcuts(type) {
    return this.additionalShortcuts[type] || [];
  },
  getBindedShortcuts: function getBindedShortcuts(components, input) {
    var result = [];
    (0, _utils.eachComponent)(components, function (component) {
      if (component === input) {
        return;
      }
      if (component.shortcut) {
        result.push(component.shortcut);
      }
      if (component.values) {
        component.values.forEach(function (value) {
          if (value.shortcut) {
            result.push(value.shortcut);
          }
        });
      }
    }, true);
    return result;
  },
  getAvailableShortcuts: function getAvailableShortcuts(form, component) {
    if (!component) {
      return [];
    }
    return [''].concat(_lodash.default.difference(this.getAlphaShortcuts().concat(this.getAdditionalShortcuts(component.type)), this.getBindedShortcuts(form.components, component)));
  }
};
exports.default = _default;