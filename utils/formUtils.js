"use strict";

require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.symbol.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.applyFormChanges = applyFormChanges;
exports.eachComponent = eachComponent;
exports.escapeRegExCharacters = escapeRegExCharacters;
exports.findComponent = findComponent;
exports.findComponents = findComponents;
exports.flattenComponents = flattenComponents;
exports.formatAsCurrency = formatAsCurrency;
exports.generateFormChange = generateFormChange;
exports.getComponent = getComponent;
exports.getStrings = getStrings;
exports.getValue = getValue;
exports.hasCondition = hasCondition;
exports.isExtendedLayoutComponent = isExtendedLayoutComponent;
exports.isLayoutComponent = isLayoutComponent;
exports.isSubset = isSubset;
exports.matchComponent = matchComponent;
exports.parseFloatExt = parseFloatExt;
exports.removeComponent = removeComponent;
exports.searchComponents = searchComponents;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.match.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.array.splice.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.string.includes.js");
var _get = _interopRequireDefault(require("lodash/get"));
var _set = _interopRequireDefault(require("lodash/set"));
var _has = _interopRequireDefault(require("lodash/has"));
var _clone = _interopRequireDefault(require("lodash/clone"));
var _forOwn = _interopRequireDefault(require("lodash/forOwn"));
var _isString = _interopRequireDefault(require("lodash/isString"));
var _isNaN = _interopRequireDefault(require("lodash/isNaN"));
var _isNil = _interopRequireDefault(require("lodash/isNil"));
var _isPlainObject = _interopRequireDefault(require("lodash/isPlainObject"));
var _round = _interopRequireDefault(require("lodash/round"));
var _chunk = _interopRequireDefault(require("lodash/chunk"));
var _pad = _interopRequireDefault(require("lodash/pad"));
var _findIndex = _interopRequireDefault(require("lodash/findIndex"));
var _fastJsonPatch = _interopRequireDefault(require("fast-json-patch"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
/**
 * Determine if a component is a layout component or not.
 *
 * @param {Object} component
 *   The component to check.
 *
 * @returns {Boolean}
 *   Whether or not the component is a layout component.
 */
function isLayoutComponent(component) {
  return Boolean(component.columns && Array.isArray(component.columns) || component.rows && Array.isArray(component.rows) || component.components && Array.isArray(component.components));
}
function isExtendedLayoutComponent(component) {
  return Boolean(isLayoutComponent(component) || component.type === 'content');
}

/**
 * Iterate through each component within a form.
 *
 * @param {Object} components
 *   The components to iterate.
 * @param {Function} fn
 *   The iteration function to invoke for each component.
 * @param {Boolean} includeAll
 *   Whether or not to include layout components.
 * @param {String} path
 *   The current data path of the element. Example: data.user.firstName
 * @param {Object} parent
 *   The parent object.
 */
function eachComponent(components, fn, includeAll, path, parent) {
  if (!components) return;
  path = path || '';
  components.forEach(function (component, index) {
    if (!component) {
      return;
    }
    var hasColumns = component.columns && Array.isArray(component.columns);
    var hasRows = component.rows && Array.isArray(component.rows) && component.rows[0] && Array.isArray(component.rows[0]);
    var hasComps = component.components && Array.isArray(component.components);
    var noRecurse = false;
    var newPath = component.key ? path ? "".concat(path, ".").concat(component.key) : component.key : '';

    // Keep track of parent references.
    if (parent) {
      // Ensure we don't create infinite JSON structures.
      component.parent = (0, _clone.default)(parent);
      delete component.parent.components;
      delete component.parent.componentMap;
      delete component.parent.columns;
      delete component.parent.rows;
    }
    if (includeAll || component.tree || !hasColumns && !hasRows && !hasComps) {
      noRecurse = fn(component, newPath, components, index);
    }
    var subPath = function subPath() {
      if (component.key && !['panel', 'table', 'well', 'columns', 'fieldset', 'tabs', 'form'].includes(component.type) && (['datagrid', 'container', 'editgrid'].includes(component.type) || component.tree)) {
        return newPath;
      } else if (component.key && component.type === 'form') {
        return "".concat(newPath, ".data");
      }
      return path;
    };
    if (!noRecurse) {
      if (hasColumns) {
        component.columns.forEach(function (column) {
          return eachComponent(column.components, fn, includeAll, subPath(), parent ? component : null);
        });
      } else if (hasRows) {
        component.rows.forEach(function (row) {
          if (Array.isArray(row)) {
            row.forEach(function (column) {
              return eachComponent(column.components, fn, includeAll, subPath(), parent ? component : null);
            });
          }
        });
      } else if (hasComps) {
        eachComponent(component.components, fn, includeAll, subPath(), parent ? component : null);
      }
    }
  });
}

/**
 * Matches if a component matches the query.
 *
 * @param component
 * @param query
 * @return {boolean}
 */
function matchComponent(component, query) {
  if ((0, _isString.default)(query)) {
    return component.key === query;
  } else {
    var matches = false;
    (0, _forOwn.default)(query, function (value, key) {
      matches = (0, _get.default)(component, key) === value;
      if (!matches) {
        return false;
      }
    });
    return matches;
  }
}

/**
 * Get a component by its key
 *
 * @param {Object} components
 *   The components to iterate.
 * @param {String|Object} key
 *   The key of the component to get, or a query of the component to search.
 *
 * @returns {Object}
 *   The component that matches the given key, or undefined if not found.
 */
function getComponent(components, key, includeAll) {
  var result;
  eachComponent(components, function (component, path) {
    if (path === key || component.key === key) {
      component.path = path;
      result = component;
      return true;
    }
  }, includeAll);
  return result;
}

/**
 * Finds a component provided a query of properties of that component.
 *
 * @param components
 * @param query
 * @return {*}
 */
function searchComponents(components, query) {
  var results = [];
  eachComponent(components, function (component, path) {
    if (matchComponent(component, query)) {
      component.path = path;
      results.push(component);
    }
  }, true);
  return results;
}

/**
 * Deprecated version of findComponents. Renamed to searchComponents.
 *
 * @param components
 * @param query
 * @returns {*}
 */
function findComponents(components, query) {
  console.warn('formio.js/utils findComponents is deprecated. Use searchComponents instead.');
  return searchComponents(components, query);
}
var possibleFind = null;
var possiblePath = [];
var unknownCounter = {};
var checkComponent = function checkComponent(component, key, path) {
  // Search for components without a key as well.
  if (!component.key) {
    if (!unknownCounter.hasOwnProperty(component.type)) {
      unknownCounter[component.type] = 0;
    }
    unknownCounter[component.type]++;
    if (key === component.type + unknownCounter[component.type]) {
      possibleFind = component;
      possiblePath = (0, _clone.default)(path);
    }
  } else if (possibleFind && component.key === possibleFind.key) {
    var nextCount = component.key.match(/([0-9]+)$/g);
    if (nextCount) {
      unknownCounter[component.type] = parseInt(nextCount.pop(), 10);
      possibleFind = null;
      possiblePath = [];
    }
  }
  if (component.key === key) {
    return true;
  }
  return false;
};

/**
 * This function will find a component in a form and return the component AND THE PATH to the component in the form.
 *
 * @param components
 * @param key
 * @param fn
 * @param path
 * @returns boolean - If the component was found.
 */
function findComponent(components, key, path, fn) {
  if (!components || !key) {
    return false;
  }
  if (typeof path === 'function') {
    fn = path;
    path = [];
  }
  path = path || [];
  if (!path.length) {
    // Reset search params.
    possibleFind = null;
    possiblePath = [];
    unknownCounter = {};
  }
  var found = false;
  components.forEach(function (component, index) {
    var newPath = path.slice();
    newPath.push(index);
    if (!component) return;
    if (component.hasOwnProperty('columns') && Array.isArray(component.columns)) {
      newPath.push('columns');
      component.columns.forEach(function (column, index) {
        var colPath = newPath.slice();
        colPath.push(index);
        column.type = 'column';
        if (checkComponent(column, key, colPath)) {
          found = true;
          fn(column, colPath);
        } else if (findComponent(column.components, key, colPath.concat(['components']), fn)) {
          found = true;
        }
      });
    }
    if (component.hasOwnProperty('rows') && Array.isArray(component.rows)) {
      newPath.push('rows');
      component.rows.forEach(function (row, index) {
        var rowPath = newPath.slice();
        rowPath.push(index);
        row.forEach(function (column, index) {
          var colPath = rowPath.slice();
          colPath.push(index);
          column.type = 'cell';
          if (checkComponent(column, key, colPath)) {
            found = true;
            fn(column, colPath);
          } else if (findComponent(column.components, key, colPath.concat(['components']), fn)) {
            found = true;
          }
        });
      });
    }
    if (component.hasOwnProperty('components') && Array.isArray(component.components) && findComponent(component.components, key, newPath.concat(['components']), fn)) {
      found = true;
    }

    // Check this component.
    if (checkComponent(component, key, newPath)) {
      found = true;
      fn(component, newPath);
    }
  });

  // If the component was not found BUT there was a possibility then return it.
  if (!path.length && !found && possibleFind) {
    found = true;
    fn(possibleFind, possiblePath);
  }

  // Return if this if found.
  return found;
}

/**
 * Remove a component by path.
 *
 * @param components
 * @param path
 */
function removeComponent(components, path) {
  // Using _.unset() leave a null value. Use Array splice instead.
  var index = path.pop();
  if (path.length !== 0) {
    components = (0, _get.default)(components, path);
  }
  components.splice(index, 1);
}
function generateFormChange(type, data) {
  var change = null;
  var schema = data.schema;
  switch (type) {
    case 'add':
      change = {
        op: 'add',
        key: schema.key,
        container: data.parent.key,
        index: (0, _findIndex.default)(data.parent.components, {
          id: data.id
        }),
        component: schema
      };
      break;
    case 'edit':
      change = {
        op: 'edit',
        key: schema.key,
        patches: _fastJsonPatch.default.compare(data.originalComponent, schema)
      };

      // Don't save if nothing changed.
      if (!change.patches.length) {
        change = null;
      }
      break;
    case 'remove':
      change = {
        op: 'remove',
        key: schema.key
      };
      break;
  }
  return change;
}

// Get the parent component provided a component key.
var getParent = function getParent(form, key, fn) {
  if (!findComponent(form.components, key, null, fn)) {
    // Return the root form if no parent is found so it will add the component to the root form.
    fn(form);
  }
};
function applyFormChanges(form, changes) {
  var failed = [];
  changes.forEach(function (change) {
    var found = false;
    switch (change.op) {
      case 'add':
        var newComponent = change.component;
        getParent(form, change.container, function (parent) {
          // A move will first run an add so remove any existing components with matching key before inserting.
          findComponent(form.components, change.key, null, function (component, path) {
            // If found, use the existing component. (If someone else edited it, the changes would be here)
            newComponent = component;
            removeComponent(form.components, path);
          });
          found = true;
          parent.components.splice(change.index, 0, newComponent);
        });
        break;
      case 'remove':
        findComponent(form.components, change.key, null, function (component, path) {
          found = true;
          removeComponent(form.components, path);
        });
        break;
      case 'edit':
        findComponent(form.components, change.key, null, function (component, path) {
          found = true;
          try {
            (0, _set.default)(form.components, path, _fastJsonPatch.default.applyPatch(component, change.patches).newDocument);
          } catch (err) {
            failed.push(change);
          }
        });
        break;
      case 'move':
        break;
    }
    if (!found) {
      failed.push(change);
    }
  });
  return {
    form: form,
    failed: failed
  };
}

/**
 * Flatten the form components for data manipulation.
 *
 * @param {Object} components
 *   The components to iterate.
 * @param {Boolean} includeAll
 *   Whether or not to include layout components.
 *
 * @returns {Object}
 *   The flattened components map.
 */
function flattenComponents(components, includeAll) {
  var flattened = {};
  eachComponent(components, function (component, path) {
    flattened[path] = component;
  }, includeAll);
  return flattened;
}

/**
 * Returns if this component has a conditional statement.
 *
 * @param component - The component JSON schema.
 *
 * @returns {boolean} - TRUE - This component has a conditional, FALSE - No conditional provided.
 */
function hasCondition(component) {
  return Boolean(component.customConditional || component.conditional && component.conditional.when || component.conditional && component.conditional.json);
}

/**
 * Extension of standard #parseFloat(value) function, that also clears input string.
 *
 * @param {any} value
 *   The value to parse.
 *
 * @returns {Number}
 *   Parsed value.
 */
function parseFloatExt(value) {
  return parseFloat((0, _isString.default)(value) ? value.replace(/[^\de.+-]/gi, '') : value);
}

/**
 * Formats provided value in way how Currency component uses it.
 *
 * @param {any} value
 *   The value to format.
 *
 * @returns {String}
 *   Value formatted for Currency component.
 */
function formatAsCurrency(value) {
  var parsedValue = parseFloatExt(value);
  if ((0, _isNaN.default)(parsedValue)) {
    return '';
  }
  var parts = (0, _round.default)(parsedValue, 2).toString().split('.');
  parts[0] = (0, _chunk.default)(Array.from(parts[0]).reverse(), 3).reverse().map(function (part) {
    return part.reverse().join('');
  }).join(',');
  parts[1] = (0, _pad.default)(parts[1], 2, '0');
  return parts.join('.');
}

/**
 * Escapes RegEx characters in provided String value.
 *
 * @param {String} value
 *   String for escaping RegEx characters.
 * @returns {string}
 *   String with escaped RegEx characters.
 */
function escapeRegExCharacters(value) {
  return value.replace(/[-[\]/{}()*+?.\\^$|]/g, '\\$&');
}

/**
 * Get the value for a component key, in the given submission.
 *
 * @param {Object} submission
 *   A submission object to search.
 * @param {String} key
 *   A for components API key to search for.
 */
function getValue(submission, key) {
  var search = function search(data) {
    if ((0, _isPlainObject.default)(data)) {
      if ((0, _has.default)(data, key)) {
        return data[key];
      }
      var value = null;
      (0, _forOwn.default)(data, function (prop) {
        var result = search(prop);
        if (!(0, _isNil.default)(result)) {
          value = result;
          return false;
        }
      });
      return value;
    } else {
      return null;
    }
  };
  return search(submission.data);
}

/**
 * Iterate over all components in a form and get string values for translation.
 * @param form
 */
function getStrings(form) {
  var _this = this;
  var properties = ['label', 'title', 'legend', 'tooltip', 'description', 'placeholder', 'prefix', 'suffix', 'errorLabel', 'content', 'html'];
  var strings = [];
  eachComponent(form.components, function (component) {
    properties.forEach(function (property) {
      if (component.hasOwnProperty(property) && component[property]) {
        strings.push({
          key: component.key,
          type: component.type,
          property: property,
          string: component[property]
        });
      }
    });
    if ((!component.dataSrc || component.dataSrc === 'values') && component.hasOwnProperty('values') && Array.isArray(component.values) && component.values.length) {
      component.values.forEach(function (value, index) {
        strings.push({
          key: component.key,
          property: "value[".concat(index, "].label"),
          string: component.values[index].label
        });
      });
    }

    // Hard coded values from Day component
    if (component.type === 'day') {
      ['day', 'month', 'year', 'Day', 'Month', 'Year', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'].forEach(function (string) {
        strings.push({
          key: component.key,
          property: 'day',
          string: string
        });
      });
      if (component.fields.day.placeholder) {
        strings.push({
          key: component.key,
          property: 'fields.day.placeholder',
          string: component.fields.day.placeholder
        });
      }
      if (component.fields.month.placeholder) {
        strings.push({
          key: component.key,
          property: 'fields.month.placeholder',
          string: component.fields.month.placeholder
        });
      }
      if (component.fields.year.placeholder) {
        strings.push({
          key: component.key,
          property: 'fields.year.placeholder',
          string: component.fields.year.placeholder
        });
      }
    }
    if (component.type === 'editgrid') {
      var string = _this.component.addAnother || 'Add Another';
      if (component.addAnother) {
        strings.push({
          key: component.key,
          property: 'addAnother',
          string: string
        });
      }
    }
    if (component.type === 'select') {
      ['loading...', 'Type to search'].forEach(function (string) {
        strings.push({
          key: component.key,
          property: 'select',
          string: string
        });
      });
    }
  }, true);
  return strings;
}
function isSubset(obj1, obj2) {
  if (obj1 === obj2) return true;
  if (_typeof(obj2) !== 'object' || obj2 === null || _typeof(obj1) !== 'object' || obj1 === null) {
    return false;
  }
  var keys2 = Object.keys(obj2);
  for (var _i = 0, _keys = keys2; _i < _keys.length; _i++) {
    var key = _keys[_i];
    if (!Object.keys(obj1).includes(key)) return false;
    if (Array.isArray(obj2[key]) && Array.isArray(obj1[key])) {
      if (obj2[key].length !== obj1[key].length) return false;
      for (var i = 0; i < obj2[key].length; i++) {
        if (!isSubset(obj1[key][i], obj2[key][i])) return false;
      }
    } else {
      if (!isSubset(obj1[key], obj2[key])) return false;
    }
  }
  return true;
}