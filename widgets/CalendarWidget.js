"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.reflect.set.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.string.match.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.string.split.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _flatpickr = _interopRequireDefault(require("flatpickr"));
var _InputWidget2 = _interopRequireDefault(require("./InputWidget"));
var _utils = require("../utils/utils");
var _moment = _interopRequireDefault(require("moment"));
var _lodash = _interopRequireDefault(require("lodash"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }
function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new TypeError('failed to set property'); } return value; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var DEFAULT_FORMAT = 'dd-MM-yyyy hh:mm a';
var ISO_8601_FORMAT = 'dd-MM-yyyyTHH:mm:ssZ';
var CalendarWidget = /*#__PURE__*/function (_InputWidget) {
  _inherits(CalendarWidget, _InputWidget);
  var _super = _createSuper(CalendarWidget);
  /* eslint-enable camelcase */

  function CalendarWidget(settings, component) {
    var _this;
    _classCallCheck(this, CalendarWidget);
    _this = _super.call(this, settings, component);
    // Change the format to map to the settings.
    if (_this.settings.noCalendar) {
      _this.settings.format = _this.settings.format.replace(/dd-MM-yyyy /g, '');
    }
    if (!_this.settings.enableTime) {
      _this.settings.format = _this.settings.format.replace(/ hh:mm a$/g, '');
    } else if (_this.settings.time_24hr) {
      _this.settings.format = _this.settings.format.replace(/hh:mm a$/g, 'HH:mm');
    }
    _this.component.suffix = true;
    return _this;
  }

  /**
   * Load the timezones.
   *
   * @return {boolean} TRUE if the zones are loading, FALSE otherwise.
   */
  _createClass(CalendarWidget, [{
    key: "loadZones",
    value: function loadZones() {
      var _this2 = this;
      var timezone = this.timezone;
      if (!(0, _utils.zonesLoaded)() && (0, _utils.shouldLoadZones)(timezone)) {
        (0, _utils.loadZones)(timezone).then(function () {
          return _this2.emit('redraw');
        });

        // Return zones are loading.
        return true;
      }

      // Zones are already loaded.
      return false;
    }
  }, {
    key: "attach",
    value: function attach(input) {
      var _this3 = this;
      _get(_getPrototypeOf(CalendarWidget.prototype), "attach", this).call(this, input);
      if (input && !input.getAttribute('placeholder')) {
        input.setAttribute('placeholder', this.settings.format);
      }
      var dateFormatInfo = (0, _utils.getLocaleDateFormatInfo)(this.settings.language);
      this.defaultFormat = {
        date: dateFormatInfo.dayFirst ? 'd/m/Y ' : 'm/d/Y ',
        time: 'h:i K'
      };
      this.closedOn = 0;
      this.valueFormat = this.settings.dateFormat || ISO_8601_FORMAT;
      this.valueMomentFormat = (0, _utils.convertFormatToMoment)(this.valueFormat);
      this.settings.minDate = (0, _utils.getDateSetting)(this.settings.minDate);
      this.settings.maxDate = (0, _utils.getDateSetting)(this.settings.maxDate);
      this.settings.defaultDate = (0, _utils.getDateSetting)(this.settings.defaultDate);
      this.settings.altFormat = (0, _utils.convertFormatToFlatpickr)(this.settings.format);
      this.settings.dateFormat = (0, _utils.convertFormatToFlatpickr)(this.settings.dateFormat);
      this.settings.onChange = function () {
        return _this3.emit('update');
      };
      this.settings.onClose = function () {
        _this3.closedOn = Date.now();
        _this3.emit('blur');
      };

      // Removes console errors from Flatpickr.
      this.settings.errorHandler = function () {
        return null;
      };

      // Extension of the parseDate method for validating input data.
      this.settings.parseDate = function (inputDate, format) {
        _this3.enteredDate = inputDate;
        if (_this3.calendar) {
          _this3.calendar.clear();
        }

        // Check for validation errors.
        if (_this3.component.widget.checkDataValidity()) {
          // Solving the problem with parsing dates with MMM or MMMM format.
          if (!inputDate.match(/[a-z]{3,}/gi)) {
            if (format.indexOf('M') !== -1) {
              format = format.replace('M', 'm');
            } else if (format.indexOf('F') !== -1) {
              format = format.replace('F', 'm');
            }
          } else {
            var match = inputDate.match(/([a-z]{3})/gi);
            inputDate = _lodash.default.replace(inputDate, match, _lodash.default.capitalize(match));
          }
          return _flatpickr.default.parseDate(inputDate, format);
        }
        if (_this3.calendar) {
          _this3.calendar.close();
        }
        return undefined;
      };
      this.settings.formatDate = function (date, format) {
        // Only format this if this is the altFormat and the form is readOnly.
        if (_this3.settings.readOnly && format === _this3.settings.altFormat) {
          if (_this3.settings.saveAs === 'text' || _this3.loadZones()) {
            return _flatpickr.default.formatDate(date, format);
          }
          return (0, _utils.formatOffset)(_flatpickr.default.formatDate.bind(_flatpickr.default), date, format, _this3.timezone);
        }
        return _flatpickr.default.formatDate(date, format);
      };
      if (this._input) {
        // Create a new flatpickr.
        this.calendar = new _flatpickr.default(this._input, this.settings);

        // Enforce the input mask of the format.
        this.setInputMask(this.calendar._input, (0, _utils.convertFormatToMask)(this.settings.format));

        // Make sure we commit the value after a blur event occurs.
        this.addEventListener(this.calendar._input, 'blur', function (event) {
          var activeElement = _this3.settings.shadowRoot ? _this3.settings.shadowRoot.activeElement : document.activeElement;
          var relatedTarget = event.relatedTarget ? event.relatedTarget : activeElement;
          if (!(relatedTarget !== null && relatedTarget !== void 0 && relatedTarget.className.split(/\s+/).includes('flatpickr-day'))) {
            var inputValue = _this3.calendar.input.value;
            var dateValue = inputValue ? (0, _moment.default)(_this3.calendar.input.value, (0, _utils.convertFormatToMoment)(_this3.valueFormat)).toDate() : inputValue;
            _this3.calendar.setDate(dateValue, true, _this3.settings.altFormat);
          }
        });

        // Makes it possible to enter the month as text or digits in the same time.
        if (this.settings.format.match(/\bM{3}\b/gi)) {
          this.addEventListener(this.calendar._input, 'keyup', function (e) {
            var format = _this3.settings.format;
            var value = e.target.value;
            if (value && value[format.indexOf('M')].match(/\d/)) {
              format = format.replace('MMM', 'MM');
            } else if (value && value[format.indexOf('M')].match(/[a-z]/i)) {
              format = format.replace('MMM', 'e');
            }
            if (_this3.inputMasks[0]) {
              _this3.inputMasks[0].destroy();
              _this3.inputMasks = [];
            }
            _this3.setInputMask(_this3.calendar._input, (0, _utils.convertFormatToMask)(format));
          });
        }
      }
    }
  }, {
    key: "timezone",
    get: function get() {
      if (this.settings.timezone) {
        return this.settings.timezone;
      }
      if (this.settings.displayInTimezone === 'submission' && this.settings.submissionTimezone) {
        return this.settings.submissionTimezone;
      }
      if (this.settings.displayInTimezone === 'utc') {
        return 'UTC';
      }

      // Return current timezone if none are provided.
      return (0, _utils.currentTimezone)();
    }
  }, {
    key: "defaultSettings",
    get: function get() {
      return CalendarWidget.defaultSettings;
    }
  }, {
    key: "addSuffix",
    value: function addSuffix(container) {
      var _this4 = this;
      var suffix = this.ce('span', {
        class: 'input-group-addon input-group-append',
        style: 'cursor: pointer'
      });
      suffix.appendChild(this.ce('span', {
        class: 'input-group-text'
      }, this.getIcon(this.settings.enableDate ? 'calendar' : 'time')));
      this.addEventListener(suffix, 'click', function () {
        if (_this4.calendar && !_this4.calendar.isOpen && Date.now() - _this4.closedOn > 200) {
          _this4.calendar.open();
        }
      });
      container.appendChild(suffix);
      return suffix;
    }
  }, {
    key: "disabled",
    set: function set(disabled) {
      _set(_getPrototypeOf(CalendarWidget.prototype), "disabled", disabled, this, true);
      if (this.calendar) {
        if (disabled) {
          this.calendar._input.setAttribute('disabled', 'disabled');
        } else {
          this.calendar._input.removeAttribute('disabled');
        }
        this.calendar.close();
        this.calendar.redraw();
      }
    }
  }, {
    key: "input",
    get: function get() {
      return this.calendar ? this.calendar.altInput : null;
    }
  }, {
    key: "localeFormat",
    get: function get() {
      var format = '';
      if (this.settings.enableDate) {
        format += this.defaultFormat.date;
      }
      if (this.settings.enableTime) {
        format += this.defaultFormat.time;
      }
      return format;
    }
  }, {
    key: "dateTimeFormat",
    get: function get() {
      return this.settings.useLocaleSettings ? this.localeFormat : (0, _utils.convertFormatToFlatpickr)(this.dateFormat);
    }
  }, {
    key: "dateFormat",
    get: function get() {
      return _lodash.default.get(this.settings, 'format', DEFAULT_FORMAT);
    }

    /**
     * Get the default date for the calendar.
     * @return {*}
     */
  }, {
    key: "defaultDate",
    get: function get() {
      return (0, _utils.getDateSetting)(this.settings.defaultDate);
    }
  }, {
    key: "defaultValue",
    get: function get() {
      var defaultDate = this.defaultDate;
      return defaultDate ? defaultDate.toISOString() : '';
    }

    /**
     * Return the date value.
     *
     * @param date
     * @param format
     * @return {string}
     */
  }, {
    key: "getDateValue",
    value: function getDateValue(date, format) {
      return (0, _moment.default)(date).format((0, _utils.convertFormatToMoment)(format));
    }

    /**
     * Return the value of the selected date.
     *
     * @return {*}
     */
  }, {
    key: "getValue",
    value: function getValue() {
      // Standard output format.
      if (!this.calendar) {
        return _get(_getPrototypeOf(CalendarWidget.prototype), "getValue", this).call(this);
      }

      // Get the selected dates from the calendar widget.
      var dates = this.calendar.selectedDates;
      if (!dates || !dates.length) {
        return _get(_getPrototypeOf(CalendarWidget.prototype), "getValue", this).call(this);
      }
      if (!(dates[0] instanceof Date)) {
        return 'Invalid Date';
      }
      return this.getDateValue(dates[0], this.valueFormat);
    }

    /**
     * Set the selected date value.
     *
     * @param value
     */
  }, {
    key: "setValue",
    value: function setValue(value) {
      if (!this.calendar) {
        return _get(_getPrototypeOf(CalendarWidget.prototype), "setValue", this).call(this, value);
      }
      if (value) {
        if (this.settings.saveAs !== 'text' && this.settings.readOnly && !this.loadZones()) {
          this.calendar.setDate((0, _utils.momentDate)(value, this.valueFormat, this.timezone).toDate(), false);
        } else {
          this.calendar.setDate((0, _moment.default)(value, this.valueMomentFormat).toDate(), false);
        }
      } else {
        this.calendar.clear(false);
      }
    }
  }, {
    key: "getView",
    value: function getView(value, format) {
      format = format || this.dateFormat;
      if (this.settings.saveAs === 'text') {
        return this.getDateValue(value, format);
      }
      return (0, _utils.formatDate)(value, format, this.timezone);
    }
  }, {
    key: "validationValue",
    value: function validationValue(value) {
      if (typeof value === 'string') {
        if (value) {
          this.enteredDate = (0, _moment.default)(value).format((0, _utils.convertFormatToMoment)(this.settings.format));
          return new Date(value);
        }
        return '';
      }
      return value.map(function (val) {
        return new Date(val);
      });
    }
  }, {
    key: "destroy",
    value: function destroy() {
      _get(_getPrototypeOf(CalendarWidget.prototype), "destroy", this).call(this);
      this.calendar.destroy();
    }
  }], [{
    key: "defaultSettings",
    get: /* eslint-disable camelcase */
    function get() {
      return {
        type: 'calendar',
        altInput: true,
        allowInput: true,
        clickOpens: true,
        enableDate: true,
        enableTime: true,
        mode: 'single',
        noCalendar: false,
        format: DEFAULT_FORMAT,
        dateFormat: ISO_8601_FORMAT,
        useLocaleSettings: false,
        language: 'us-en',
        defaultDate: null,
        hourIncrement: 1,
        minuteIncrement: 5,
        time_24hr: false,
        saveAs: 'date',
        displayInTimezone: '',
        timezone: '',
        minDate: '',
        maxDate: ''
      };
    }
  }]);
  return CalendarWidget;
}(_InputWidget2.default);
exports.default = CalendarWidget;