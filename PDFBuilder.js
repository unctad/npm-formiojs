"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _WebformBuilder2 = _interopRequireDefault(require("./WebformBuilder"));
var _utils = require("./utils/utils");
var _PDF = _interopRequireDefault(require("./PDF"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var PDFBuilder = /*#__PURE__*/function (_WebformBuilder) {
  _inherits(PDFBuilder, _WebformBuilder);
  var _super = _createSuper(PDFBuilder);
  function PDFBuilder() {
    _classCallCheck(this, PDFBuilder);
    return _super.apply(this, arguments);
  }
  _createClass(PDFBuilder, [{
    key: "defaultComponents",
    get: function get() {
      return {
        pdf: {
          title: 'PDF Fields',
          weight: 0,
          default: true,
          components: {
            textfield: true,
            number: true,
            password: true,
            email: true,
            phoneNumber: true,
            currency: true,
            checkbox: true,
            signature: true,
            select: true,
            textarea: true,
            datetime: true,
            file: true
          }
        },
        basic: false,
        advanced: false,
        layout: false,
        data: false,
        premium: false,
        resource: false
      };
    }
  }, {
    key: "addDropZone",
    value: function addDropZone() {
      var _this = this;
      if (!this.dropZone) {
        this.dropZone = this.ce('div', {
          class: 'formio-drop-zone'
        });
        this.prepend(this.dropZone);
      }
      this.addEventListener(this.dropZone, 'dragover', function (event) {
        event.preventDefault();
        return false;
      });
      this.addEventListener(this.dropZone, 'drop', function (event) {
        event.preventDefault();
        _this.dragStop(event);
        return false;
      });
      this.disableDropZone();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;
      return this.onElement.then(function () {
        _this2.build(_this2.clear());
        _this2.isBuilt = true;
        _this2.on('resetForm', function () {
          return _this2.resetValue();
        }, true);
        _this2.on('refreshData', function () {
          return _this2.updateValue();
        }, true);
        setTimeout(function () {
          _this2.onChange();
          _this2.emit('render');
        }, 1);
      });
    }
  }, {
    key: "enableDropZone",
    value: function enableDropZone() {
      if (this.dropZone) {
        var iframeRect = (0, _utils.getElementRect)(this.pdfForm.element);
        this.dropZone.style.height = iframeRect && iframeRect.height ? "".concat(iframeRect.height, "px") : '1000px';
        this.dropZone.style.width = iframeRect && iframeRect.width ? "".concat(iframeRect.width, "px") : '100%';
        this.addClass(this.dropZone, 'enabled');
      }
    }
  }, {
    key: "disableDropZone",
    value: function disableDropZone() {
      if (this.dropZone) {
        this.removeClass(this.dropZone, 'enabled');
      }
    }
  }, {
    key: "addComponentTo",
    value: function addComponentTo(schema, parent, element, sibling) {
      var comp = _get(_getPrototypeOf(PDFBuilder.prototype), "addComponentTo", this).call(this, schema, parent, element, sibling);
      comp.isNew = true;
      if (this.pdfForm && schema.overlay) {
        this.pdfForm.postMessage({
          name: 'addElement',
          data: schema
        });
      }
      return comp;
    }
  }, {
    key: "addComponent",
    value: function addComponent(component, element, data, before, noAdd, state) {
      return _get(_getPrototypeOf(PDFBuilder.prototype), "addComponent", this).call(this, component, element, data, before, true, state);
    }
  }, {
    key: "deleteComponent",
    value: function deleteComponent(component) {
      if (this.pdfForm && component.component) {
        this.pdfForm.postMessage({
          name: 'removeElement',
          data: component.component
        });
      }
      return _get(_getPrototypeOf(PDFBuilder.prototype), "deleteComponent", this).call(this, component);
    }
  }, {
    key: "dragStart",
    value: function dragStart(event, component) {
      event.stopPropagation();
      event.dataTransfer.setData('text/plain', 'true');
      this.currentComponent = component;
      this.enableDropZone();
    }
  }, {
    key: "removeEventListeners",
    value: function removeEventListeners(all) {
      var _this3 = this;
      _get(_getPrototypeOf(PDFBuilder.prototype), "removeEventListeners", this).call(this, all);
      _lodash.default.each(this.groups, function (group) {
        _lodash.default.each(group.components, function (builderComponent) {
          _this3.removeEventListener(builderComponent, 'dragstart');
          _this3.removeEventListener(builderComponent, 'dragend');
        });
      });
    }
  }, {
    key: "clear",
    value: function clear() {
      var state = {};
      this.destroy(state);
      return state;
    }
  }, {
    key: "redraw",
    value: function redraw() {
      if (this.pdfForm) {
        this.pdfForm.postMessage({
          name: 'redraw'
        });
      }
    }
  }, {
    key: "dragStop",
    value: function dragStop(event) {
      var schema = this.currentComponent ? this.currentComponent.schema : null;
      if (!schema) {
        return false;
      }

      // Special case for file components - default to image mode when added via PDF builder
      if (schema.type === 'file') {
        schema.image = true;
      }
      schema.overlay = {
        top: event.offsetY,
        left: event.offsetX,
        width: schema.defaultOverlayWidth || 100,
        height: schema.defaultOverlayHeight || 20
      };
      this.addComponentTo(schema, this, this.getContainer());
      this.disableDropZone();
      return false;
    }

    // Don't need to add a submit button here... the pdfForm will already do this.
  }, {
    key: "addSubmitButton",
    value: function addSubmitButton() {}
  }, {
    key: "addBuilderComponent",
    value: function addBuilderComponent(component, group) {
      var _this4 = this;
      var builderComponent = _get(_getPrototypeOf(PDFBuilder.prototype), "addBuilderComponent", this).call(this, component, group);
      if (builderComponent) {
        builderComponent.element.draggable = true;
        builderComponent.element.setAttribute('draggable', true);
        this.addEventListener(builderComponent.element, 'dragstart', function (event) {
          return _this4.dragStart(event, component);
        }, true);
        this.addEventListener(builderComponent.element, 'dragend', function () {
          return _this4.disableDropZone();
        }, true);
      }
      return builderComponent;
    }
  }, {
    key: "refreshDraggable",
    value: function refreshDraggable() {
      this.addSubmitButton();
      this.builderReadyResolve();
    }
  }, {
    key: "build",
    value: function build() {
      var _this5 = this;
      this.buildSidebar();
      if (!this.pdfForm) {
        this.element.noDrop = true;
        this.pdfForm = new _PDF.default(this.element, this.options);
        this.addClass(this.pdfForm.element, 'formio-pdf-builder');
      }
      this.pdfForm.destroy(true);
      this.pdfForm.on('iframe-elementUpdate', function (schema) {
        var component = _this5.getComponentById(schema.id);
        if (component && component.component) {
          component.component.overlay = {
            page: schema.page,
            left: schema.left,
            top: schema.top,
            height: schema.height,
            width: schema.width
          };
          _this5.editComponent(component);
          _this5.emit('updateComponent', component);
        }
        return component;
      }, true);
      this.pdfForm.on('iframe-componentUpdate', function (schema) {
        var component = _this5.getComponentById(schema.id);
        if (component && component.component) {
          component.component.overlay = {
            page: schema.overlay.page,
            left: schema.overlay.left,
            top: schema.overlay.top,
            height: schema.overlay.height,
            width: schema.overlay.width
          };
          _this5.emit('updateComponent', component);
          var localComponent = _lodash.default.find(_this5.form.components, {
            id: schema.id
          });
          if (localComponent) {
            localComponent.overlay = _lodash.default.clone(component.component.overlay);
          }
          _this5.emit('change', _this5.form);
        }
        return component;
      }, true);
      this.pdfForm.on('iframe-componentClick', function (schema) {
        var component = _this5.getComponentById(schema.id);
        if (component) {
          _this5.editComponent(component);
        }
      }, true);
      this.addComponents();
      this.addDropZone();
      this.updateDraggable();
      this.formReadyResolve();
    }
  }, {
    key: "setForm",
    value: function setForm(form) {
      var _this6 = this;
      // If this is a brand new form, make sure it has a submit button component
      if (!form.created && !_lodash.default.find(form.components || [], {
        type: 'button',
        key: 'submit'
      })) {
        form.components.push({
          type: 'button',
          label: this.t('Submit'),
          key: 'submit',
          size: 'md',
          block: false,
          action: 'submit',
          disableOnInvalid: true,
          theme: 'primary'
        });
      }
      return _get(_getPrototypeOf(PDFBuilder.prototype), "setForm", this).call(this, form).then(function () {
        return _this6.ready.then(function () {
          // Ensure PDFBuilder component IDs are included in form schema to prevent desync with child PDF
          var formCopy = _lodash.default.cloneDeep(form);
          formCopy.components.forEach(function (c, i) {
            return c.id = _this6.components[i].id;
          });
          if (_this6.pdfForm) {
            return _this6.pdfForm.setForm(formCopy);
          }
          return form;
        });
      });
    }
  }]);
  return PDFBuilder;
}(_WebformBuilder2.default);
exports.default = PDFBuilder;