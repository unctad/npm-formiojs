"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.split.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.object.values.js");
require("core-js/modules/es.object.assign.js");
require("core-js/modules/es.string.ends-with.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.array.find-index.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _Webform2 = _interopRequireDefault(require("./Webform"));
var _dragula = _interopRequireDefault(require("dragula/dist/dragula"));
var _tooltip = _interopRequireDefault(require("tooltip.js"));
var _Components = _interopRequireDefault(require("./components/Components"));
var _builder = _interopRequireDefault(require("./utils/builder"));
var _utils = require("./utils/utils");
var _EventEmitter = _interopRequireDefault(require("./EventEmitter"));
var _nativePromiseOnly = _interopRequireDefault(require("native-promise-only"));
var _lodash = _interopRequireDefault(require("lodash"));
var _Base = _interopRequireDefault(require("./components/base/Base.form"));
var _transliteration = require("transliteration");
var _stringHash = _interopRequireDefault(require("string-hash"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); } // Import from dist because dragula requires "global" to be defined which messes up Angular.
require('./components/builder');
var WebformBuilder = /*#__PURE__*/function (_Webform) {
  _inherits(WebformBuilder, _Webform);
  var _super = _createSuper(WebformBuilder);
  function WebformBuilder(element, options) {
    var _this;
    _classCallCheck(this, WebformBuilder);
    _this = _super.call(this, element, options);
    _this.builderHeight = 0;
    _this.dragContainers = [];
    _this.sidebarContainers = [];
    _this.updateDraggable = _lodash.default.debounce(_this.refreshDraggable.bind(_assertThisInitialized(_this)), 200);

    // Setup the builder options.
    _this.options.builder = _lodash.default.defaultsDeep({}, _this.options.builder, _this.defaultComponents);
    _this.options.enableButtons = _lodash.default.defaults({}, _this.options.enableButtons, {
      remove: true,
      copy: true,
      paste: true,
      edit: true,
      editJson: false
    });
    _this.options.showAdvanced = false;
    _this.options.showHelp = false;
    _this.options.openedPanels = false;
    _this.options.pasteMode = false;

    // Turn off if explicitely said to do so...
    _lodash.default.each(_this.defaultComponents, function (config, key) {
      if (config === false) {
        _this.options.builder[key] = false;
      }
    });
    _this.builderReady = new _nativePromiseOnly.default(function (resolve) {
      _this.builderReadyResolve = resolve;
    });
    _this.groups = {};
    _this.options.sideBarScroll = _lodash.default.get(_this.options, 'sideBarScroll', true);
    _this.options.sideBarScrollOffset = _lodash.default.get(_this.options, 'sideBarScrollOffset', 0);
    _this.options.hooks = _this.options.hooks || {};
    _this.options.hooks.addComponents = function (components, parent) {
      if (!components || !components.length && !components.nodrop) {
        // Return a simple alert so they know they can add something here.
        return [{
          type: 'htmlelement',
          internal: true,
          tag: 'div',
          className: 'drag-and-drop-alert alert',
          attrs: [{
            attr: 'id',
            value: "".concat(parent.id, "-placeholder")
          }, {
            attr: 'style',
            value: 'text-align:center;'
          }, {
            attr: 'role',
            value: 'alert'
          }],
          content: _this.t('Drag and Drop a form component')
        }];
      }
      return components;
    };
    _this.options.hooks.addComponent = function (container, comp, parent) {
      if (!comp || !comp.component) {
        return container;
      }
      var parentCompReadOnly = parent && parent.schemaReadOnly;
      var compReadOnly = parentCompReadOnly || comp.schemaReadOnly;
      if (!comp.noEdit && !comp.component.internal) {
        // Make sure the component position is relative so the buttons align properly.
        comp.getElement().style.position = 'relative';
        var tooltipPosition = 'top';
        if (comp.component.type === 'panel') tooltipPosition = 'top';
        if (comp.component.type === 'block') tooltipPosition = 'top';
        if (comp.component.type === 'table') tooltipPosition = 'top';
        var removeButton = null;
        var editButton = null;
        var translateButton = null;
        if (!compReadOnly) {
          removeButton = _this.ce('div', {
            class: 'btn btn-xxs text-danger component-settings-button-2 component-settings-button-remove'
          }, _this.getIcon('trash-alt', 'fa-regular'));
          _this.addEventListener(removeButton, 'click', function () {
            comp.deleteValue();
            _this.deleteComponent(comp, true);
          });
          new _tooltip.default(removeButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: removeButton,
            title: _this.t('Remove'),
            popperOptions: {
              positionFixed: true
            }
          });
          editButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit'
          }, _this.getIcon('gear', 'fa-regular'));
          _this.addEventListener(editButton, 'click', function () {
            document.dispatchEvent(new CustomEvent('formioEditComponent', {
              detail: comp
            }));
            _this.editComponent(comp);
          });
          new _tooltip.default(editButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: editButton,
            title: _this.t('Edit'),
            popperOptions: {
              positionFixed: true
            }
          });
          translateButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit'
          }, _this.getIcon('globe', 'fa-regular'));
          _this.addEventListener(translateButton, 'click', function () {
            document.dispatchEvent(new CustomEvent('formioEditComponent', {
              detail: comp,
              tab: 'translate'
            }));
            _this.editComponent(comp, false, 'translate');
          });
          new _tooltip.default(translateButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: translateButton,
            title: _this.t('Translate'),
            popperOptions: {
              positionFixed: true
            }
          });
        }
        var moveButton = null;
        if (parent && parent.type === 'datagrid') {
          moveButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit'
          }, _this.getIcon('up-down-left-right', 'fa-solid'));
          new _tooltip.default(moveButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: moveButton,
            title: _this.t('Move'),
            popperOptions: {
              positionFixed: true
            }
          });
        }
        var minimizeButton = _this.ce('div', {
          class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-minimize'
        }, _this.getIcon('minus', 'fa-regular'));
        _this.addEventListener(minimizeButton, 'click', function () {
          var compEl = comp.getElement();
          if (compEl) {
            if (compEl.classList.contains('minimized')) {
              compEl.classList.remove('minimized');
              comp.component.minimized = false;
            } else {
              compEl.classList.add('minimized');
              compEl.minimized = true;
            }
            var minimizeBtnEl = compEl.querySelector('.component-settings-button-minimize .fa-regular');
            if (minimizeBtnEl) {
              if (minimizeBtnEl.classList.contains('fa-minus')) {
                minimizeBtnEl.classList.remove('fa-minus');
                minimizeBtnEl.classList.add('fa-plus');
              } else {
                minimizeBtnEl.classList.add('fa-minus');
                minimizeBtnEl.classList.remove('fa-plus');
              }
            }
          }
          _this.updateComponent(comp);
          _this.emit('saveComponent', comp);
        });
        new _tooltip.default(minimizeButton, {
          trigger: 'hover',
          placement: tooltipPosition,
          container: minimizeButton,
          title: _this.t('Toggle size'),
          popperOptions: {
            positionFixed: true
          }
        });
        if (comp.component.minimized) {
          var scopeEl = comp.getElement();
          if (scopeEl) {
            scopeEl.classList.add('minimized');
          }
          var minBtnIconEl = minimizeButton.querySelector('[class*="fa-"]');
          if (minBtnIconEl) {
            minBtnIconEl.classList.remove('fa-minus');
            minBtnIconEl.classList.add('fa-plus');
          }
        }
        var copyButton = _this.ce('div', {
          class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-copy'
        }, _this.getIcon('clone', 'fa-regular'));
        _this.addEventListener(copyButton, 'click', function (ev) {
          document.dispatchEvent(new CustomEvent('formioCopyComponent', {
            detail: comp
          }));
          return _this.copyComponent(comp);
        });
        new _tooltip.default(copyButton, {
          trigger: 'hover',
          placement: tooltipPosition,
          container: copyButton,
          title: _this.t('Copy'),
          popperOptions: {
            positionFixed: true
          }
        });
        var pasteButton = null;
        var pasteWithTransformButton = null;
        if (!parentCompReadOnly) {
          pasteButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-paste'
          }, _this.getIcon('clipboard', 'fa-regular'));
          var pasteTooltip = new _tooltip.default(pasteButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: pasteButton,
            title: _this.t('Paste below'),
            popperOptions: {
              positionFixed: true
            }
          });
          _this.addEventListener(pasteButton, 'click', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  pasteTooltip.hide();
                  _context.next = 3;
                  return _this.pasteComponent(comp);
                case 3:
                case "end":
                  return _context.stop();
              }
            }, _callee);
          })));
          if (_this.options.pasteWithTransformationEnabled) {
            pasteWithTransformButton = _this.ce('div', {
              class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-paste'
            }, _this.getIcon('clipboard-prescription', 'fa-regular'));
            var pasteWithTransformTooltip = new _tooltip.default(pasteWithTransformButton, {
              trigger: 'hover',
              placement: tooltipPosition,
              container: pasteWithTransformButton,
              title: _this.t('Paste with transform below'),
              popperOptions: {
                positionFixed: true
              }
            });
            _this.addEventListener(pasteWithTransformButton, 'click', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
              return _regeneratorRuntime().wrap(function _callee2$(_context2) {
                while (1) switch (_context2.prev = _context2.next) {
                  case 0:
                    pasteWithTransformTooltip.hide();
                    _context2.next = 3;
                    return _this.pasteComponent(comp, true);
                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }, _callee2);
            })));
          }
        }
        var editJsonButton = null;
        if (!compReadOnly) {
          editJsonButton = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-settings-button-2 component-settings-button-edit-json'
          }, _this.getIcon('wrench', 'fa-regular'));
          _this.addEventListener(editJsonButton, 'click', function () {
            return _this.editComponent(comp, true);
          });
          new _tooltip.default(editJsonButton, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: editJsonButton,
            title: _this.t('Edit JSON'),
            popperOptions: {
              positionFixed: true
            }
          });
        }

        // Set in paste mode if we have an item in our clipboard.
        if (_this.options.pasteMode) {
          _this.addClass(_this.element, 'builder-paste-mode');
        }

        // Add the edit buttons to the component.
        var fieldActions = _this.ce('div', {
          class: 'component-btn-group component-configuration-group',
          'data-id': comp.component.key
        }, [_this.options.enableButtons.edit && (comp.type === 'content' || comp.component.type === 'fieldset' || comp.component.type === 'tabs' || comp.component.type === 'table' || comp.component.type === 'columns' || comp.component.type === 'editgrid' || comp.component.type === 'datagrid' || comp.component.type === 'radio') || comp.component.type === 'container' ? minimizeButton : null, _this.options.enableButtons.copy ? copyButton : null, _this.options.enableButtons.paste ? pasteButton : null, _this.options.enableButtons.paste && _this.options.pasteWithTransformationEnabled ? pasteWithTransformButton : null, _this.options.enableButtons.editJson ? editJsonButton : null, _this.options.enableButtons.edit ? editButton : null, _this.options.enableButtons.edit && comp.component.type !== 'table' ? translateButton : null, _this.options.enableButtons.edit ? moveButton : null, _this.options.enableButtons.remove ? removeButton : null]);
        var children = false;
        var effectCount = Array.isArray(comp.component.effectsIds) && comp.component.effectsIds.length || comp.component.numberOfEffects;
        if (effectCount) {
          children = true;
          var hasEffect = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-effects ".concat(effectCount > 1 ? 'more-than-one' : '')
          }, 'E');
          if (!compReadOnly) {
            _this.addEventListener(hasEffect, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'action'
              }));
              _this.editComponent(comp, false, 'determinant');
            });
          }
          new _tooltip.default(hasEffect, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasEffect,
            title: _this.t('Effects'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasEffect);
        }
        var actionCount = Array.isArray(comp.component.actionRowIds) && comp.component.actionRowIds.length || comp.component.numberOfActions;
        if (actionCount) {
          children = true;
          var hasAction = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-action ".concat(actionCount > 1 ? 'more-than-one' : '')
          }, 'A');
          if (!compReadOnly) {
            _this.addEventListener(hasAction, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'action'
              }));
              _this.editComponent(comp, false, 'action');
            });
          }
          new _tooltip.default(hasAction, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasAction,
            title: _this.t('Actions'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasAction);
        }
        var determinantCount = Array.isArray(comp.component.determinantIds) && comp.component.determinantIds.length || comp.component.numberOfConditions;
        if (determinantCount) {
          children = true;
          var hasDeterminant = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-determinant ".concat(determinantCount > 1 ? 'more-than-one' : '')
          }, 'D');
          if (!compReadOnly) {
            _this.addEventListener(hasDeterminant, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'determinant'
              }));
              _this.editComponent(comp, false, 'determinant');
            });
          }
          new _tooltip.default(hasDeterminant, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasDeterminant,
            title: _this.t('Determinants'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasDeterminant);
        }
        var formulaCount = Array.isArray(comp.component.formulaRowIds) && comp.component.formulaRowIds.length || comp.component.numberOfFormulas;
        if (formulaCount) {
          children = true;
          var hasFormula = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-formula ".concat(formulaCount > 1 ? 'more-than-one' : '')
          }, 'F');
          if (!compReadOnly) {
            _this.addEventListener(hasFormula, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'formula'
              }));
              _this.editComponent(comp, false, 'formula');
            });
          }
          new _tooltip.default(hasFormula, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasFormula,
            title: _this.t('Formula'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasFormula);
        }
        var blacklistCount = Array.isArray(comp.component.blacklistRowIds) && comp.component.blacklistRowIds.length;
        if (blacklistCount) {
          children = true;
          var hasBlacklist = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-blacklist ".concat(blacklistCount > 1 ? 'more-than-one' : '')
          }, 'B');
          if (!compReadOnly) {
            _this.addEventListener(hasBlacklist, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'validation'
              }));
              _this.editComponent(comp, false, 'validation');
            });
          }
          new _tooltip.default(hasBlacklist, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasBlacklist,
            title: _this.t('Blacklist'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasBlacklist);
        }
        var validationCount = Array.isArray(comp.component.validationRowIds) && comp.component.validationRowIds.length;
        if (validationCount) {
          children = true;
          var hasValidation = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-validation ".concat(validationCount > 1 ? 'more-than-one' : '')
          }, 'V');
          if (!compReadOnly) {
            _this.addEventListener(hasValidation, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'validation'
              }));
              _this.editComponent(comp, false, 'validation');
            });
          }
          new _tooltip.default(hasValidation, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasValidation,
            title: _this.t('Validation'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasValidation);
        }
        if (Array.isArray(comp.component.copyValueFrom) && comp.component.copyValueFrom.length) {
          children = true;
          var hasCopyValueFrom = _this.ce('div', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-copy-value-from ".concat(comp.component.copyValueFrom.length > 1 ? 'more-than-one' : '')
          }, 'C');
          if (!compReadOnly) {
            _this.addEventListener(hasCopyValueFrom, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'data'
              }));
              _this.editComponent(comp, false, 'data');
            });
          }
          new _tooltip.default(hasCopyValueFrom, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasCopyValueFrom,
            title: _this.t('CopyValueFrom'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasCopyValueFrom);
        }
        if (comp.component.linkedWithRequirement && comp.component.linkingRequirement || comp.component.linkedWithResult && comp.component.linkingResult) {
          children = true;
          var suffix = comp.component.linkedWithRequirement && comp.component.linkingRequirement ? 'component-configuration-button-linked-requirement' : 'component-configuration-button-linked-result';
          var hasLink = _this.ce('div', {
            class: 'btn btn-xxs btn-default component-configuration-button component-configuration-button-linked '.concat(suffix)
          }, 'L');
          if (!compReadOnly) {
            _this.addEventListener(hasLink, 'click', function () {
              document.dispatchEvent(new CustomEvent('formioEditComponent', {
                detail: comp,
                tab: 'data'
              }));
              _this.editComponent(comp, false, 'data');
            });
          }
          new _tooltip.default(hasLink, {
            trigger: 'hover',
            placement: tooltipPosition,
            container: hasLink,
            title: _this.t('Linked'),
            popperOptions: {
              positionFixed: true
            }
          });
          fieldActions.prepend(hasLink);
        }
        if (children) {
          fieldActions.classList.add('hasIcons');
        }
        comp.prepend(fieldActions);
      }
      if (!container.noDrop && !parentCompReadOnly) {
        _this.addDragContainer(container, parent);
      }
      return container;
    };
    _this.setToolbarElement();
    _this.setBuilderElement();
    return _this;
  }
  _createClass(WebformBuilder, [{
    key: "defaultComponents",
    get: function get() {
      return {
        basic: {
          title: 'Basic Components',
          weight: 0,
          default: true
        },
        advanced: {
          title: 'Advanced',
          weight: 10
        },
        layout: {
          title: 'Layout',
          weight: 20
        },
        data: {
          title: 'Data',
          weight: 30
        }
      };
    }
  }, {
    key: "scrollSidebar",
    value: function scrollSidebar() {
      var newTop = window.scrollY - this.sideBarTop + this.options.sideBarScrollOffset;
      var shouldScroll = newTop > 0;
      if (shouldScroll && newTop + this.sideBarElement.offsetHeight < this.builderHeight) {
        this.sideBarElement.style.marginTop = "".concat(newTop, "px");
      } else if (shouldScroll && this.sideBarElement.offsetHeight < this.builderHeight) {
        this.sideBarElement.style.marginTop = "".concat(this.builderHeight - this.sideBarElement.offsetHeight, "px");
      } else {
        this.sideBarElement.style.marginTop = '0px';
      }
    }
  }, {
    key: "setBuilderElement",
    value: function setBuilderElement() {
      var _this2 = this;
      return this.onElement.then(function () {
        _this2.addClass(_this2.wrapper, 'row formbuilder');
        _this2.builderSidebar = _this2.ce('div', {
          class: 'col col-lg-4 col-xl-3 formcomponents',
          id: 'col1'
        });
        _this2.prependTo(_this2.builderSidebar, _this2.wrapper);
        _this2.addClass(_this2.element, 'formarea');
        _this2.element.component = _this2;
      });
    }
  }, {
    key: "toggleCollapsePanels",
    value: function toggleCollapsePanels() {
      var panelElements = this.element.querySelectorAll('.panel');
      var panelComponents = [];
      for (var i = 0; i < panelElements.length; i++) {
        if (panelElements[i].component) {
          panelComponents.push(panelElements[i].component);
        }
      }
      this.options.openedPanels = panelComponents.filter(function (c) {
        return c.component.title;
      }).every(function (c) {
        return !c.component.collapsed;
      });
      this.togglePanelsTogglerIcon();
    }
  }, {
    key: "togglePanelsTogglerIcon",
    value: function togglePanelsTogglerIcon() {
      if (this.options.openedPanels) {
        this.panelsToggler.getElementsByTagName('i')[0].classList.remove('fa-angle-down');
        this.panelsToggler.getElementsByTagName('i')[0].classList.add('fa-angle-up');
      } else {
        this.panelsToggler.getElementsByTagName('i')[0].classList.add('fa-angle-down');
        this.panelsToggler.getElementsByTagName('i')[0].classList.remove('fa-angle-up');
      }
    }
  }, {
    key: "togglePanels",
    value: function togglePanels() {
      if (this.options.openedPanels) {
        this.emit('goOpenPanels', this);
      } else {
        this.emit('goClosePanels', this);
      }
      this.options.openedPanels = !this.options.openedPanels;
      this.togglePanelsTogglerIcon();
    }
  }, {
    key: "setToolbarElement",
    value: function setToolbarElement() {
      var _this3 = this;
      if (this.options.builder.noToolbar) return;
      this.toolbarRow = document.getElementById('myToolbar');
      //const toolbarHtml = require('./htmltemplates/formbuilder-toolbar.html');
      var t1 = this.t('Containers');
      var t2 = this.t('Toggle blocks');
      var t3 = this.t('Fields');
      var t4 = this.t('Other components');
      var t5 = this.t('Data');
      var t6 = this.t('Custom components');
      var t7 = this.t('Service forms');
      var toolbarHtml = "<div class=\"formbuilder-toolbar row\"><div class=\"col-auto formio-component formio-component-tabs formbuilder-toolbar-fixed-container col p-0\"><ul class=\"nav nav-tabs justify-content-between\" id=\"formbuilder-toolbar-fixed-nav\" role=\"tablist\"><li class=\"nav-item\"><a class=\"nav-link active\" id=\"tools-containers-link\" data-toggle=\"tab\" data-target=\"#tools-containers\" role=\"tab\" aria-controls=\"tools-containers\" aria-selected=\"true\">".concat(t1, "</a></li><li class=\"pull-right nav-item\"><a id=\"panelsToggler\" class=\"nav-link\"><i class=\"fa fa-angle-down\"></i>").concat(t2, "</a></li></ul><div class=\"tab-content\" id=\"formbuilder-toolbar-fixed-content\"><div class=\"tab-pane show active\" id=\"tools-containers\" role=\"tabpanel\" aria-labelledby=\"tools-containers-link\"><div class=\"row\"><div id=\"toolbar-layout-components\" class=\"col\"></div></div></div></div></div><div class=\"formio-component formio-component-tabs col formbuilder-toolbar-container p-0\"><ul class=\"nav nav-tabs\" id=\"formbuilder-toolbar-nav\" role=\"tablist\"><li class=\"nav-item\"><a class=\"nav-link active\" id=\"tools-fields-link\" data-toggle=\"tab\" data-target=\"#tools-fields\" role=\"tab\" aria-controls=\"tools-fields\" aria-selected=\"true\">").concat(t3, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-others-link\" data-target=\"#tools-others\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"tools-others\" aria-selected=\"false\">").concat(t4, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-data-link\" data-toggle=\"tab\" data-target=\"#tools-data\" role=\"tab\" aria-controls=\"tools-data\" aria-selected=\"false\">").concat(t5, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-custom-link\" data-toggle=\"tab\" data-target=\"#tools-custom\" role=\"tab\" aria-controls=\"tools-custom\" aria-selected=\"false\">").concat(t6, "</a></li><li class=\"nav-item\"><a class=\"nav-link\" id=\"tools-serviceforms-link\" data-toggle=\"tab\" data-target=\"#tools-serviceforms\" role=\"tab\" aria-controls=\"tools-serviceforms\" aria-selected=\"false\">").concat(t7, "</a></li></ul><div class=\"tab-content\" id=\"formbuilder-toolbar-content\"><div class=\"tab-pane fade show active\" id=\"tools-fields\" role=\"tabpanel\" aria-labelledby=\"tools-fields-link\"><div class=\"row\"><div id=\"toolbar-basic-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-others\" role=\"tabpanel\" aria-labelledby=\"tools-others-link\"><div class=\"row\"><div id=\"toolbar-advanced-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-data\" role=\"tabpanel\" aria-labelledby=\"tools-data-link\"><div class=\"row\"><div id=\"toolbar-data-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-custom\" role=\"tabpanel\" aria-labelledby=\"tools-custom-link\"><div class=\"row\"><div id=\"toolbar-custom-components\" class=\"col\"></div></div></div><div class=\"tab-pane fade\" id=\"tools-serviceforms\" role=\"tabpanel\" aria-labelledby=\"tools-custom-link\"><div class=\"row\"><div id=\"toolbar-serviceforms-components\" class=\"col\"></div></div></div></div></div></div>");
      this.toolbarRow.innerHTML = toolbarHtml;
      this.groupBasic = this.toolbarRow.querySelector('#toolbar-basic-components');
      this.groupAdvanced = this.toolbarRow.querySelector('#toolbar-advanced-components');
      this.groupLayout = this.toolbarRow.querySelector('#toolbar-layout-components');
      this.groupData = this.toolbarRow.querySelector('#toolbar-data-components');
      this.groupCustom = this.toolbarRow.querySelector('#toolbar-custom-components');
      this.groupServiceForms = this.toolbarRow.querySelector('#toolbar-serviceforms-components');
      this.panelsToggler = this.toolbarRow.querySelector('#panelsToggler');
      this.panelsToggler.addEventListener('click', function (event) {
        event.preventDefault();
        _this3.togglePanels();
      });
      var allComponents = _lodash.default.filter(_lodash.default.map(_Components.default.components, function (component, type) {
        if (!component.builderInfo || component.builderInfo.hidden) {
          return null;
        }
        component.type = type;
        return component;
      }));
      if (this.options.builder.customComponents && this.options.builder.customComponents.components) {
        Array.prototype.push.apply(allComponents, this.options.builder.customComponents.components);
      }
      var components = {};
      _lodash.default.map(_lodash.default.sortBy(allComponents, function (component) {
        return component.builderInfo.weight;
      }), function (component) {
        var builderInfo = component.builderInfo;
        builderInfo.key = component.type;
        components[builderInfo.key] = builderInfo;
      });
      _lodash.default.each(components, function (component) {
        component.element = _this3.ce('span', _defineProperty({
          id: "builder-".concat(component.key),
          class: "btn formcomponent drag-copy ".concat(component.extraClass)
        }, 'aria-label', component.title));
        if (component.group === 'serviceforms') {
          component.element.addEventListener('click', function (ev) {
            document.dispatchEvent(new CustomEvent('openFormForCopy', {
              detail: component
            }));
            ev.preventDefault();
          });
        }
        component.tooltip = new _tooltip.default(component.element, {
          trigger: 'hover',
          placement: 'bottom',
          container: component.element,
          title: _this3.t(component.title)
        });
        if (component.group !== 'serviceforms') {
          _this3.addEventListener(component.element, 'mouseout', function () {
            return component.tooltip.hide();
          });
        }
        if (component.icon) {
          component.element.appendChild(_this3.ce('i', {
            class: component.icon,
            'aria-hidden': 'true'
          }));
        }
        component.element.builderInfo = component;
        if (component.group === 'basic') {
          _this3.groupBasic.appendChild(component.element);
        } else if (component.group === 'advanced') {
          _this3.groupAdvanced.appendChild(component.element);
        } else if (component.group === 'layout') {
          _this3.groupLayout.appendChild(component.element);
        } else if (component.group === 'data') {
          _this3.groupData.appendChild(component.element);
        } else if (component.group === 'custom') {
          _this3.groupCustom.appendChild(component.element);
        } else if (component.group === 'serviceforms') {
          _this3.groupServiceForms.appendChild(component.element);
        }
      });
      this.groupBasic.component = this;
      this.groupAdvanced.component = this;
      this.groupLayout.component = this;
      this.groupData.component = this;
      this.groupCustom.component = this;
      this.groupServiceForms.component = this;
      this.updateDraggable();
      this.on('aPanelCollapsed', function (comp) {
        _this3.emit('saveComponent', comp);
        _this3.toggleCollapsePanels();
      });
      this.on('allPanelsCollapsed', function (comp) {
        _this3.emit('saveComponent', comp);
      });
      this.toggleCollapsePanels();
    }
  }, {
    key: "ready",
    get: function get() {
      return this.builderReady;
    }
  }, {
    key: "setForm",
    value: function setForm(form) {
      var _this4 = this;
      //populate isEnabled for recaptcha form settings
      var isRecaptchaEnabled = false;
      if (form.components) {
        (0, _utils.eachComponent)(form.components, function (component) {
          if (isRecaptchaEnabled) {
            return;
          }
          if (component.type === 'recaptcha') {
            isRecaptchaEnabled = true;
            return false;
          }
        });
        if (isRecaptchaEnabled) {
          _lodash.default.set(form, 'settings.recaptcha.isEnabled', true);
        } else if (_lodash.default.get(form, 'settings.recaptcha.isEnabled')) {
          _lodash.default.set(form, 'settings.recaptcha.isEnabled', false);
        }
      }
      this.emit('change', form);
      if (this.builderHeight && this.element) {
        this.element.style.minHeight = "".concat(this.builderHeight, "px");
      }
      return _get(_getPrototypeOf(WebformBuilder.prototype), "setForm", this).call(this, form).then(function (retVal) {
        setTimeout(function () {
          if (_this4.element.style.minHeight) {
            _this4.element.style.minHeight = null;
          }
          _this4.builderHeight = _this4.element.offsetHeight;
        }, 200);
        return retVal;
      });
    }
  }, {
    key: "deleteComponent",
    value: function deleteComponent(component) {
      var dispatchEvent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (!component.parent) {
        return;
      }
      if (component.component && component.component.canBeRemoved === false) {
        return;
      }
      var remove = true;
      if (typeof component.getComponents === 'function' && component.getComponents().length > 0) {
        var message = 'Removing this component will also remove all of its children. Are you sure you want to do this?';
        remove = window.confirm(this.t(message));
      }
      if (remove) {
        if (dispatchEvent) {
          document.dispatchEvent(new CustomEvent('formioDeleteComponent', {
            detail: component
          }));
        }
        this.emit('deleteComponentId', component.id);
        component.parent.removeComponentById(component.id);
        this.form = this.schema;
        this.emit('deleteComponent', component);
      }
      return remove;
    }
  }, {
    key: "updateComponent",
    value: function updateComponent(component) {
      var _this5 = this;
      // Update the preview.
      if (this.componentPreview) {
        if (this.preview) {
          this.preview.destroy();
        }
        this.preview = _Components.default.create(component.component, {
          preview: true,
          events: new _EventEmitter.default({
            wildcard: false,
            maxListeners: 0
          })
        }, {}, true);
        this.preview.on('componentEdit', function (comp) {
          _lodash.default.merge(component.component, comp.component);
          _this5.editForm.redraw();
        });
        this.preview.build();
        this.preview.isBuilt = true;
        this.componentPreview.innerHTML = '';
        this.componentPreview.appendChild(this.preview.getElement());
      }

      // Ensure this component has a key.
      if (component.isNew) {
        var namespaceKey = this.recurseNamespace(component.parent);
        if (!component.keyModified) {
          if (!namespaceKey && this.options.formName) {
            // if component is not inside any container and exists form prefix then lets add it
            component.component.key = _lodash.default.camelCase(this.options.formName.concat(component.component.label || component.component.placeholder || component.component.type));
          } else {
            component.component.key = _lodash.default.camelCase(component.component.label || component.component.placeholder || component.component.type);
          }
          component.component.key = _lodash.default.camelCase((0, _transliteration.transliterate)(component.component.key));
        }

        // Set a unique key for this component.
        // BuilderUtils.uniquify(this._form, component.component);
        var ns = this.findNamespaceRoot(component.component);
        // if (!namespaceKey) {
        _builder.default.uniquify(this._form, component.component, _lodash.default.camelCase(this.options.formName), false, false, false);
        // }
        // else {
        //   BuilderUtils.uniquify(ns, component.component);
        // }
        if (this.editForm) {
          var keyComponent = (0, _utils.getComponent)(this.editForm.components, 'key');
          if (keyComponent && keyComponent.element) {
            keyComponent.redraw();
          }
        }
      }

      // Change the "default value" field to be reflective of this component.
      if (this.defaultValueComponent) {
        var defaultValueComponentType = this.defaultValueComponent.type;
        _lodash.default.assign(this.defaultValueComponent, _lodash.default.omit(component.component, ['key', 'label', 'disableAddingRows', 'disableRemovingRows', 'disableAddingRemovingRows', 'placeholder', 'tooltip', 'validate', 'disabled', 'hidden', 'fields.day.required', 'fields.month.required', 'fields.year.required', 'defaultValue', 'defaultDate', 'clearOnHide']));
        if (typeof _Components.default.components[this.defaultValueComponent.type] !== 'function') {
          this.defaultValueComponent.type = defaultValueComponentType || this.defaultValueComponent.type;
        }
        if (typeof _Components.default.components[this.defaultValueComponent.type] !== 'function') {
          this.defaultValueComponent.type = 'textarea';
          this.defaultValueComponent.editor = 'ace';
        }
        if (this.defaultValueComponent.components) {
          this.defaultValueComponent.components = _lodash.default.cloneDeep(this.defaultValueComponent.components);
          (0, _utils.eachComponent)(this.defaultValueComponent.components, function (childComp) {
            if (childComp.validate) {
              delete childComp.validate;
            }
          }, true);
        }
        if (this.defaultValueComponent.customClass) {
          var customClasses = this.defaultValueComponent.customClass.split(/\s+/).filter(function (c) {
            return !['hide', 'hidden'].includes(c);
          });
          this.defaultValueComponent.customClass = customClasses.join(' ');
        }
        if (this.defaultValueComponent.type === 'checkbox') {
          this.defaultValueComponent.label = 'Checked by default';
        }
        this.editForm.emit('defaultValueComponentChanged');
      }

      // Called when we update a component.
      this.emit('updateComponent', component);
    }

    /**
       * When a component sets its api key, we need to check if it is unique within its namespace. Find the namespace root
       * so we can calculate this correctly.
       * @param component
       */
  }, {
    key: "findNamespaceRoot",
    value: function findNamespaceRoot(component) {
      // First get the component with nested parents.
      var comp = (0, _utils.getComponent)(component.parent ? component.parent.components : this._form.components, component.key, true);
      var namespaceKey = this.recurseNamespace(comp);

      // If there is no key, it is the root form.
      if (!namespaceKey || this.form.key === namespaceKey) {
        return this.form.components;
      }

      // If the current component is the namespace, we don't need to find it again.
      if (namespaceKey === component.key) {
        return component.components;
      }

      // Get the namespace component so we have the original object.
      var namespaceComponent = (0, _utils.getComponent)(this.form.components, namespaceKey, true);
      return namespaceComponent.components;
    }
  }, {
    key: "recurseNamespace",
    value: function recurseNamespace(component) {
      // If there is no parent, we are at the root level.
      if (!component) {
        return null;
      }

      // Some components are their own namespace.
      if (['tree'].includes(component.type) || component.tree || component.arrayTree) {
        return component.key;
      }

      // Anything else, keep going up.
      return this.recurseNamespace(component.parent);
    }
  }, {
    key: "toggleAdvancedFields",
    value: function toggleAdvancedFields() {
      if (this.dialog) {
        this.advancedFields = this.dialog.getElementsByClassName('advanced-field');
        if (this.options.showAdvanced) {
          this.dialog.advancedToggler.getElementsByTagName('i')[0].classList.remove('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.advancedFields, function (advancedField) {
            if (advancedField.parentNode.classList.contains('col')) {
              advancedField.parentNode.classList.remove('hidden');
            } else {
              advancedField.classList.remove('hidden');
            }
          });
          this.dialog.body.classList.add('advanced-field-enabled');
        } else {
          this.dialog.advancedToggler.getElementsByTagName('i')[0].classList.add('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.advancedFields, function (advancedField) {
            if (advancedField.parentNode.classList.contains('col')) {
              advancedField.parentNode.classList.add('hidden');
            } else {
              advancedField.classList.add('hidden');
            }
          });
          this.dialog.body.classList.remove('advanced-field-enabled');
        }
      }
    }
  }, {
    key: "toggleHelp",
    value: function toggleHelp() {
      if (this.dialog) {
        this.helpFields = this.dialog.getElementsByClassName('fa-question-circle');
        if (this.options.showHelp) {
          this.dialog.helpToggler.getElementsByTagName('i')[0].classList.remove('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.helpFields, function (helpField) {
            helpField.classList.remove('hidden');
          });
          this.dialog.body.classList.add('help-tooltip-enabled');
        } else {
          this.dialog.helpToggler.getElementsByTagName('i')[0].classList.add('fa-flip-horizontal', 'text-muted');
          Array.prototype.filter.call(this.helpFields, function (helpField) {
            helpField.classList.add('hidden');
          });
          this.dialog.body.classList.remove('help-tooltip-enabled');
        }
      }
    }

    /* eslint-disable max-statements */
  }, {
    key: "editComponent",
    value: function editComponent(component, isJsonEdit) {
      var _this6 = this;
      var tabKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      this.toggleAdvancedFields();
      this.toggleHelp();
      var componentDefaultValue = component.defaultValue;
      if (_lodash.default.isNil(componentDefaultValue)) {
        component.deleteValue();
      } else {
        component.setValue(componentDefaultValue, {
          noUpdateEvent: true
        });
      }
      var componentCopy = _lodash.default.cloneDeep(component);
      var componentClass = _Components.default.components[componentCopy.component.type];
      var isCustom = componentClass === undefined;
      //custom component should be edited as JSON
      isJsonEdit = isJsonEdit || isCustom || componentClass.useJsonEdit;
      componentClass = isCustom ? _Components.default.components.unknown : componentClass;
      // Make sure we only have one dialog open at a time.
      if (this.dialog) {
        this.dialog.close();
      }
      this.dialog = this.createModal(componentCopy.name);
      var formioForm = this.ce('div');
      if (!this.options.noComponentPreview) {
        this.componentPreview = this.ce('div', {
          class: 'component-preview'
        });
      }
      var componentInfo = componentClass ? componentClass.builderInfo : {};
      var saveButton = this.ce('button', {
        class: 'btn btn-primary',
        style: 'margin-right: 10px;',
        'data-event': 'save-component'
      }, this.t('general.save'));
      var topSaveButton = saveButton.cloneNode(true);
      var cancelButton = this.ce('button', {
        class: 'btn btn-default',
        style: 'margin-right: 10px;'
      }, this.t('general.cancel'));
      var topCancelButton = cancelButton.cloneNode(true);
      var removeButton = this.ce('button', {
        class: 'btn btn-danger'
      }, this.t('Remove'));
      var topRemoveButton = removeButton.cloneNode(true);
      this.dialog.helpToggler = this.ce('span', {
        class: 'modal-help-toggler'
      }, [this.ce('i', {
        class: 'fa fa-toggle-large-on fa-flip-horizontal toggle__icon ml-4 mr-2'
      }), this.t('Help')]);
      this.dialog.advancedToggler = this.ce('span', {
        class: 'modal-advanced-toggler'
      }, [this.ce('i', {
        class: 'fa fa-toggle-large-on fa-flip-horizontal toggle__icon ml-4 mr-2'
      }), this.t('Advanced')]);
      this.addEventListener(this.dialog.helpToggler, 'click', function (event) {
        event.preventDefault();
        _this6.options.showHelp = !_this6.options.showHelp;
        _this6.toggleHelp();
      });
      this.addEventListener(this.dialog.advancedToggler, 'click', function (event) {
        event.preventDefault();
        _this6.options.showAdvanced = !_this6.options.showAdvanced;
        _this6.toggleAdvancedFields();
      });
      var componentTitle = componentCopy.component.subType ? componentCopy.component.subType : componentInfo.title;
      var componentEdit = this.ce('div', {}, [this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col'
      }, this.ce('h1', {
        class: 'page-title'
      }, [component.isNew ? this.t("Add ".concat(componentTitle)) : this.t("Edit ".concat(componentTitle)), this.dialog.helpToggler, this.dialog.advancedToggler, topRemoveButton, topCancelButton, topSaveButton]))]), this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col col-sm-12'
      }, formioForm)])]);

      // Append the settings page to the dialog body.
      this.dialog.body.appendChild(componentEdit);

      // Allow editForm overrides per component.
      var overrides = _lodash.default.get(this.options, "editForm.".concat(componentCopy.component.type), []);

      // Get the editform for this component.
      var editForm;
      //custom component has its own Edit Form defined
      if (isJsonEdit && !isCustom) {
        editForm = {
          'components': [{
            'type': 'textarea',
            'as': 'json',
            'editor': 'ace',
            'weight': 10,
            'input': true,
            'key': 'componentJson',
            'label': 'Component JSON',
            'tooltip': 'Edit the JSON for this component.'
          }]
        };
      } else if (typeof componentClass.editForm === 'function') {
        editForm = componentClass.editForm(_lodash.default.cloneDeep(overrides));
      } else {
        var componentEditForm = _lodash.default.cloneDeep(componentClass.editForm || []);
        if (!Array.isArray(componentEditForm)) {
          componentEditForm = [];
        }
        editForm = (0, _Base.default)([].concat(_toConsumableArray(componentEditForm), _toConsumableArray(_lodash.default.cloneDeep(overrides))));
      }

      // Change the defaultValue component to be reflective.
      this.defaultValueComponent = (0, _utils.getComponent)(editForm.components, 'defaultValue');
      if (this.defaultValueComponent) {
        var defaultValueComponentType = this.defaultValueComponent.type;
        _lodash.default.assign(this.defaultValueComponent, _lodash.default.omit(componentCopy.component, ['key', 'label', 'disableAddingRows', 'disableRemovingRows', 'disableAddingRemovingRows', 'placeholder', 'tooltip', 'validate', 'disabled', 'hidden', 'fields.day.required', 'fields.month.required', 'fields.year.required', 'defaultValue', 'defaultDate', 'clearOnHide']));
        if (Array.isArray(this.defaultValueComponent.customClasses) && this.defaultValueComponent.customClasses.length > 0) {
          this.defaultValueComponent.customClass = this.defaultValueComponent.customClasses.filter(function (c) {
            return !['hide', 'hidden'].includes(c);
          }).join(' ');
          delete this.defaultValueComponent.customClasses;
        } else if (this.defaultValueComponent.customClass) {
          var customClasses = this.defaultValueComponent.customClass.split(/\s+/).filter(function (c) {
            return !['hide', 'hidden'].includes(c);
          });
          this.defaultValueComponent.customClass = customClasses.join(' ');
        }
        if (typeof _Components.default.components[this.defaultValueComponent.type] !== 'function') {
          this.defaultValueComponent.type = defaultValueComponentType || this.defaultValueComponent.type;
        }
        if (typeof _Components.default.components[this.defaultValueComponent.type] !== 'function') {
          this.defaultValueComponent.type = 'textarea';
          this.defaultValueComponent.editor = 'ace';
        }
        if (this.defaultValueComponent.type === 'checkbox') {
          this.defaultValueComponent.label = 'Checked by default';
        }
        if (this.defaultValueComponent.components) {
          this.defaultValueComponent.components = _lodash.default.cloneDeep(this.defaultValueComponent.components);
          (0, _utils.eachComponent)(this.defaultValueComponent.components, function (childComp) {
            if (childComp.validate) {
              delete childComp.validate;
            }
          }, true);
        }
      }

      // Create the form instance.
      var editFormOptions = _lodash.default.get(this, 'options.editForm', {});
      this.editForm = new _Webform2.default(formioForm, _objectSpread({
        language: this.options.language
      }, editFormOptions));
      if (component.isNew) this.editForm.element.classList.add('is-new');

      // Set the form to the edit form.
      this.editForm.form = editForm;

      // Pass along the form being edited.
      this.editForm.editForm = this._form;
      this.editForm.editComponent = component;

      // Update the preview with this component.
      this.updateComponent(componentCopy);

      // Register for when the edit form changes.
      this.editForm.on('change', function (event) {
        _this6.toggleAdvancedFields();
        _this6.toggleHelp();
        if (event.changed) {
          // See if this is a manually modified key. Treat JSON edited component keys as manually modified
          if (event.changed.component && event.changed.component.key === 'key' || isJsonEdit) {
            componentCopy.keyModified = true;
          }

          // Set the component JSON to the new data.
          var editFormData = _this6.editForm.getValue().data;
          //for custom component use value in 'componentJson' field as JSON of component
          if ((editFormData.type === 'custom' || isJsonEdit) && editFormData.componentJson) {
            componentCopy.component = editFormData.componentJson;
          } else {
            componentCopy.component = editFormData;
          }

          // Update the component.
          _this6.updateComponent(componentCopy);
        }
      });

      // Modify the component information in the edit form.
      this.editForm.formReady.then(function () {
        //for custom component populate component setting with component JSON
        if (isJsonEdit) {
          _this6.editForm.setValue({
            data: {
              componentJson: _lodash.default.cloneDeep(componentCopy.component)
            }
          });
        } else {
          _this6.editForm.setValue({
            data: componentCopy.component
          });
          if (tabKey) {
            var tabsComp = _this6.editForm.getComponent('tabs');
            if (tabsComp && tabsComp.component && Array.isArray(tabsComp.component.components)) {
              // eslint-disable-next-line max-depth
              for (var i = 0; i < tabsComp.component.components.length; i++) {
                // eslint-disable-next-line max-depth
                if (tabsComp.component.components[i].key === tabKey) {
                  tabsComp.setTab(i);
                  break;
                }
              }
            }
          }
        }
        _this6.toggleAdvancedFields();
        _this6.toggleHelp();
        if (_this6.defaultValueComponent) {
          _this6.editForm.emit('defaultValueComponentChanged');
        }
      });
      this.addEventListener(topCancelButton, 'click', function (event) {
        event.preventDefault();
        _this6.emit('cancelComponent', component);
        _this6.dialog.close();
      });
      this.addEventListener(cancelButton, 'click', function (event) {
        event.preventDefault();
        _this6.emit('cancelComponent', component);
        _this6.dialog.close();
      });
      this.addEventListener(topRemoveButton, 'click', function (event) {
        event.preventDefault();
        component.deleteValue();
        _this6.deleteComponent(component);
        _this6.dialog.close();
      });
      this.addEventListener(removeButton, 'click', function (event) {
        event.preventDefault();
        component.deleteValue();
        _this6.deleteComponent(component);
        _this6.dialog.close();
      });
      this.addEventListener(saveButton, 'click', function (event) {
        event.preventDefault();
        if (!_this6.editForm.checkValidity(_this6.editForm.data, true)) {
          console.log('Form data is not valid! Cancelling saving!', _this6.editForm.data);
          var errors = _this6.editForm.errors;
          console.log('Validation errors', errors);
          _this6.emit('validationErrors', errors, component);
          return;
        }
        var originalComponent = component.schema;
        component.isNew = false;
        component.deleteValue();
        //for JSON Edit use value in 'componentJson' field as JSON of component
        if (isJsonEdit) {
          component.component = _this6.editForm.data.componentJson;
        } else {
          component.component = componentCopy.component;
        }
        if (component.dragEvents && component.dragEvents.onSave) {
          component.dragEvents.onSave(component);
        }
        _this6.form = _this6.schema;
        _this6.emit('saveElement', _this6.editForm.data);
        _this6.emit('saveComponent', component, originalComponent);
        _this6.dialog.close();
      });
      this.addEventListener(topSaveButton, 'click', function (event) {
        event.preventDefault();
        if (!_this6.editForm.checkValidity(_this6.editForm.data, true)) {
          console.log('Form data is not valid! Cancelling saving!', _this6.editForm.data);
          var errors = _this6.editForm.errors;
          console.log('Validation errors', errors);
          _this6.emit('validationErrors', errors, component);
          return;
        }
        var originalComponent = component.schema;
        component.isNew = false;
        component.deleteValue();
        //for JSON Edit use value in 'componentJson' field as JSON of component
        if (isJsonEdit) {
          component.component = _this6.editForm.data.componentJson;
        } else {
          component.component = componentCopy.component;
        }
        if (component.dragEvents && component.dragEvents.onSave) {
          component.dragEvents.onSave(component);
        }
        _this6.form = _this6.schema;
        _this6.emit('saveElement', _this6.editForm.data);
        _this6.emit('saveComponent', component, originalComponent);
        _this6.dialog.close();
      });
      this.addEventListener(this.dialog, 'close', function () {
        _this6.editForm.destroy(true);
        if (_this6.componentPreview) {
          _this6.preview.destroy(true);
        }
        _this6.dialog.remove();
        if (component.isNew) {
          _this6.deleteComponent(component);
        }
      });

      // Called when we edit a component.
      this.emit('editComponent', component);
    }
    /* eslint-enable max-statements */

    /**
     * Creates copy of component schema and stores it under clipboard.
     * @param {Component} component
     * @return {*}
     */
  }, {
    key: "copyComponent",
    value: function copyComponent(component) {
      var _this7 = this;
      this.addClass(this.element, 'builder-paste-mode');
      var copy = {
        'formio.clipboard': {
          component: _lodash.default.cloneDeep(component.schema),
          origin: this.options.BASE_URL,
          serviceId: this.options.serviceId
        }
      };
      navigator.clipboard.writeText(JSON.stringify(copy)).then(function (message) {
        _this7.emit('notify', 'Component schema added to clipboard!', component);
      });
    }

    /**
     * Paste copied component after the current component.
     * @param {Component} component
     * @return {*}
     */
  }, {
    key: "pasteComponent",
    value: function () {
      var _pasteComponent = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(component) {
        var _this8 = this;
        var transform,
          data,
          pastedObject,
          schema,
          idMapping,
          tabItemsWithDeterminantIds,
          formDeterminants,
          keyMap,
          newDeterminantIds,
          _iterator,
          _step,
          subSchema,
          i,
          _args3 = arguments;
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              transform = _args3.length > 1 && _args3[1] !== undefined ? _args3[1] : false;
              _context3.next = 3;
              return navigator.clipboard.readText();
            case 3:
              data = _context3.sent;
              _context3.prev = 4;
              data = JSON.parse(data);
              pastedObject = data && data['formio.clipboard'] && data['formio.clipboard'];
              if (!(!pastedObject || !pastedObject.component)) {
                _context3.next = 10;
                break;
              }
              this.emit('validationErrors', [{
                message: 'Paste failed! Clipboard does not contain formio component'
              }], component);
              return _context3.abrupt("return");
            case 10:
              if (!(transform && (pastedObject.serviceId !== this.options.serviceId || pastedObject.origin !== this.options.BASE_URL))) {
                _context3.next = 13;
                break;
              }
              this.emit('validationErrors', [{
                message: 'Paste failed! Paste with transformation is allowed only when copying component from the same service'
              }], component);
              return _context3.abrupt("return");
            case 13:
              this.removeClass(this.element, 'builder-paste-mode');
              schema = pastedObject.component;
              idMapping = false;
              (0, _utils.cleanPastedSchema)(schema, transform);
              (0, _utils.formatMoustacheSchema)(schema, (0, _utils.isInsideGrid)(component));
              navigator.clipboard.writeText('').then(function (message) {});
              this.options.pasteMode = false;
              if (!transform) {
                _context3.next = 29;
                break;
              }
              tabItemsWithDeterminantIds = [];
              (0, _utils.eachComponent)([schema], function (comp) {
                if ((!comp.type || comp.type === 'tab') && Array.isArray(comp.components) && comp.condition > 0) {
                  tabItemsWithDeterminantIds.push(comp.id);
                }
              }, true);
              idMapping = {};
              if (!(tabItemsWithDeterminantIds.length > 0)) {
                _context3.next = 29;
                break;
              }
              _context3.next = 27;
              return Promise.all(tabItemsWithDeterminantIds.map(function (id) {
                return _this8.options.methods.generateFormDeterminantOutOfOtherRelation('TabItem', id);
              }));
            case 27:
              formDeterminants = _context3.sent;
              formDeterminants.forEach(function (fd, index) {
                idMapping[tabItemsWithDeterminantIds[index]] = {
                  id: fd.id,
                  determinantIds: _this8.options.methods.findAllPossibleDeterminantIds(fd.jsonDeterminant)
                };
              });
            case 29:
              keyMap = _builder.default.uniquify(this._form, schema, _lodash.default.camelCase(this.options.formName), true, transform, idMapping);
              if (!transform) {
                _context3.next = 35;
                break;
              }
              _context3.next = 33;
              return this.options.methods.generateDeterminantForNewKeys(_builder.default.extractNecessaryProperties(schema), keyMap);
            case 33:
              newDeterminantIds = _context3.sent;
              (0, _utils.eachComponent)([schema], function (comp) {
                if (comp.formDeterminantId && newDeterminantIds[comp.formDeterminantId]) {
                  comp.formDeterminantId = newDeterminantIds[comp.formDeterminantId];
                }
                if (comp.determinantIds && Array.isArray(comp.determinantIds)) {
                  comp.determinantIds.forEach(function (id, index) {
                    if (newDeterminantIds[id]) comp.determinantIds[index] = newDeterminantIds[id];
                  });
                }
              }, true);
            case 35:
              // TODO following is needed for TOBE-11818
              //         const prePaste = {
              //           keyMap: BuilderUtils.uniquify(this._form, schema, _.camelCase(this.options.formName), true),
              //           origin:  data['formio.clipboard'].origin,
              //           serviceId:  data['formio.clipboard'].serviceId,
              //           formName:  _.camelCase(this.options.formName),
              //           pasteAfter: component.key,
              //           pastedComponent: schema
              //         };

              // If this is an empty "nested" component, and it is empty, then paste the component inside this component.
              if (typeof component.addComponent === 'function' && !component.components.length) {
                if (schema.type === 'form' || schema.type === 'wizard') {
                  // eslint-disable-next-line max-depth
                  if (Array.isArray(schema.components)) {
                    // eslint-disable-next-line max-depth
                    _iterator = _createForOfIteratorHelper(schema.components);
                    try {
                      for (_iterator.s(); !(_step = _iterator.n()).done;) {
                        subSchema = _step.value;
                        component.addComponent(subSchema);
                      }
                    } catch (err) {
                      _iterator.e(err);
                    } finally {
                      _iterator.f();
                    }
                  }
                } else {
                  component.addComponent(schema);
                }
              } else {
                if (schema.type === 'form' || schema.type === 'wizard') {
                  // eslint-disable-next-line max-depth
                  if (Array.isArray(schema.components)) {
                    // eslint-disable-next-line max-depth
                    for (i = schema.components.length - 1; i >= 0; i--) {
                      component.parent.addComponent(schema.components[i], false, false, component.element.nextSibling);
                    }
                  }
                } else {
                  component.parent.addComponent(schema, false, false, component.element.nextSibling);
                }
              }
              this.form = this.schema;
              this.emit('pasteComponent', component);
              _context3.next = 44;
              break;
            case 40:
              _context3.prev = 40;
              _context3.t0 = _context3["catch"](4);
              console.error('error happened', _context3.t0);
              this.emit('validationErrors', [{
                message: 'Paste failed! Please try to copy again'
              }], component);
            case 44:
            case "end":
              return _context3.stop();
          }
        }, _callee3, this, [[4, 40]]);
      }));
      function pasteComponent(_x) {
        return _pasteComponent.apply(this, arguments);
      }
      return pasteComponent;
    }()
  }, {
    key: "destroy",
    value: function destroy() {
      var state = _get(_getPrototypeOf(WebformBuilder.prototype), "destroy", this).call(this);
      if (this.dragula) {
        this.dragula.destroy();
      }
      return state;
    }

    /**
     * Insert an element in the weight order.
     *
     * @param info
     * @param items
     * @param element
     * @param container
     */
  }, {
    key: "insertInOrder",
    value: function insertInOrder(info, items, element, container) {
      // Determine where this item should be added.
      var beforeWeight = 0;
      var before = null;
      _lodash.default.each(items, function (itemInfo) {
        if (info.key !== itemInfo.key && info.weight < itemInfo.weight && (!beforeWeight || itemInfo.weight < beforeWeight)) {
          before = itemInfo.element;
          beforeWeight = itemInfo.weight;
        }
      });
      if (before) {
        try {
          container.insertBefore(element, before);
        } catch (err) {
          container.appendChild(element);
        }
      } else {
        container.appendChild(element);
      }
    }
  }, {
    key: "getFieldIcon",
    value: function getFieldIcon(type) {
      if (type) {
        switch (type.toLowerCase()) {
          case 'panel':
          case 'block':
            return 'far fa-window';
          case 'editgrid':
          case 'datagrid':
            return 'far fa-th';
          case 'table':
            return 'far fa-table';
          case null:
          default:
            return false;
        }
      } else {
        return false;
      }
    }
  }, {
    key: "addBuilderGroup",
    value: function addBuilderGroup(info, container) {
      var _this9 = this;
      var fromAsyncContext = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var path = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      if (!info || !info.key) {
        console.warn('Invalid Group Provided.');
        return;
      }
      if (!path) {
        path = '';
      }
      var currentPath = path ? "".concat(path, ".").concat(info.key) : info.key;
      info.uKey = (0, _stringHash.default)(currentPath);
      info = _lodash.default.clone(info);
      var ico = '';
      if (this.getFieldIcon(info.type)) {
        ico = this.ce('i', {
          class: "".concat(this.getFieldIcon(info.type), " branch-bloc-icon mr-2")
        }, '');
        if (info.disabled) {
          new _tooltip.default(ico, {
            trigger: 'hover',
            placement: 'left',
            container: ico,
            title: this.t('Not in roles flow'),
            popperOptions: {
              positionFixed: true
            }
          });
        }
      }
      var moustach = '';
      if (info.hasMoustach) {
        moustach = this.ce('i', {
          class: 'working-title'
        }, '');
        new _tooltip.default(moustach, {
          trigger: 'hover',
          placement: 'right',
          container: 'body',
          title: info.name,
          popperOptions: {
            positionFixed: true
          }
        });
      }
      var groupAnchor = this.ce('button', {
        class: 'btn btn-block builder-group-button no-drag',
        'type': 'button',
        'data-toggle': 'collapse',
        'data-parent': "#".concat(container.id),
        'data-target': "#group-".concat(info.uKey)
      }, [ico, this.text(info.title), moustach]);
      var initiateCopy = '';
      if (!info.noCopy && info.type !== 'form' && info.key !== 'allFields') {
        initiateCopy = this.ce('span', {
          class: 'copyComponent fa fa-copy'
        });
        this.addEventListener(initiateCopy, 'click', function () {
          document.dispatchEvent(new CustomEvent('formioCopyComponent', {
            detail: info
          }));
          return _this9.copyComponent(info);
        }, true);
      }

      // Add a listener when it is clicked.
      if (!(0, _utils.bootstrapVersion)(this.options)) {
        this.addEventListener(groupAnchor, 'click', function (event) {
          event.preventDefault();
          var clickedGroupId = event.target.getAttribute('data-target').replace('#group-', '');
          if (_this9.groups[clickedGroupId]) {
            var clickedGroup = _this9.groups[clickedGroupId];
            var wasIn = _this9.hasClass(clickedGroup.panel, 'in');
            if (wasIn) {
              _this9.removeClass(clickedGroup.panel, 'in');
              _this9.removeClass(clickedGroup.panel, 'show');
            } else {
              var previousGroup = null;
              var parent = clickedGroup;
              while (parent) {
                _this9.addClass(parent.panel, 'in');
                _this9.addClass(parent.panel, 'show');
                // eslint-disable-next-line max-depth
                if (parent.groups) {
                  // eslint-disable-next-line max-depth
                  for (var _i = 0, _Object$values = Object.values(parent.groups); _i < _Object$values.length; _i++) {
                    var subGroup = _Object$values[_i];
                    // eslint-disable-next-line max-depth
                    if (previousGroup && previousGroup !== subGroup) {
                      _this9.removeClass(subGroup.panel, 'in');
                      _this9.removeClass(subGroup.panel, 'show');
                    }
                  }
                }
                previousGroup = parent;
                parent = parent.parent;
              }
            }
            // Match the form builder height to the sidebar.
            _this9.element.style.minHeight = "".concat(_this9.builderSidebar.offsetHeight, "px");
            _this9.scrollSidebar();
            event.stopPropagation();
          }
        }, true);
      }
      var panelClass = 'card panel panel-default form-builder-panel no-drag';
      if (info.disabled) {
        panelClass += ' disabled';
      }
      info.element = this.ce('div', {
        class: panelClass,
        id: "group-panel-".concat(_lodash.default.camelCase(info.key))
      }, [this.ce('div', {
        class: 'card-header panel-heading form-builder-group-header no-drag'
      }, [this.ce('h5', {
        class: 'mb-0 panel-title'
      }, [groupAnchor, initiateCopy])])]);
      info.body = this.ce('div', {
        id: "group-container-".concat(_lodash.default.camelCase(info.key)),
        class: 'card-body panel-body no-drop'
      });

      // Add this group body to the drag containers.
      this.sidebarContainers.push(info.body);
      var groupBodyClass = 'panel-collapse collapse';
      if (info.default) {
        switch ((0, _utils.bootstrapVersion)(this.options)) {
          case 4:
            groupBodyClass += ' show';
            break;
          case 3:
            groupBodyClass += ' in';
            break;
          default:
            groupBodyClass += ' in show';
            break;
        }
      }
      info.panel = this.ce('div', {
        class: groupBodyClass,
        'data-parent': "#".concat(container.id),
        id: "group-".concat(_lodash.default.camelCase(info.key))
      }, info.body);
      info.element.appendChild(info.panel);
      this.groups[info.uKey] = info;
      this.insertInOrder(info, this.groups, info.element, container);
      var cb = function cb() {
        var asyncContext = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
        // Now see if this group has subgroups.
        if (info.groups) {
          _lodash.default.each(info.groups, function (subInfo, subGroup) {
            subInfo.key = subGroup;
            subInfo.parent = info;
            _this9.addBuilderGroup(subInfo, info.body, asyncContext, currentPath);
          });
        }
        if (asyncContext) {
          _lodash.default.each(info.components, function (comp, key) {
            if (comp && !(comp.components && comp.components.length)) {
              _this9.addBuilderComponent(comp, info, currentPath);
            }
          });
          if ((!info.components || !Object.keys(info.components).length) && (!info.groups || !Object.keys(info.groups).length)) {
            _this9.addBuilderComponent({
              key: 'empty',
              title: _this9.t('No choices to choose from'),
              noOptions: true,
              noCopy: true
            }, info, currentPath);
          }
        }
      };
      if (info.loadOnDemand) {
        var loadComponents = function loadComponents(loadedInfo) {
          info.alreadyLoaded = true;
          Object.assign(info, loadedInfo);
          _this9.groups[info.uKey] = info;
          // eslint-disable-next-line callback-return
          cb(true);
          groupAnchor.classList.remove('preload');
          _this9.updateDraggable();
        };
        this.addEventListener(groupAnchor, 'click', function (event) {
          event.preventDefault();
          if (info.alreadyLoaded) return;
          groupAnchor.classList.add('preload');
          return info.loadOnDemand(loadComponents);
        }, true);
      } else {
        return cb(fromAsyncContext);
      }
    }
  }, {
    key: "addBuilderComponentInfo",
    value: function addBuilderComponentInfo(component) {
      if (!component || !component.group || !this.groups[component.group]) {
        return;
      }
      component = _lodash.default.clone(component);
      var groupInfo = this.groups[component.group];
      if (!groupInfo.components) {
        groupInfo.components = {};
      }
      if (!groupInfo.components.hasOwnProperty(component.key)) {
        groupInfo.components[component.key] = component;
      }
      return component;
    }
  }, {
    key: "addBuilderComponent",
    value: function addBuilderComponent(component, group) {
      var _this10 = this;
      var path = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      if (!component) {
        return;
      }
      if (!group && component.group && this.groups[component.group]) {
        group = this.groups[component.group];
      }
      if (!group || component.groups && Object.keys(component.groups).length > 1) {
        return;
      }
      if (!path) {
        path = '';
      }
      var compClass = "btn btn-primary btn-xs btn-block formcomponent no-drag ".concat(component.type);
      if (component.disabled) {
        compClass += ' disabled';
      }
      var currentPath = path ? "".concat(path, ".").concat(component.key) : component.key;
      component.uKey = (0, _stringHash.default)(currentPath);
      component.element = this.ce('span', {
        id: "builder-".concat(component.uKey),
        class: compClass
      });
      if (component.icon) {
        var ico = this.ce('i', {
          class: "".concat(component.icon, " mr2")
        });
        if (component.disabled) {
          new _tooltip.default(ico, {
            trigger: 'hover',
            placement: 'left',
            container: ico,
            title: this.t('Not in roles flow'),
            popperOptions: {
              positionFixed: true
            }
          });
        }
        component.element.appendChild(ico);
      }
      var copyMoustash = this.ce('span', {
        class: component.noOptions ? '' : 'copyMoustach far fa-brackets-curly'
      });
      component.element.appendChild(copyMoustash);
      if (!component.noOptions) {
        this.addEventListener(copyMoustash, 'click', function () {
          var variable = "".concat((0, _utils.isSchemaInsideGrid)(group) ? 'row' : 'data', ".").concat(component.key);
          if (component.type === 'select') {
            variable += '.value';
          }
          if (component.key.endsWith('_child_value') || component.key.endsWith('_child_key')) {
            variable = variable.replace(/_child_(?!.*_child_)/, '.');
          }
          navigator.clipboard.writeText("{{".concat(variable, "}}"));
        }, true);
      }
      if (!component.noCopy) {
        var initiateCopy = this.ce('span', {
          class: 'copyComponent fa fa-copy'
        });
        new _tooltip.default(initiateCopy, {
          trigger: 'hover',
          placement: 'top',
          container: initiateCopy,
          title: this.t('Copy'),
          popperOptions: {
            positionFixed: true
          }
        });
        component.element.appendChild(initiateCopy);
        this.addEventListener(initiateCopy, 'click', function () {
          document.dispatchEvent(new CustomEvent('formioCopyComponent', {
            detail: component
          }));
          return _this10.copyComponent(component);
        }, true);
      }
      component.element.builderInfo = component;
      var moustach = '';
      if (component.workingLabel) {
        moustach = this.ce('i', {
          class: 'working-title'
        }, '');
        new _tooltip.default(moustach, {
          trigger: 'hover',
          placement: 'right',
          container: 'body',
          title: component.name,
          popperOptions: {
            positionFixed: true
          }
        });
      }
      var elementLabel = this.ce('span', {
        class: 'btn-label'
      }, [this.text(component.title), moustach]);
      component.element.appendChild(elementLabel);
      this.insertInOrder(component, group.components, component.element, group.body);
      return component;
    }
  }, {
    key: "addBuilderButton",
    value: function addBuilderButton(info, container) {
      var _this11 = this;
      var button;
      if (!info.uKey) {
        info.uKey = (0, _stringHash.default)(info.key);
      }
      info.element = this.ce('div', {
        style: 'margin: 5px 0;'
      }, button = this.ce('span', {
        class: "btn btn-block ".concat(info.style || 'btn-default')
      }, info.title));
      // Make sure it persists across refreshes.
      this.addEventListener(button, 'click', function () {
        return _this11.emit(info.event);
      }, true);
      this.groups[info.uKey] = info;
      this.insertInOrder(info, this.groups, info.element, container);
    }
  }, {
    key: "buildSidebar",
    value: function buildSidebar() {
      var _this12 = this;
      // Do not rebuild the sidebar.
      if (this.sideBarElement || this.options.builder.noSideBar) {
        return;
      }
      this.groups = {};
      this.sidebarContainers = [];
      this.sideBarElement = this.ce('div', {
        id: "builder-sidebar-".concat(this.id),
        class: 'accordion panel-group'
      });
      this.searchInpuSidebar = this.ce('input', {
        class: 'form-control',
        placeholder: this.t('Search')
      });
      this.sideBarElement.appendChild(this.searchInpuSidebar);
      this.addEventListener(this.searchInpuSidebar, 'input', function (event) {
        event.stopPropagation();
        _this12.searchSidebarGroups(_this12.searchInpuSidebar.value.toLowerCase());
      }, true);
      this.addEventListener(this.searchInpuSidebar, 'click', function (event) {
        event.preventDefault();
        event.stopPropagation();
      }, true);
      // Add the groups.
      _lodash.default.each(this.options.builder, function (info, group) {
        if (info) {
          info.key = group;
          if (info.type === 'button') {
            _this12.addBuilderButton(info, _this12.sideBarElement);
          } else {
            _this12.addBuilderGroup(info, _this12.sideBarElement);
          }
        }
      });

      // Get all of the components builder info grouped and sorted.
      var components = {};
      var allComponents = _lodash.default.filter(_lodash.default.map(_Components.default.components, function (component, type) {
        if (!component.builderInfo) {
          return null;
        }
        component.type = type;
        return component;
      }));
      _lodash.default.map(_lodash.default.sortBy(allComponents, function (component) {
        return component.builderInfo.weight;
      }), function (component) {
        var builderInfo = component.builderInfo;
        builderInfo.key = component.type;
        components[builderInfo.key] = builderInfo;
        _this12.addBuilderComponentInfo(builderInfo);
      });

      // Add the components in each group.
      _lodash.default.each(this.groups, function (info) {
        if (info.loadOnDemand) return;
        _lodash.default.each(info.components, function (comp, key) {
          if (comp) {
            _this12.addBuilderComponent(comp === true ? components[key] : comp, info);
          }
        });
      });

      // Add the new sidebar element.
      this.builderSidebar.appendChild(this.sideBarElement);
      this.updateDraggable();
      this.sideBarTop = this.sideBarElement.getBoundingClientRect().top + window.scrollY;
    }
  }, {
    key: "getParentElement",
    value: function getParentElement(element) {
      var containerComponent = element;
      do {
        containerComponent = containerComponent.parentNode;
      } while (containerComponent && !containerComponent.component);
      return containerComponent;
    }
  }, {
    key: "addDragContainer",
    value: function addDragContainer(element, component, dragEvents) {
      _lodash.default.remove(this.dragContainers, function (container) {
        return element.id && element.id === container.id;
      });
      element.component = component;
      if (dragEvents) {
        element.dragEvents = dragEvents;
      }
      this.addClass(element, 'drag-container');
      if (!element.id) {
        element.id = "builder-element-".concat(component.id);
      }
      this.dragContainers.push(element);
      this.updateDraggable();
    }
  }, {
    key: "searchSidebarGroups",
    value: function searchSidebarGroups(text) {
      var groups = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var someMatched = false;
      var genericComps = Array.isArray(groups) ? groups : Object.values(groups || this.groups);
      var _iterator2 = _createForOfIteratorHelper(genericComps),
        _step2;
      try {
        for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
          var _this$groups$genericC;
          var genericComp = _step2.value;
          if (!groups && genericComp.parent) {
            continue;
          }
          var visible = true;
          if (text) {
            var match = (genericComp.title || genericComp.name || genericComp.label || '').toLowerCase().includes(text);
            if (genericComp.groups) {
              // eslint-disable-next-line max-depth
              if (this.searchSidebarGroups(text, genericComp.groups)) {
                match = true;
              }
            }
            if (genericComp.components) {
              // eslint-disable-next-line max-depth
              if (this.searchSidebarGroups(text, genericComp.components)) {
                match = true;
              }
            }
            someMatched = someMatched || match;
            if (!match) {
              visible = false;
            }
          } else {
            if (genericComp.groups) {
              this.searchSidebarGroups(text, genericComp.groups);
            }
            if (genericComp.components) {
              this.searchSidebarGroups(text, genericComp.components);
            }
            someMatched = true;
          }
          var element = genericComp.element || ((_this$groups$genericC = this.groups[genericComp.uKey]) === null || _this$groups$genericC === void 0 ? void 0 : _this$groups$genericC.element);
          if (element) {
            if (visible) {
              // eslint-disable-next-line max-depth
              if (element.classList.contains('d-none')) {
                element.classList.remove('d-none');
              }
            } else if (!element.classList.contains('d-none')) {
              element.classList.add('d-none');
            }
          }
        }
      } catch (err) {
        _iterator2.e(err);
      } finally {
        _iterator2.f();
      }
      return someMatched;
    }
  }, {
    key: "clear",
    value: function clear() {
      this.dragContainers = [];
      return _get(_getPrototypeOf(WebformBuilder.prototype), "clear", this).call(this);
    }
  }, {
    key: "addComponentTo",
    value: function addComponentTo(schema, parent, element, sibling, after) {
      var component = parent.addComponent(schema, element, parent.data, sibling);
      if (after) {
        after(component);
      }

      // Get path to the component in the parent component.
      var path = 'components';
      switch (component.parent.type) {
        case 'table':
          path = "rows[".concat(component.tableRow, "][").concat(component.tableColumn, "].components");
          break;
        case 'columns':
          path = "columns[".concat(component.column, "].components");
          break;
        case 'tabs':
          path = "components[".concat(component.tab, "].components");
          break;
      }
      // Index within container
      var index = _lodash.default.findIndex(_lodash.default.get(component.parent.schema, path), {
        key: component.component.key
      }) || 0;
      this.emit('addComponent', component, path, index);
      return component;
    }

    /* eslint-disable  max-statements */
  }, {
    key: "onDrop",
    value: function onDrop(element, target, source, sibling) {
      if (!element || !element.id) {
        console.warn('No element.id defined for dropping');
        return;
      }
      var builderElement = source.querySelector("#".concat(element.id));
      var newParent = this.getParentElement(element);
      if (!newParent || !newParent.component) {
        return console.warn('Could not find parent component.');
      }

      // Remove any instances of the placeholder.
      var placeholder = document.getElementById("".concat(newParent.component.id, "-placeholder"));
      if (placeholder) {
        placeholder = placeholder.parentNode;
        placeholder.parentNode.removeChild(placeholder);
      }

      // If the sibling is the placeholder, then set it to null.
      if (sibling === placeholder) {
        sibling = null;
      }

      // Make this element go before the submit button if it is still on the builder.
      if (!sibling && this.submitButton && newParent.contains(this.submitButton.element)) {
        sibling = this.submitButton.element;
      }

      // If this is a new component, it will come from the builderElement
      if (builderElement && builderElement.builderInfo && !builderElement.builderInfo.schema && builderElement.builderInfo.builderInfo) {
        builderElement.builderInfo.schema = builderElement.builderInfo.builderInfo.schema;
      }
      if (builderElement && builderElement.builderInfo && builderElement.builderInfo.schema) {
        var componentSchema = _lodash.default.clone(builderElement.builderInfo.schema);
        if (this.options.defaultComponents) {
          if (this.options.defaultComponents['*']) {
            Object.assign(componentSchema, _lodash.default.clone(this.options.defaultComponents['*']));
          }
          if (componentSchema.type && this.options.defaultComponents[componentSchema.type]) {
            Object.assign(componentSchema, _lodash.default.clone(this.options.defaultComponents[componentSchema.type]));
          }
        }
        if (target.dragEvents && target.dragEvents.onDrop) {
          target.dragEvents.onDrop(element, target, source, sibling, componentSchema);
        }

        // Add the new component.
        var component = this.addComponentTo(componentSchema, newParent.component, newParent, sibling, function (comp) {
          // Set that this is a new component.
          comp.isNew = true;

          // Pass along the save event.
          if (target.dragEvents) {
            comp.dragEvents = target.dragEvents;
          }
        });

        // Edit the component.
        this.editComponent(component);

        // Remove the element.
        if (target.contains(element)) {
          target.removeChild(element);
        }
      }
      // Check to see if this is a moved component.
      else if (element.component) {
        var _componentSchema = element.component.schema;
        if (target.dragEvents && target.dragEvents.onDrop) {
          target.dragEvents.onDrop(element, target, source, sibling, _componentSchema);
        }

        // Remove the component from its parent.
        if (element.component.parent) {
          this.emit('deleteComponentForMoving', element.component);
          element.component.parent.removeComponent(element.component);
        }

        // Add the new component.
        var _component = this.addComponentTo(_componentSchema, newParent.component, newParent, sibling);
        if (target.dragEvents && target.dragEvents.onSave) {
          target.dragEvents.onSave(_component);
        }

        // Refresh the form.
        if (target.component && target.component.root && target.component.root !== this) {
          target.component.root.form = target.component.root.schema;
        }
        this.form = this.schema;
        this.emit('saveComponent', _component);
      }
    }
    /* eslint-enable  max-statements */

    /**
     * Adds a submit button if there are no components.
     */
  }, {
    key: "addSubmitButton",
    value: function addSubmitButton() {
      if (!this.getComponents().length) {
        this.submitButton = this.addComponent({
          type: 'button',
          label: this.t('Submit'),
          key: 'submit',
          size: 'md',
          block: false,
          action: 'submit',
          disableOnInvalid: true,
          theme: 'primary'
        });
      }
    }
  }, {
    key: "refreshDraggable",
    value: function refreshDraggable() {
      var _this13 = this;
      if (this.dragula) {
        this.dragula.destroy();
      }
      var dragables = this.sidebarContainers.concat(this.dragContainers).concat(this.groupBasic).concat(this.groupAdvanced).concat(this.groupLayout).concat(this.groupData).concat(this.groupCustom).concat(this.groupServiceForms);
      if (this.options.externalDragables) {
        dragables = dragables.concat(this.options.externalDragables);
      }
      var that = this;
      dragables.forEach(function (dragable) {
        if (!dragable) return;
        dragable.addEventListener('click', function (ev) {
          that.cancelDragAndDrop = true;
        });
      });
      this.dragula = (0, _dragula.default)(dragables, {
        moves: function moves(el) {
          if (el.classList.contains('no-drag')) {
            return false;
          }
          setTimeout(function () {
            if (that.cancelDragAndDrop) {
              that.cancelDragAndDrop = false;
              return;
            }
            document.dispatchEvent(new CustomEvent('formioFormBuilderDragStarted', {
              detail: that
            }));
          }, 300);
          return true;
        },
        copy: function copy(el) {
          return el.classList.contains('drag-copy');
        },
        accepts: function accepts(el, target) {
          return !el.contains(target) && !target.classList.contains('no-drop');
        }
      }).on('drop', function (element, target, source, sibling) {
        var result = _this13.onDrop(element, target, source, sibling);
        document.dispatchEvent(new CustomEvent('formioFormBuilderDragEnded', {
          detail: _this13
        }));
        return result;
      });

      // If there are no components, then we need to add a default submit button.
      //this.addSubmitButton();
      this.builderReadyResolve();
    }
  }, {
    key: "build",
    value: function build(state) {
      var _this14 = this;
      var cb = function cb() {
        _this14.buildSidebar();
        _get(_getPrototypeOf(WebformBuilder.prototype), "build", _this14).call(_this14, state);
        _this14.updateDraggable();
        _this14.formReadyResolve();
      };
      navigator.permissions.query({
        name: 'clipboard-read'
      }).then(function (permissionStatus) {
        // Check if permission is granted
        _this14.options.enableButtons.copy = permissionStatus.state === 'granted';
        permissionStatus.onchange = function () {
          location.reload();
        };
        return navigator.clipboard.readText().then(function (clipboard) {
          try {
            clipboard = JSON.parse(clipboard);
            _this14.options.pasteMode = clipboard && clipboard.hasOwnProperty('formio.clipboard');
          } catch (e) {
            _this14.options.pasteMode = false;
          }
          cb();
        }, function (error) {
          _this14.options.pasteMode = false;
          cb();
        });
      });
    }
  }]);
  return WebformBuilder;
}(_Webform2.default);
exports.default = WebformBuilder;