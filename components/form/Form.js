"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.reflect.set.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _Base = _interopRequireDefault(require("../base/Base"));
var _eventemitter = _interopRequireDefault(require("eventemitter2"));
var _nativePromiseOnly = _interopRequireDefault(require("native-promise-only"));
var _utils = require("../../utils/utils");
var _Formio = _interopRequireDefault(require("../../Formio"));
var _Form = _interopRequireDefault(require("../../Form"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }
function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new TypeError('failed to set property'); } return value; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var FormComponent = /*#__PURE__*/function (_BaseComponent) {
  _inherits(FormComponent, _BaseComponent);
  var _super = _createSuper(FormComponent);
  function FormComponent(component, options, data) {
    var _this;
    _classCallCheck(this, FormComponent);
    _this = _super.call(this, component, options, data);
    _this.subForm = null;
    _this.formSrc = '';
    _this.subFormReady = new _nativePromiseOnly.default(function (resolve, reject) {
      _this.subFormReadyResolve = resolve;
      _this.subFormReadyReject = reject;
    });
    _this.subFormLoaded = false;
    _this.subscribe();
    return _this;
  }
  _createClass(FormComponent, [{
    key: "dataReady",
    get: function get() {
      return this.subFormReady;
    }
  }, {
    key: "defaultSchema",
    get: function get() {
      return FormComponent.schema();
    }
  }, {
    key: "emptyValue",
    get: function get() {
      return {
        data: {}
      };
    }
  }, {
    key: "root",
    get: function get() {
      return this._root;
    },
    set: function set(inst) {
      this._root = inst;
      this.nosubmit = inst.nosubmit;
    }
  }, {
    key: "nosubmit",
    get: function get() {
      return this._nosubmit || false;
    },
    set: function set(value) {
      this._nosubmit = !!value;
      if (this.subForm) {
        this.subForm.nosubmit = !!value;
      }
    }
  }, {
    key: "currentForm",
    get: function get() {
      return this._currentForm;
    },
    set: function set(instance) {
      var _this2 = this;
      this._currentForm = instance;
      if (!this.subForm) {
        return;
      }
      this.subForm.getComponents().forEach(function (component) {
        component.currentForm = _this2;
      });
    }
  }, {
    key: "subscribe",
    value: function subscribe() {
      var _this3 = this;
      this.on('nosubmit', function (value) {
        _this3.nosubmit = value;
      });
    }
  }, {
    key: "destroy",
    value: function destroy() {
      var state = _get(_getPrototypeOf(FormComponent.prototype), "destroy", this).call(this) || {};
      if (this.subForm) {
        this.subForm.destroy();
      }
      return state;
    }

    /**
     * Render a subform.
     *
     * @param form
     * @param options
     */
  }, {
    key: "renderSubForm",
    value: function renderSubForm(form, options) {
      var _this4 = this;
      if (this.options.builder) {
        this.element.appendChild(this.ce('div', {
          class: 'text-muted text-center p-2'
        }, this.text(form.title)));
        return;
      }
      options.events = this.createEmitter();

      // Iterate through every component and hide the submit button.
      (0, _utils.eachComponent)(form.components, function (component) {
        if (component.type === 'button' && (component.action === 'submit' || !component.action)) {
          component.hidden = true;
        }
      });
      this.subForm = new _Form.default(this.element, form, options).create();
      this.subForm.formReady.then(function () {
        return _this4.subForm.root = _this4.root;
      });
      this.subForm.currentForm = this;
      this.subForm.parent = this;
      this.subForm.parentVisible = this.visible;
      if (!this.options.temporary) {
        this.subForm.on('change', function () {
          _this4.dataValue = _this4.subForm.getValue();
          _this4.triggerChange({
            noEmit: true
          });
        });
      }
      this.subForm.url = this.formSrc;
      this.subForm.nosubmit = this.nosubmit;
      this.restoreValue();
      if (!this.isHidden()) {
        this.subForm.form = form;
      }
      this.subFormReadyResolve(this.subForm);
      return this.subFormReady;
    }
  }, {
    key: "clearOnHide",
    value: function clearOnHide(show) {
      _get(_getPrototypeOf(FormComponent.prototype), "clearOnHide", this).call(this, show);
      if (this.subForm && this.component.clearOnHide) {
        this.subForm.clearOnHide(show);
        this.subForm.triggerChange({
          noEmit: true
        });
      }
    }
  }, {
    key: "show",
    value: function show() {
      var _get2;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      var state = (_get2 = _get(_getPrototypeOf(FormComponent.prototype), "show", this)).call.apply(_get2, [this].concat(args));
      if (!this.subFormLoaded) {
        if (state) {
          this.loadSubForm();
        }
        // If our parent is read-only and is done loading, and we were never asked
        // to load a subform, consider our subform loading promise resolved
        else if (this.parent.options.readOnly && !this.parent.loading) {
          this.subFormReadyResolve(this.subForm);
        }
      }
      return state;
    }

    /**
     * Load the subform.
     */
    /* eslint-disable max-statements */
  }, {
    key: "loadSubForm",
    value: function loadSubForm() {
      var _this5 = this;
      // Only load the subform if the subform isn't loaded and the conditions apply.
      if (this.subFormLoaded) {
        return this.subFormReady;
      }
      this.subFormLoaded = true;
      var srcOptions = {};
      if (this.options && this.options.base) {
        srcOptions.base = this.options.base;
      }
      if (this.options && this.options.project) {
        srcOptions.project = this.options.project;
      }
      if (this.options && this.options.readOnly) {
        srcOptions.readOnly = this.options.readOnly;
      }
      if (this.options && this.options.breadcrumbSettings) {
        srcOptions.breadcrumbSettings = this.options.breadcrumbSettings;
      }
      if (this.options && this.options.buttonSettings) {
        srcOptions.buttonSettings = this.options.buttonSettings;
      }
      if (this.options && this.options.icons) {
        srcOptions.icons = this.options.icons;
      }
      if (this.options && this.options.viewAsHtml) {
        srcOptions.viewAsHtml = this.options.viewAsHtml;
      }
      if (this.options && this.options.hide) {
        srcOptions.hide = this.options.hide;
      }
      if (this.options && this.options.show) {
        srcOptions.show = this.options.show;
      }
      if (_lodash.default.has(this.options, 'language')) {
        srcOptions.language = this.options.language;
      }
      if (this.component.src) {
        this.formSrc = this.component.src;
      }
      if (!this.component.src && !this.options.formio && (this.component.form || this.component.path)) {
        if (this.component.project) {
          this.formSrc = _Formio.default.getBaseUrl();
          // Check to see if it is a MongoID.
          if ((0, _utils.isMongoId)(this.component.project)) {
            this.formSrc += '/project';
          }
          this.formSrc += "/".concat(this.component.project);
          srcOptions.project = this.formSrc;
        } else {
          this.formSrc = _Formio.default.getProjectUrl();
          srcOptions.project = this.formSrc;
        }
        if (this.component.form) {
          this.formSrc += "/form/".concat(this.component.form);
        } else if (this.component.path) {
          this.formSrc += "/".concat(this.component.path);
        }
      }

      // Build the source based on the root src path.
      if (!this.formSrc && this.options.formio) {
        var rootSrc = this.options.formio.formsUrl;
        if (this.component.path) {
          var parts = rootSrc.split('/');
          parts.pop();
          this.formSrc = "".concat(parts.join('/'), "/").concat(this.component.path);
        }
        if (this.component.form) {
          this.formSrc = "".concat(rootSrc, "/").concat(this.component.form);
        }
      }

      // Add revision version if set.
      if (this.component.formRevision || this.component.formRevision === 0) {
        this.formSrc += "/v/".concat(this.component.formRevision);
      }

      // Determine if we already have a loaded form object.
      if (this.component && this.component.components && this.component.components.length) {
        // Pass config down to sub forms.
        if (this.root && this.root.form && this.root.form.config && !this.component.config) {
          this.component.config = this.root.form.config;
        }
        this.renderSubForm(this.component, srcOptions);
      } else if (this.formSrc) {
        var query = {
          params: {
            live: 1
          }
        };
        new _Formio.default(this.formSrc).loadForm(query).then(function (formObj) {
          return _this5.renderSubForm(formObj, srcOptions);
        }).catch(function (err) {
          return _this5.subFormReadyReject(err);
        });
      }
      return this.subFormReady;
    }
    /* eslint-enable max-statements */
  }, {
    key: "checkValidity",
    value: function checkValidity(data, dirty) {
      if (this.subForm) {
        return this.subForm.checkValidity(this.dataValue.data, dirty);
      }
      return _get(_getPrototypeOf(FormComponent.prototype), "checkValidity", this).call(this, data, dirty);
    }
  }, {
    key: "checkConditions",
    value: function checkConditions(data) {
      var visible = _get(_getPrototypeOf(FormComponent.prototype), "checkConditions", this).call(this, data);
      var subForm = this.subForm;

      // Return if already hidden
      if (!visible) {
        return visible;
      }
      if (subForm && subForm.hasCondition()) {
        return this.subForm.checkConditions(this.dataValue.data);
      }
      return visible;
    }
  }, {
    key: "calculateValue",
    value: function calculateValue(data, flags) {
      if (this.subForm) {
        return this.subForm.calculateValue(this.dataValue.data, flags);
      }
      return _get(_getPrototypeOf(FormComponent.prototype), "calculateValue", this).call(this, data, flags);
    }
  }, {
    key: "setPristine",
    value: function setPristine(pristine) {
      _get(_getPrototypeOf(FormComponent.prototype), "setPristine", this).call(this, pristine);
      if (this.subForm) {
        this.subForm.setPristine(pristine);
      }
    }
  }, {
    key: "shouldSubmit",
    get: function get() {
      return !this.isHidden() && (!this.component.hasOwnProperty('reference') || this.component.reference);
    }

    /**
     * Returns the data for the subform.
     *
     * @return {*}
     */
  }, {
    key: "getSubFormData",
    value: function getSubFormData() {
      if (_lodash.default.get(this.subForm, 'form.display') === 'pdf') {
        return this.subForm.getSubmission();
      } else {
        return _nativePromiseOnly.default.resolve(this.dataValue);
      }
    }

    /**
     * Submit the subform if configured to do so.
     *
     * @return {*}
     */
  }, {
    key: "submitSubForm",
    value: function submitSubForm(rejectOnError) {
      var _this6 = this;
      // If we wish to submit the form on next page, then do that here.
      if (this.shouldSubmit) {
        return this.loadSubForm().then(function () {
          return _this6.subForm.submitForm().then(function (result) {
            _this6.subForm.loading = false;
            _this6.dataValue = result.submission;
            return _this6.dataValue;
          }).catch(function (err) {
            if (rejectOnError) {
              _this6.subForm.onSubmissionError(err);
              return _nativePromiseOnly.default.reject(err);
            } else {
              return {};
            }
          });
        });
      }
      return this.getSubFormData();
    }

    /**
     * Submit the form before the next page is triggered.
     */
  }, {
    key: "beforePage",
    value: function beforePage(next) {
      var _this7 = this;
      return this.submitSubForm(true).then(function () {
        return _get(_getPrototypeOf(FormComponent.prototype), "beforePage", _this7).call(_this7, next);
      });
    }

    /**
     * Submit the form before the whole form is triggered.
     */
  }, {
    key: "beforeSubmit",
    value: function beforeSubmit() {
      var _this8 = this;
      var submission = this.dataValue;

      // This submission has already been submitted, so just return the reference data.
      if (submission && submission._id && submission.form) {
        this.dataValue = this.shouldSubmit ? {
          _id: submission._id,
          form: submission.form
        } : submission;
        if (!this.shouldSubmit) {
          return _nativePromiseOnly.default.resolve(this.dataValue);
        }
      }
      return this.submitSubForm(false).then(function (data) {
        if (data._id) {
          _this8.dataValue = {
            _id: data._id,
            form: data.form
          };
        }
        return _this8.dataValue;
      }).then(function () {
        return _get(_getPrototypeOf(FormComponent.prototype), "beforeSubmit", _this8).call(_this8);
      });
    }
  }, {
    key: "build",
    value: function build() {
      this.createElement();
      if (!this.options.temporary) {
        this.restoreValue();
      }
      this.attachLogic();
    }
  }, {
    key: "isHidden",
    value: function isHidden() {
      if (this.component.hidden || !this.visible) {
        return true;
      }
      return !_get(_getPrototypeOf(FormComponent.prototype), "checkConditions", this).call(this, this.rootValue);
    }
  }, {
    key: "setValue",
    value: function setValue(submission, flags, norecurse) {
      var _this9 = this;
      this._submission = submission;
      if (_lodash.default.isEmpty(this._submission.metadata)) {
        _lodash.default.set(this._submission, 'metadata', _lodash.default.get(this.root, 'submission.metadata', {}));
      }
      if (this.subForm || norecurse) {
        if (!norecurse && submission && submission._id && this.subForm.formio && !flags.noload && (_lodash.default.isEmpty(submission.data) || this.shouldSubmit)) {
          var submissionUrl = "".concat(this.subForm.formio.formsUrl, "/").concat(submission.form, "/submission/").concat(submission._id);
          this.subForm.setUrl(submissionUrl, this.options);
          this.subForm.nosubmit = false;
          this.subForm.loadSubmission().then(function (sub) {
            return _this9.setValue(sub, flags, true);
          });
          return _get(_getPrototypeOf(FormComponent.prototype), "setValue", this).call(this, submission, flags);
        } else {
          return this.subForm ? this.subForm.setValue(submission, flags) : _get(_getPrototypeOf(FormComponent.prototype), "setValue", this).call(this, submission, flags);
        }
      }
      var changed = _get(_getPrototypeOf(FormComponent.prototype), "setValue", this).call(this, this._submission, flags);
      var hidden = this.isHidden();
      var subForm;
      if (hidden) {
        subForm = this.subFormReady;
      } else {
        subForm = this.loadSubForm();
      }
      subForm.then(function () {
        return _this9.setValue(_this9._submission, flags, true);
      });
      return changed;
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (this.subForm) {
        return this.subForm.getValue();
      }
      return this.dataValue;
    }
  }, {
    key: "getAllComponents",
    value: function getAllComponents() {
      if (!this.subForm) {
        return [];
      }
      return this.subForm.getAllComponents();
    }
  }, {
    key: "updateSubFormVisibility",
    value: function updateSubFormVisibility() {
      if (this.subForm) {
        this.subForm.parentVisible = this.visible;
      }
    }
  }, {
    key: "visible",
    get: function get() {
      return _get(_getPrototypeOf(FormComponent.prototype), "visible", this);
    },
    set: function set(value) {
      _set(_getPrototypeOf(FormComponent.prototype), "visible", value, this, true);
      this.updateSubFormVisibility();
    }
  }, {
    key: "parentVisible",
    get: function get() {
      return _get(_getPrototypeOf(FormComponent.prototype), "parentVisible", this);
    },
    set: function set(value) {
      _set(_getPrototypeOf(FormComponent.prototype), "parentVisible", value, this, true);
      this.updateSubFormVisibility();
    }
  }, {
    key: "isInternalEvent",
    value: function isInternalEvent(event) {
      switch (event) {
        case 'focus':
        case 'blur':
        case 'componentChange':
        case 'componentError':
        case 'error':
        case 'formLoad':
        case 'languageChanged':
        case 'render':
        case 'checkValidity':
        case 'initialized':
        case 'submit':
        case 'submitButton':
        case 'nosubmit':
        case 'updateComponent':
        case 'submitDone':
        case 'submissionDeleted':
        case 'requestDone':
        case 'nextPage':
        case 'prevPage':
        case 'wizardNavigationClicked':
        case 'updateWizardNav':
        case 'restoreDraft':
        case 'saveDraft':
        case 'saveComponent':
          return true;
        default:
          return false;
      }
    }
  }, {
    key: "createEmitter",
    value: function createEmitter() {
      var emiter = new _eventemitter.default({
        wildcard: false,
        maxListeners: 0
      });
      var nativeEmit = emiter.emit;
      var that = this;
      emiter.emit = function (event) {
        var eventType = event.replace("".concat(that.options.namespace, "."), '');
        for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
          args[_key2 - 1] = arguments[_key2];
        }
        nativeEmit.call.apply(nativeEmit, [this, event].concat(args));
        if (!that.isInternalEvent(eventType)) {
          that.emit.apply(that, [eventType].concat(args));
        }
      };
      return emiter;
    }
  }, {
    key: "deleteValue",
    value: function deleteValue() {
      _get(_getPrototypeOf(FormComponent.prototype), "setValue", this).call(this, null, {
        noUpdateEvent: true,
        noDefault: true
      });
      _lodash.default.unset(this.data, this.key);
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len3 = arguments.length, extend = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        extend[_key3] = arguments[_key3];
      }
      return _Base.default.schema.apply(_Base.default, [{
        label: 'Form',
        type: 'form',
        key: 'form',
        src: '',
        reference: true,
        form: '',
        path: ''
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Nested Form',
        icon: 'fab fa-wpforms',
        group: 'advanced',
        documentation: 'http://help.form.io/userguide/#form',
        weight: 110,
        schema: FormComponent.schema()
      };
    }
  }]);
  return FormComponent;
}(_Base.default);
exports.default = FormComponent;