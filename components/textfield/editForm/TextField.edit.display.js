"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _widgets = _interopRequireDefault(require("../../../widgets"));
var _lodash = _interopRequireDefault(require("lodash"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var _default = [{
  weight: 50,
  type: 'select',
  input: true,
  key: 'widget.type',
  label: 'Widget',
  placeholder: 'Select a widget',
  customClass: 'advanced-field',
  tooltip: 'The widget is the display UI used to input the value of the field.',
  onChange: function onChange(context) {
    context.data.widget = _lodash.default.pick(context.data.widget, 'type');
  },
  dataSrc: 'values',
  data: {
    values: [{
      label: 'Calendar',
      value: 'calendar'
    }]
  },
  conditional: {
    json: {
      '===': [{
        var: 'data.type'
      }, 'textfield']
    }
  }
}, {
  weight: 55,
  customClass: 'advanced-field',
  type: 'textarea',
  key: 'widget',
  label: 'Widget Settings',
  calculateValue: function calculateValue(context) {
    if (_lodash.default.isEmpty(_lodash.default.omit(context.data.widget, 'type'))) {
      var settings = {};
      if (context.data.widget && context.data.widget.type) {
        settings = _widgets.default[context.data.widget.type].defaultSettings;
      }
      return settings;
    }
    return context.data.widget;
  },
  input: true,
  rows: 5,
  editor: 'ace',
  as: 'json',
  conditional: {
    json: {
      '!!': {
        var: 'data.widget.type'
      }
    }
  }
}, {
  'label': '',
  weight: 699,
  'columns': [{
    'components': [{
      weight: 420,
      type: 'textfield',
      input: true,
      key: 'prefix',
      customClass: 'advanced-field',
      label: 'Prefix'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 430,
      type: 'textfield',
      input: true,
      key: 'suffix',
      customClass: 'advanced-field',
      label: 'Suffix'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }
  // {
  //   'components': [
  //     {
  //       weight: 600,
  //       type: 'textfield',
  //       input: true,
  //       key: 'tabindex',
  //       label: 'Tab Index',
  //       placeholder: 'Tab Index',
  //       customClass: 'advanced-field',
  //       tooltip: 'Sets the tabindex attribute of this component to override the tab order of the form. See the <a href=\\\'https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex\\\'>MDN documentation</a> on tabindex for more information.'
  //     }
  //   ],
  //   'width': 4,
  //   'offset': 0,
  //   'push': 0,
  //   'pull': 0,
  //   'type': 'column',
  //   'input': false,
  //   'hideOnChildrenHidden': false,
  //   'key': 'column-1',
  //   'tableView': true,
  //   'label': 'Column'
  // }
  ],

  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-2'
}, {
  'label': '',
  'columns': [{
    'components': [{
      weight: 710,
      type: 'checkbox',
      input: true,
      key: 'showWordCount',
      customClass: 'advanced-field',
      label: 'Show Word Counter'
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 720,
      type: 'checkbox',
      input: true,
      key: 'showCharCount',
      customClass: 'advanced-field',
      label: 'Show Character Counter'
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-11'
}, {
  key: 'column-1',
  ignore: true
}, {
  key: 'column-3',
  ignore: true
}, {
  type: 'select',
  key: 'size',
  label: 'Field size',
  input: true,
  dataSrc: 'values',
  weight: 109,
  data: {
    values: [{
      label: 'Extra Small',
      value: 'xs'
    }, {
      label: 'Small',
      value: 'sm'
    }, {
      label: 'Medium',
      value: 'md'
    }, {
      label: 'Large',
      value: 'lg'
    }]
  }
}];
exports.default = _default;