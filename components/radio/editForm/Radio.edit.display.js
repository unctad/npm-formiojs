"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _builder = _interopRequireDefault(require("../../../utils/builder"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var _default = [{
  type: 'select',
  input: true,
  label: 'Options Label Position',
  key: 'optionsLabelPosition',
  tooltip: 'Position for the label for options for this field.',
  dataSrc: 'values',
  weight: 701,
  defaultValue: 'right',
  data: {
    values: [{
      label: 'Top',
      value: 'top'
    }, {
      label: 'Left',
      value: 'left'
    }, {
      label: 'Right',
      value: 'right'
    }, {
      label: 'Bottom',
      value: 'bottom'
    }]
  }
}, {
  type: 'datagrid',
  input: true,
  label: 'Values',
  key: 'values',
  tooltip: 'The radio button values that can be picked for this field. Values are text submitted with the form data. Labels are text that appears next to the radio buttons on the form.',
  weight: 1,
  reorder: true,
  defaultValue: [{
    label: '',
    value: ''
  }],
  components: [{
    label: 'Label',
    key: 'label',
    input: true,
    type: 'textfield',
    logic: [{
      name: 'checkDisabled',
      trigger: {
        type: 'javascript',
        javascript: 'result = !!data.subType'
      },
      actions: [{
        name: 'setDisabled',
        type: 'property',
        property: {
          label: 'Disabled',
          value: 'disabled',
          type: 'boolean'
        },
        state: true
      }]
    }]
  }, {
    label: 'Value',
    key: 'value',
    input: true,
    type: 'textfield',
    allowCalculateOverride: true,
    calculateValue: function calculateValue(ctx) {
      if (ctx.instance.hasEmptiedValueOnInitialized == null) {
        ctx.instance.hasEmptiedValueOnInitialized = ctx.instance.isEmpty(ctx.instance.dataValue);
      }
      return ctx.instance.hasEmptiedValueOnInitialized ? ctx._.camelCase(ctx.row.label) : ctx.instance.dataValue;
    },
    logic: [{
      name: 'checkDisabled',
      trigger: {
        type: 'javascript',
        javascript: 'result = !!data.subType'
      },
      actions: [{
        name: 'setDisabled',
        type: 'property',
        property: {
          label: 'Disabled',
          value: 'disabled',
          type: 'boolean'
        },
        state: true
      }]
    }]
  }
  // {
  //   type: 'select',
  //   input: true,
  //   weight: 180,
  //   label: 'Shortcut',
  //   key: 'shortcut',
  //   tooltip: 'The shortcut key for this option.',
  //   dataSrc: 'custom',
  //   data: {
  //     custom(values, component, data, row, utils, instance, form) {
  //       return BuilderUtils.getAvailableShortcuts(form, component);
  //     }
  //   }
  // }
  ],

  logic: [{
    name: 'checkDisabled',
    trigger: {
      type: 'javascript',
      javascript: 'result = !!data.subType'
    },
    actions: [{
      name: 'setDisabled',
      type: 'property',
      property: {
        label: 'Disabled',
        value: 'disabled',
        type: 'boolean'
      },
      state: true
    }, {
      name: 'setDisabledReorder',
      type: 'property',
      property: {
        label: 'Reorder',
        value: 'reorder',
        type: 'boolean'
      },
      state: false
    }]
  }]
}, {
  type: 'checkbox',
  input: true,
  key: 'inline',
  label: 'Inline Layout',
  tooltip: 'Displays the checkboxes/radios horizontally.',
  weight: 706
}, {
  key: 'placeholderRow',
  customClass: 'advanced-field'
}, {
  key: 'column-3',
  ignore: true
}, {
  'key': 'placeholderRow',
  'ignore': true,
  'hidden': true
}, {
  'weight': 10,
  'type': 'textarea',
  'input': true,
  'key': 'tooltip',
  'label': 'Tooltip',
  'rows': 1,
  'placeholder': 'To add a tooltip to this field, enter text here.',
  'tooltip': 'Adds a tooltip to the side of this field.'
}];
exports.default = _default;