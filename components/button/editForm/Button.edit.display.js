"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _builder = _interopRequireDefault(require("../../../utils/builder"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var _default = [{
  'label': '',
  weight: 110,
  'columns': [{
    'components': [{
      type: 'select',
      key: 'action',
      label: 'Action',
      customClass: 'advanced-field',
      input: true,
      dataSrc: 'values',
      weight: 110,
      tooltip: 'This is the action to be performed by this button.',
      data: {
        values: [
        // { label: 'Print', value: 'print' },
        // { label: 'Submit', value: 'submit' },
        // { label: 'Save State', value: 'saveState' },
        {
          label: 'Event',
          value: 'event'
        }, {
          label: 'Custom',
          value: 'custom'
        }, {
          label: 'Reset',
          value: 'reset'
        }, {
          label: 'Open a link',
          value: 'openLink'
        }, {
          label: 'Open a link in a new tab',
          value: 'openLinkInNewTab'
        }, {
          label: 'Open service and data',
          value: 'openServiceAndData'
        }, {
          label: 'Open service and data in a new tab',
          value: 'openServiceAndDataInNewTab'
        }
        // { label: 'OAuth', value: 'oauth' },
        // { label: 'POST to URL', value: 'url' }
        ]
      }
    }],

    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'textfield',
      label: 'Save State',
      customClass: 'advanced-field',
      key: 'state',
      weight: 112,
      tooltip: 'The state you wish to save the submission under when this button is pressed. Example "draft" would save the submission in Draft Mode.',
      input: true,
      conditional: {
        json: {
          '!==': [{
            var: 'data.action'
          }, 'saveState']
        }
      }
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'textarea',
      key: 'custom',
      label: 'Button Custom Logic',
      customClass: 'advanced-field',
      tooltip: 'The custom logic to evaluate when the button is clicked.',
      rows: 5,
      editor: 'ace',
      input: true,
      weight: 120,
      placeholder: "data['mykey'] = data['anotherKey'];",
      conditional: {
        json: {
          '===': [{
            var: 'data.action'
          }, 'custom']
        }
      }
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-13'
}, {
  type: 'textfield',
  label: 'Button Event',
  key: 'event',
  input: true,
  weight: 120,
  tooltip: 'The event to fire when the button is clicked.',
  conditional: {
    json: {
      '===': [{
        var: 'data.action'
      }, 'event']
    }
  }
}, {
  type: 'textfield',
  label: 'Link in a new tab',
  key: 'linkInNewTab',
  input: true,
  weight: 120,
  conditional: {
    json: {
      '===': [{
        var: 'data.action'
      }, 'openLinkInNewTab']
    }
  }
}, {
  type: 'textfield',
  label: 'Link',
  key: 'link',
  input: true,
  weight: 120,
  conditional: {
    json: {
      '===': [{
        var: 'data.action'
      }, 'openLink']
    }
  }
}, {
  type: 'textfield',
  inputType: 'url',
  key: 'url',
  input: true,
  weight: 120,
  label: 'Button URL',
  tooltip: 'The URL where the submission will be sent.',
  placeholder: 'https://example.form.io',
  conditional: {
    json: {
      '===': [{
        var: 'data.action'
      }, 'url']
    }
  }
}, {
  type: 'datagrid',
  key: 'headers',
  input: true,
  weight: 130,
  label: 'Headers',
  addAnother: 'Add Header',
  tooltip: 'Headers Properties and Values for your request',
  components: [{
    key: 'header',
    label: 'Header',
    input: true,
    type: 'textfield'
  }, {
    key: 'value',
    label: 'Value',
    input: true,
    type: 'textfield'
  }],
  conditional: {
    json: {
      '===': [{
        var: 'data.action'
      }, 'url']
    }
  }
}, {
  type: 'select',
  key: 'theme',
  label: 'Theme',
  input: true,
  tooltip: 'The color theme of this button.',
  dataSrc: 'values',
  weight: 140,
  data: {
    values: [{
      label: 'Default',
      value: 'default'
    }, {
      label: 'Primary',
      value: 'primary'
    }, {
      label: 'Info',
      value: 'info'
    }, {
      label: 'Success',
      value: 'success'
    }, {
      label: 'Danger',
      value: 'danger'
    }, {
      label: 'Warning',
      value: 'warning'
    }]
  }
}, {
  type: 'select',
  key: 'size',
  label: 'Field size',
  input: true,
  tooltip: 'The size of this button.',
  dataSrc: 'values',
  weight: 109,
  data: {
    values: [{
      label: 'Extra Small',
      value: 'xs'
    }, {
      label: 'Small',
      value: 'sm'
    }, {
      label: 'Medium',
      value: 'md'
    }, {
      label: 'Large',
      value: 'lg'
    }, {
      label: 'Extra Large',
      value: 'xl'
    }]
  }
}, {
  'label': '',
  weight: 699,
  'columns': [{
    'components': [{
      type: 'textfield',
      key: 'leftIcon',
      label: 'Left Icon',
      customClass: 'advanced-field',
      input: true,
      placeholder: 'Enter icon classes',
      tooltip: "This is the full icon class string to show the icon. Example: 'fa fa-plus'",
      weight: 160
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'textfield',
      key: 'rightIcon',
      label: 'Right Icon',
      input: true,
      customClass: 'advanced-field',
      placeholder: 'Enter icon classes',
      tooltip: "This is the full icon class string to show the icon. Example: 'fa fa-plus'",
      weight: 170
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }
  // {
  //   'components': [
  //     {
  //       type: 'select',
  //       input: true,
  //       weight: 180,
  //       label: 'Shortcut',
  //       key: 'shortcut',
  //       customClass: 'advanced-field',
  //       tooltip: 'Shortcut for this component.',
  //       dataSrc: 'custom',
  //       data: {
  //         custom(values, component, data, row, utils, instance, form) {
  //           return BuilderUtils.getAvailableShortcuts(form, component);
  //         }
  //       }
  //     },
  //   ],
  //   'width': 4,
  //   'offset': 0,
  //   'push': 0,
  //   'pull': 0,
  //   'type': 'column',
  //   'input': false,
  //   'hideOnChildrenHidden': false,
  //   'key': 'column-1',
  //   'tableView': true,
  //   'label': 'Column'
  // }
  ],

  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-14'
}, {
  'label': '',
  weight: 700,
  'columns': [{
    'components': [{
      type: 'checkbox',
      key: 'block',
      label: 'Block',
      input: true,
      weight: 610,
      tooltip: 'This control should span the full width of the bounding container.'
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'checkbox',
      key: 'disableOnInvalid',
      label: 'Disable on Form Invalid',
      tooltip: 'This will disable this field if the form is invalid.',
      input: true,
      weight: 620
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'checkbox',
      input: true,
      inputType: 'checkbox',
      key: 'showValidations',
      customClass: 'advanced-field',
      label: 'Show Validations',
      weight: 115,
      tooltip: 'When the button is pressed, show any validation errors on the form.',
      conditional: {
        json: {
          '!==': [{
            var: 'data.action'
          }, 'submit']
        }
      }
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-15'
}, {
  key: 'placeholderRow',
  customClass: 'advanced-field'
}];
exports.default = _default;