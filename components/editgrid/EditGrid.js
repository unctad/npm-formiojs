"use strict";

require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.array.splice.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _NestedComponent2 = _interopRequireDefault(require("../nested/NestedComponent"));
var _Base = _interopRequireDefault(require("../base/Base"));
var _Components = _interopRequireDefault(require("../Components"));
var _utils = require("../../utils/utils");
var _templates = _interopRequireDefault(require("./templates"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var EditGridComponent = /*#__PURE__*/function (_NestedComponent) {
  _inherits(EditGridComponent, _NestedComponent);
  var _super = _createSuper(EditGridComponent);
  function EditGridComponent(component, options, data) {
    var _this;
    _classCallCheck(this, EditGridComponent);
    _this = _super.call(this, component, options, data);
    _this.type = 'datagrid';
    _this.editRows = [];
    return _this;
  }
  _createClass(EditGridComponent, [{
    key: "defaultSchema",
    get: function get() {
      return EditGridComponent.schema();
    }
  }, {
    key: "emptyValue",
    get: function get() {
      return [];
    }
  }, {
    key: "build",
    value: function build(state) {
      var _this2 = this;
      if (this.options.builder) {
        return _get(_getPrototypeOf(EditGridComponent.prototype), "build", this).call(this, state, true);
      }
      this.createElement();
      this.createLabel(this.element);
      var dataValue = this.dataValue;
      if (Array.isArray(dataValue)) {
        // Ensure we always have rows for each dataValue available.
        dataValue.forEach(function (row, rowIndex) {
          if (_this2.editRows[rowIndex]) {
            _this2.editRows[rowIndex].data = row;
          } else {
            _this2.editRows[rowIndex] = {
              components: [],
              isOpen: !!_this2.options.defaultOpen,
              data: row
            };
          }
        });
      }
      this.buildTable();
      this.createAddButton();
      this.createDescription(this.element);
      this.element.appendChild(this.errorContainer = this.ce('div', {
        class: 'has-error'
      }));
      this.attachLogic();
    }
  }, {
    key: "addComponents",
    value: function addComponents(element, data, options, state) {
      if (this.options.builder && !this.schemaReadOnly) {
        element = this.ce('div');
        this.element.appendChild(element);
        this.root.addDragContainer(element, this);
      }
      if (!options) {
        options = this.options;
      }
      options = _objectSpread(_objectSpread({}, options), {}, {
        inEditGrid: true
      });
      _get(_getPrototypeOf(EditGridComponent.prototype), "addComponents", this).call(this, element, data, options, state);
    }
  }, {
    key: "buildTable",
    value: function buildTable(fromBuild) {
      var _this3 = this;
      // Do not show the table when in builder mode.
      if (this.options.builder) {
        return;
      }
      if (!fromBuild && !this.editRows.length && this.component.defaultOpen) {
        return this.addRow(true);
      }
      var tableClass = 'editgrid-listgroup list-group ';
      ['striped', 'bordered', 'hover', 'condensed'].forEach(function (prop) {
        if (_this3.component[prop]) {
          tableClass += "table-".concat(prop, " ");
        }
      });
      var tableElement = this.ce('ul', {
        class: tableClass
      }, [this.headerElement = this.createHeader(), this.rowElements = this.editRows.map(this.createRow.bind(this)), this.footerElement = this.createFooter()]);
      if (this.tableElement && this.element.contains(this.tableElement)) {
        this.element.replaceChild(tableElement, this.tableElement);
      } else {
        this.element.appendChild(tableElement);
      }
      //add open class to the element if any edit grid row is open
      var isAnyRowOpen = this.editRows.some(function (row) {
        return row.isOpen;
      });
      if (isAnyRowOpen) {
        this.addClass(this.element, "formio-component-".concat(this.component.type, "-row-open"));
      } else {
        this.removeClass(this.element, "formio-component-".concat(this.component.type, "-row-open"));
      }
      this.tableElement = tableElement;
      if (this.allowReorder) {
        this.addDraggable([this.tableElement]);
      }
    }
  }, {
    key: "getRowDragulaOptions",
    value: function getRowDragulaOptions() {
      var superOptions = _get(_getPrototypeOf(EditGridComponent.prototype), "getRowDragulaOptions", this).call(this);
      superOptions.accepts = function (draggedElement, newParent, oldParent, nextSibling) {
        //disallow dragging above Edit Grid header
        return !nextSibling || !nextSibling.classList.contains('formio-edit-grid-header');
      };
      return superOptions;
    }
  }, {
    key: "onRowDrop",
    value: function onRowDrop(droppedElement, newParent, oldParent, nextSibling) {
      _get(_getPrototypeOf(EditGridComponent.prototype), "onRowDrop", this).call(this, droppedElement, newParent, oldParent, nextSibling);
      this.triggerChange();
    }
  }, {
    key: "createHeader",
    value: function createHeader() {
      var templateHeader = _utils.Evaluator.noeval ? _templates.default.header : _lodash.default.get(this.component, 'templates.header');
      if (!templateHeader) {
        return this.text('');
      }
      var headerMarkup = this.renderTemplate(templateHeader, {
        components: this.component.components,
        value: this.dataValue
      });
      var headerElement;
      if (this.allowReorder) {
        headerElement = this.ce('div', {
          class: 'row'
        }, [this.ce('div', {
          class: 'col-xs-1'
        }), this.ce('div', {
          class: 'col-xs-11'
        }, headerMarkup)]);
      } else {
        headerElement = headerMarkup;
      }
      return this.ce('li', {
        class: 'list-group-item list-group-header formio-edit-grid-header'
      }, headerElement);
    }
  }, {
    key: "createRow",
    value: function createRow(row, rowIndex) {
      var _this4 = this;
      var wrapper = this.ce('li', {
        class: 'list-group-item'
      });
      var rowTemplate = _utils.Evaluator.noeval ? _templates.default.row : _lodash.default.get(this.component, 'templates.row', EditGridComponent.defaultRowTemplate);

      // Store info so we can detect changes later.
      wrapper.rowData = row.data;
      wrapper.rowIndex = rowIndex;
      wrapper.rowOpen = row.isOpen;
      row.components = [];
      if (wrapper.rowOpen) {
        var dialog = this.component.modal ? this.createModal(this.component.addAnother || 'Add Another') : undefined;
        var editForm = this.component.components.map(function (comp) {
          var component = _lodash.default.cloneDeep(comp);
          var options = _lodash.default.clone(_this4.options);
          options.row = "".concat(_this4.row, "-").concat(rowIndex);
          options.name += "[".concat(rowIndex, "]");
          var instance = _this4.createComponent(component, options, row.data);
          instance.rowIndex = rowIndex;
          row.components.push(instance);
          return instance.element;
        });
        if (!this.options.readOnly) {
          editForm.push(this.ce('div', {
            class: 'editgrid-actions'
          }, [this.ce('button', {
            class: 'btn btn-primary',
            onClick: this.saveRow.bind(this, rowIndex, dialog)
          }, this.t(this.component.saveRow || 'Save')), ' ', this.component.removeRow ? this.ce('button', {
            class: 'btn btn-danger',
            onClick: this.cancelRow.bind(this, rowIndex)
          }, this.t(this.component.removeRow || 'Cancel')) : null]));
        }
        if (!this.component.modal) {
          wrapper.appendChild(this.ce('div', {
            class: 'editgrid-edit'
          }, this.ce('div', {
            class: 'editgrid-body'
          }, editForm)));
        } else {
          var formComponents = this.ce('div', {
            class: 'editgrid-edit'
          }, this.ce('div', {
            class: 'editgrid-body'
          }, editForm));
          dialog.body.appendChild(formComponents);
        }
      } else {
        var rowMarkup = this.renderTemplate(rowTemplate, {
          row: row.data,
          data: this.data,
          rowIndex: rowIndex,
          components: this.component.components,
          getView: function getView(component, data) {
            return _Components.default.create(component, _this4.options, data, true).getView(data);
          }
        }, [{
          class: 'removeRow',
          event: 'click',
          action: this.removeRow.bind(this, rowIndex)
        }, {
          class: 'editRow',
          event: 'click',
          action: this.editRow.bind(this, rowIndex)
        }]);
        var rowElement;
        if (this.allowReorder) {
          rowElement = this.ce('div', {
            class: 'row'
          }, [this.ce('div', {
            class: 'col-xs-1 formio-drag-column'
          }, this.dragButton()), this.ce('div', {
            class: 'col-xs-11'
          }, rowMarkup)]);
        } else {
          rowElement = rowMarkup;
        }
        wrapper.appendChild(rowElement);
      }
      wrapper.appendChild(row.errorContainer = this.ce('div', {
        class: 'has-error'
      }));
      this.checkData(this.data, {
        noValidate: true
      }, rowIndex);
      if (this.allowReorder) {
        wrapper.dragInfo = {
          index: rowIndex
        };
      }
      return wrapper;
    }
  }, {
    key: "createFooter",
    value: function createFooter() {
      var footerTemplate = _lodash.default.get(this.component, 'templates.footer');
      if (!footerTemplate) {
        return this.text('');
      }
      return this.ce('li', {
        class: 'list-group-item list-group-footer'
      }, this.renderTemplate(footerTemplate, {
        components: this.component.components,
        value: this.dataValue
      }));
    }
  }, {
    key: "checkData",
    value: function checkData(data) {
      var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var index = arguments.length > 2 ? arguments[2] : undefined;
      var valid = true;
      if (flags.noCheck) {
        return;
      }

      // Update the value.
      var changed = this.updateValue({
        noUpdateEvent: true
      });
      var editRow = this.editRows[index];

      // Iterate through all components and check conditions, and calculate values.
      editRow.components.forEach(function (comp) {
        changed |= comp.calculateValue(data, {
          noUpdateEvent: true
        });
        comp.checkConditions(data);
        if (!flags.noValidate) {
          valid &= comp.checkValidity(data, !editRow.isOpen);
        }
      });
      valid &= this.validateRow(index);

      // Trigger the change if the values changed.
      if (changed) {
        this.triggerChange(flags);
      }

      // Return if the value is valid.
      return valid;
    }
  }, {
    key: "createAddButton",
    value: function createAddButton() {
      if (this.options.readOnly) {
        return;
      }
      this.element.appendChild(this.ce('div', {
        class: 'editgrid-add'
      }, this.ce('button', {
        class: 'btn btn-primary',
        role: 'button',
        onClick: this.addRow.bind(this)
      }, [this.ce('span', {
        class: this.iconClass('plus'),
        'aria-hidden': true
      }), ' ', this.t(this.component.addAnother ? this.component.addAnother : 'Add Another', {})])));
    }
  }, {
    key: "addRow",
    value: function addRow(fromBuild) {
      if (this.options.readOnly) {
        return;
      }
      var dataObj = {};
      this.editRows.push({
        components: [],
        isOpen: true,
        data: dataObj
      });
      if (this.component.inlineEdit) {
        this.dataValue.push(dataObj);
      }
      this.emit('editGridAddRow', {
        component: this.component,
        row: this.editRows[this.editRows.length - 1]
      });
      if (this.component.inlineEdit) {
        this.updateGrid();
      } else {
        this.buildTable(fromBuild);
      }
    }
  }, {
    key: "editRow",
    value: function editRow(rowIndex) {
      var editRow = this.editRows[rowIndex];
      editRow.dirty = false;
      editRow.isOpen = true;
      editRow.editing = true;
      var dataSnapshot = _lodash.default.cloneDeep(this.dataValue[rowIndex]);
      if (this.component.inlineEdit) {
        editRow.backup = dataSnapshot;
        this.updateGrid();
      } else {
        editRow.data = dataSnapshot;
        this.buildTable();
      }
    }
  }, {
    key: "updateGrid",
    value: function updateGrid() {
      this.updateValue();
      this.triggerChange();
      this.buildTable();
      this.pristine = false;
      this.checkValidity(this.data);
    }
  }, {
    key: "clearErrors",
    value: function clearErrors(rowIndex) {
      var editRow = this.editRows[rowIndex];
      if (editRow && Array.isArray(editRow.components)) {
        editRow.components.forEach(function (comp) {
          comp.setPristine(true);
          comp.setCustomValidity('');
        });
      }
    }
  }, {
    key: "cancelRow",
    value: function cancelRow(rowIndex) {
      var editRow = this.editRows[rowIndex];
      if (this.options.readOnly) {
        editRow.dirty = false;
        editRow.isOpen = false;
        editRow.editing = false;
        this.buildTable();
        return;
      }
      if (editRow.editing) {
        editRow.dirty = false;
        editRow.isOpen = false;
        editRow.editing = false;
        if (this.component.inlineEdit) {
          this.dataValue[rowIndex] = editRow.backup;
        }
        editRow.data = this.dataValue[rowIndex];
        this.clearErrors(rowIndex);
      } else {
        this.clearErrors(rowIndex);
        if (this.component.inlineEdit) {
          this.splice(rowIndex);
        }
        this.removeChildFrom(editRow.element, this.tableElement);
        this.editRows.splice(rowIndex, 1);
      }
      this.updateGrid();
    }
  }, {
    key: "saveRow",
    value: function saveRow(rowIndex, modal) {
      var editRow = this.editRows[rowIndex];
      if (this.options.readOnly) {
        editRow.dirty = false;
        editRow.isOpen = false;
        this.buildTable();
        return;
      }
      editRow.dirty = true;
      if (!this.validateRow(rowIndex, true)) {
        return;
      }
      editRow.dirty = false;
      editRow.isOpen = false;
      if (!this.component.inlineEdit) {
        if (editRow.editing) {
          this.dataValue[rowIndex] = editRow.data;
        } else {
          // Insert this row into its proper place.
          var newIndex = this.dataValue.length;
          this.dataValue.push(editRow.data);
          this.editRows.splice(rowIndex, 1);
          this.editRows.splice(newIndex, 0, editRow);
        }
      }
      editRow.editing = false;
      this.updateGrid();
      if (this.component.modal) {
        modal.close();
      }
    }
  }, {
    key: "removeRow",
    value: function removeRow(rowIndex) {
      if (this.options.readOnly) {
        return;
      }
      this.splice(rowIndex);
      this.removeChildFrom(this.editRows[rowIndex].element, this.tableElement);
      this.editRows.splice(rowIndex, 1);
      this.updateGrid();
    }
  }, {
    key: "validateRow",
    value: function validateRow(rowIndex, dirty) {
      var check = true;
      var editRow = this.editRows[rowIndex];
      var isDirty = dirty || !!editRow.dirty;
      if (editRow.editing || isDirty) {
        editRow.components.forEach(function (comp) {
          comp.setPristine(!isDirty);
          check &= comp.checkValidity(null, isDirty, editRow.data);
        });
      }
      if (this.component.validate && this.component.validate.row) {
        var valid = this.evaluate(this.component.validate.row, {
          valid: true,
          row: editRow.data
        }, 'valid', true);
        if (valid === null) {
          valid = "Invalid row validation for ".concat(this.key);
        }
        editRow.errorContainer.innerHTML = '';
        if (valid !== true) {
          editRow.errorContainer.appendChild(this.ce('div', {
            class: 'editgrid-row-error help-block'
          }, valid));
          return false;
        }
      }
      return check;
    }
  }, {
    key: "checkValidity",
    value: function checkValidity(data, dirty) {
      var _this5 = this;
      if (!this.checkCondition(null, data)) {
        this.setCustomValidity('');
        return true;
      }
      var rowsValid = true;
      var rowsEditing = false;
      this.editRows.forEach(function (editRow, rowIndex) {
        // Trigger all errors on the row.
        var rowValid = _this5.validateRow(rowIndex, dirty);
        // Add has-error class to row.
        if (!rowValid) {
          _this5.addClass(editRow.element, 'has-error');
        } else {
          _this5.removeClass(editRow.element, 'has-error');
        }
        rowsValid &= rowValid;

        // Any open rows causes validation to fail.
        rowsEditing |= dirty && (editRow.editing || editRow.isOpen);
      });
      if (!rowsValid) {
        this.setCustomValidity('Please correct rows before proceeding.', dirty);
        return false;
      } else if (rowsEditing && !this.component.inlineEdit) {
        this.setCustomValidity('Please save all rows before proceeding.', dirty);
        return false;
      }
      var message = this.invalid || this.invalidMessage(data, dirty);
      this.setCustomValidity(message, dirty);
      return !message;
    }
  }, {
    key: "setCustomValidity",
    value: function setCustomValidity(message, dirty) {
      if (this.errorElement && this.errorContainer) {
        this.errorElement.innerHTML = '';
        this.removeChildFrom(this.errorElement, this.errorContainer);
      }
      this.removeClass(this.element, 'has-error');
      if (this.options.highlightErrors) {
        this.removeClass(this.element, 'alert alert-danger');
      }
      if (message && (dirty || !this.pristine)) {
        this.emit('componentError', this.error);
        this.createErrorElement();
        var errorMessage = this.ce('p', {
          class: 'help-block'
        });
        errorMessage.appendChild(this.text(message));
        this.appendTo(errorMessage, this.errorElement);
        // Add error classes
        this.addClass(this.element, 'has-error');
        if (dirty && this.options.highlightErrors) {
          this.addClass(this.element, 'alert alert-danger');
        }
      }
    }
  }, {
    key: "defaultValue",
    get: function get() {
      var value = _get(_getPrototypeOf(EditGridComponent.prototype), "defaultValue", this);
      return Array.isArray(value) ? value : [];
    }
  }, {
    key: "updateValue",
    value: function updateValue(flags, value) {
      // Intentionally skip over nested component updateValue method to keep recursive update from occurring with sub components.
      return _Base.default.prototype.updateValue.call(this, flags, value);
    }
  }, {
    key: "setValue",
    value: function setValue(value) {
      var _this6 = this;
      if (!value) {
        this.editRows = this.defaultValue;
        this.buildTable();
        return;
      }
      if (!Array.isArray(value)) {
        if (_typeof(value) === 'object') {
          value = [value];
        } else {
          return;
        }
      }
      var changed = this.hasChanged(value, this.dataValue);
      this.dataValue = value;
      var dataValue = this.dataValue;
      if (Array.isArray(dataValue)) {
        // Refresh editRow data when data changes.
        dataValue.forEach(function (row, rowIndex) {
          if (_this6.editRows[rowIndex]) {
            _this6.editRows[rowIndex].data = row;
          } else {
            _this6.editRows[rowIndex] = {
              components: [],
              isOpen: !!_this6.options.defaultOpen,
              data: row
            };
          }
        });

        // Remove any extra edit rows.
        if (dataValue.length < this.editRows.length) {
          for (var rowIndex = this.editRows.length - 1; rowIndex >= dataValue.length; rowIndex--) {
            this.removeChildFrom(this.editRows[rowIndex].element, this.tableElement);
            this.editRows.splice(rowIndex, 1);
          }
        }
      }
      this.buildTable();
      return changed;
    }

    /**
     * Get the value of this component.
     *
     * @returns {*}
     */
  }, {
    key: "getValue",
    value: function getValue() {
      return this.dataValue;
    }
  }, {
    key: "clearOnHide",
    value: function clearOnHide(show) {
      _get(_getPrototypeOf(EditGridComponent.prototype), "clearOnHide", this).call(this, show);
      if (!this.component.clearOnHide) {
        // If some components set to clearOnHide we need to clear them.
        this.buildTable();
      }
    }
  }, {
    key: "restoreComponentsContext",
    value: function restoreComponentsContext() {
      return;
    }
  }, {
    key: "insertBefore",
    value: function insertBefore(compElement, before) {
      this.getContainer().lastChild.insertBefore(compElement, before);
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _NestedComponent2.default.schema.apply(_NestedComponent2.default, [{
        type: 'editgrid',
        label: 'Edit Grid',
        key: 'editGrid',
        version: '201905',
        clearOnHide: true,
        input: true,
        tree: true,
        defaultOpen: false,
        removeRow: 'Cancel',
        components: [],
        inlineEdit: false,
        templates: {
          header: this.defaultHeaderTemplate,
          row: this.defaultRowTemplate,
          footer: ''
        }
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Edit Grid',
        icon: 'fa fa-th',
        group: 'layout',
        documentation: 'http://help.form.io/userguide/#editgrid',
        weight: 40,
        schema: EditGridComponent.schema()
      };
    }
  }, {
    key: "defaultHeaderTemplate",
    get: function get() {
      return "<div class=\"row\">\n  {% util.eachComponent(components, function(component) { %}\n    {% if (!component.hasOwnProperty('tableView') || component.tableView) { %}\n      <div class=\"col-sm-2\">{{ component.label }}</div>\n    {% } %}\n  {% }) %}\n</div>";
    }
  }, {
    key: "defaultRowTemplate",
    get: function get() {
      return "<div class=\"row\">\n  {% util.eachComponent(components, function(component) { %}\n    {% if (!component.hasOwnProperty('tableView') || component.tableView) { %}\n      <div class=\"col-sm-2\">\n        {{ getView(component, row[component.key]) }}\n      </div>\n    {% } %}\n  {% }) %}\n  {% if (!instance.options.readOnly) { %}\n    <div class=\"col-sm-2\">\n      <div class=\"btn-group pull-right\">\n        <button class=\"btn btn-default btn-light btn-sm editRow\"><i class=\"{{ iconClass('edit') }}\"></i></button>\n        <button class=\"btn btn-danger btn-sm removeRow\"><i class=\"{{ iconClass('trash') }}\"></i></button>\n      </div>\n    </div>\n  {% } %}\n</div>";
    }
  }]);
  return EditGridComponent;
}(_NestedComponent2.default);
exports.default = EditGridComponent;