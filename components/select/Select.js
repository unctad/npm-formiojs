"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.reflect.set.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.sort.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.object.assign.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _choices = _interopRequireDefault(require("choices.js/public/assets/scripts/choices.js"));
var _lodash = _interopRequireDefault(require("lodash"));
var _Base = _interopRequireDefault(require("../base/Base"));
var _Formio = _interopRequireDefault(require("../../Formio"));
var _nativePromiseOnly = _interopRequireDefault(require("native-promise-only"));
var _treeselectjs = require("treeselectjs");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }
function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new TypeError('failed to set property'); } return value; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var SelectComponent = /*#__PURE__*/function (_BaseComponent) {
  _inherits(SelectComponent, _BaseComponent);
  var _super = _createSuper(SelectComponent);
  function SelectComponent(component, options, data) {
    var _this;
    _classCallCheck(this, SelectComponent);
    _this = _super.call(this, component, options, data);

    // Trigger an update.
    _this.triggerUpdate = _lodash.default.debounce(_this.updateItems.bind(_assertThisInitialized(_this)), 100);

    // Keep track of the select options.
    _this.selectOptions = [];
    _this.selectOptionValues = [];
    if (_this.isInfiniteScrollProvided) {
      _this.isFromSearch = false;
      _this.searchServerCount = null;
      _this.defaultServerCount = null;
      _this.isScrollLoading = false;
      _this.searchDownloadedResources = [];
      _this.defaultDownloadedResources = [];
    }

    // If this component has been activated.
    _this.activated = false;

    // Determine when the items have been loaded.
    _this.itemsLoaded = new _nativePromiseOnly.default(function (resolve) {
      _this.itemsLoadedResolve = resolve;
    });
    return _this;
  }
  _createClass(SelectComponent, [{
    key: "dataReady",
    get: function get() {
      return this.itemsLoaded;
    }
  }, {
    key: "defaultSchema",
    get: function get() {
      return SelectComponent.schema();
    }
  }, {
    key: "emptyValue",
    get: function get() {
      return '';
    }
  }, {
    key: "elementInfo",
    value: function elementInfo() {
      var info = _get(_getPrototypeOf(SelectComponent.prototype), "elementInfo", this).call(this);
      info.type = 'select';
      info.changeEvent = 'change';
      return info;
    }
  }, {
    key: "createWrapper",
    value: function createWrapper() {
      return false;
    }
  }, {
    key: "isSelectResource",
    get: function get() {
      return this.component.dataSrc === 'resource';
    }
  }, {
    key: "isSelectURL",
    get: function get() {
      return this.component.dataSrc === 'url';
    }
  }, {
    key: "infiniteScrollEnabled",
    get: function get() {
      if (this.component.infiniteScrollEnabled == null) {
        return true;
      }
      return this.component.infiniteScrollEnabled;
    }
  }, {
    key: "isInfiniteScrollProvided",
    get: function get() {
      return this.infiniteScrollEnabled && (this.isSelectResource || this.isSelectURL);
    }
  }, {
    key: "itemTemplate",
    value: function itemTemplate(data) {
      if (!data) {
        return '';
      }

      // If they wish to show the value in read only mode, then just return the itemValue here.
      if (this.options.readOnly && this.component.readOnlyValue) {
        return this.itemValue(data);
      }

      // Perform a fast interpretation if we should not use the template.
      if (data && !this.component.template) {
        var itemLabel = data.label || data;
        return typeof itemLabel === 'string' ? this.t(itemLabel) : itemLabel;
      }
      if (typeof data === 'string') {
        return this.t(data);
      }
      var template = this.component.template ? this.interpolate(this.component.template, {
        item: data
      }) : data.label;
      if (template) {
        var label = template.replace(/<\/?[^>]+(>|$)/g, '');
        return template.replace(label, this.t(label, {
          skipInterpolation: true
        }));
      } else {
        return JSON.stringify(data);
      }
    }

    /**
     * @param {*} data
     * @param {boolean} [forceUseValue=false] - if true, return 'value' property of the data
     * @return {*}
     */
  }, {
    key: "itemValue",
    value: function itemValue(data) {
      var forceUseValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      if (_lodash.default.isObject(data)) {
        if (this.component.valueProperty) {
          return _lodash.default.get(data, this.component.valueProperty);
        }
        if (forceUseValue) {
          return data.value;
        }
      }
      return data;
    }
  }, {
    key: "addAutofillHoneyInput",
    value: function addAutofillHoneyInput(container, input) {
      var _this2 = this;
      var autofillInput = this.ce('input', {
        type: 'text',
        name: this.info.attr.name,
        style: 'display: none'
      });
      input.addEventListener('change', function (event) {
        autofillInput.value = JSON.stringify(event.detail ? event.detail.value : event.target.value);
      });
      autofillInput.addEventListener('change', function (event) {
        _this2.updateValue({}, JSON.parse(event.target.value));
      });
      container.appendChild(autofillInput);
    }
  }, {
    key: "createInput",
    value: function createInput(container) {
      this.selectContainer = container;
      this.selectInput = _get(_getPrototypeOf(SelectComponent.prototype), "createInput", this).call(this, container);
      if (this.component.widget !== 'html5') {
        this.addAutofillHoneyInput(this.selectContainer, this.selectInput);
      }
    }

    /**
     * Adds an option to the select dropdown.
     *
     * @param value
     * @param label
     */
  }, {
    key: "addOption",
    value: function addOption(value, label, attr) {
      var option = {
        value: value,
        label: label
      };
      if (value) {
        this.selectOptions.push(option);
      }
      this.addOptionV2(option);
    }
  }, {
    key: "convertOptionAsTree",
    value: function convertOptionAsTree(option) {
      var _this3 = this;
      var newOption = {
        name: option.label,
        value: option.value || option.id
      };
      if (option.choices) {
        newOption.children = option.choices.map(function (o) {
          return _this3.convertOptionAsTree(o);
        });
      }
      return newOption;
    }
  }, {
    key: "addOptionValue",
    value: function addOptionValue(option) {
      var _this4 = this;
      if (option.choices) {
        option.choices.forEach(function (eachChild) {
          return _this4.addOptionValue(eachChild);
        });
      } else if (option.value) {
        this.selectOptionValues.push(option.value);
      }
    }
  }, {
    key: "addOptionV2",
    value: function addOptionV2(option, attr) {
      if (option.value || option.choices) {
        if (this.treeselect) {
          this.selectOptions.push(this.convertOptionAsTree(option));
        } else {
          this.selectOptions.push(option);
        }
        this.addOptionValue(option);
      }
      if (this.choices || this.treeselect) {
        return;
      }
      option.element = document.createElement('option');
      if (this.dataValue === option.value) {
        option.element.setAttribute('selected', 'selected');
        option.element.selected = 'selected';
      }
      option.element.innerHTML = option.label;
      if (attr) {
        _lodash.default.each(attr, function (value, key) {
          option.element.setAttribute(key, value);
        });
      }
      this.selectInput.appendChild(option.element);
    }
  }, {
    key: "addValueOptions",
    value: function addValueOptions(items) {
      items = items || [];
      var added = false;
      if (!this.selectOptions.length) {
        var currentChoices = Array.isArray(this.dataValue) ? this.dataValue : [this.dataValue];
        added = this.addCurrentChoices(currentChoices, items);
        if (!added && !this.component.multiple) {
          this.addPlaceholder(this.selectInput);
        }
      }
      return added;
    }
  }, {
    key: "disableInfiniteScroll",
    value: function disableInfiniteScroll() {
      if (!this.downloadedResources) {
        return;
      }
      this.downloadedResources.serverCount = this.downloadedResources.length;
      this.serverCount = this.downloadedResources.length;
    }

    /* eslint-disable max-statements */
  }, {
    key: "setItems",
    value: function setItems(items, fromSearch) {
      var _this5 = this;
      // If the items is a string, then parse as JSON.
      if (typeof items == 'string') {
        try {
          items = JSON.parse(items);
        } catch (err) {
          console.warn(err.message);
          items = [];
        }
      }

      // Allow js processing (needed for form builder)
      if (this.component.onSetItems && typeof this.component.onSetItems === 'function') {
        var newItems = this.component.onSetItems(this, items);
        if (newItems) {
          items = newItems;
        }
      }
      if (!this.choices && !this.treeselect && this.selectInput) {
        this.selectInput.innerHTML = '';
      }

      // If they provided select values, then we need to get them instead.
      if (this.component.selectValues) {
        items = _lodash.default.get(items, this.component.selectValues, items) || [];
      }
      var areItemsEqual;
      if (this.isInfiniteScrollProvided) {
        areItemsEqual = this.isSelectURL ? _lodash.default.isEqual(items, this.downloadedResources) : false;
        var areItemsEnded = this.component.limit > items.length;
        var areItemsDownloaded = areItemsEqual && this.downloadedResources && this.downloadedResources.length === items.length;
        if (areItemsEnded) {
          this.disableInfiniteScroll();
          if (areItemsEqual && this.selectOptions) {
            return;
          }
        } else if (areItemsDownloaded) {
          this.selectOptionValues = [];
          this.selectOptions = [];
        } else {
          this.serverCount = items.serverCount;
        }
      }
      if (this.isScrollLoading && items) {
        if (!areItemsEqual) {
          this.downloadedResources = this.downloadedResources ? this.downloadedResources.concat(items) : items;
        }
        this.downloadedResources.serverCount = items.serverCount || this.downloadedResources.serverCount;
      } else {
        this.downloadedResources = items || [];
        this.selectOptions = [];
        this.selectOptionValues = [];
      }

      // Add the value options.
      if (!fromSearch) {
        this.addValueOptions(items);
      }
      if (this.component.widget === 'html5' && !this.component.placeholder) {
        this.addOption(null, '');
      }
      var options = [];
      var groupedOptions = {};
      _lodash.default.each(items, function (item) {
        var option = {
          value: _this5.itemValue(item),
          label: _this5.itemTemplate(item)
        };
        if (item.group) {
          if (!groupedOptions[item.group]) {
            groupedOptions[item.group] = [];
          }
          groupedOptions[item.group].push(option);
        } else {
          options.push(option);
        }
      });
      if (this.component.orderByLabel) {
        var orderDesc = this.component.orderByLabel === 'desc';
        for (var _i = 0, _Object$keys = Object.keys(groupedOptions); _i < _Object$keys.length; _i++) {
          var key = _Object$keys[_i];
          groupedOptions[key] = _lodash.default.orderBy(groupedOptions[key], 'label');
          if (orderDesc) {
            groupedOptions[key] = groupedOptions[key].reverse();
          }
        }
      }
      for (var _i2 = 0, _Object$keys2 = Object.keys(groupedOptions); _i2 < _Object$keys2.length; _i2++) {
        var _key = _Object$keys2[_i2];
        if (!groupedOptions[_key]) {
          continue;
        }
        this.solveNestedGroup(groupedOptions[_key], groupedOptions);
      }
      var _loop = function _loop() {
        var key = _Object$keys3[_i3];
        var groupOption = options.find(function (o) {
          return o.value === key;
        });
        if (groupOption) {
          groupOption.id = key;
          delete groupOption.value;
          groupOption.choices = groupedOptions[key];
        } else {
          options.push({
            id: key,
            label: _this5.t(String(key)),
            choices: groupedOptions[key]
          });
        }
      };
      for (var _i3 = 0, _Object$keys3 = Object.keys(groupedOptions); _i3 < _Object$keys3.length; _i3++) {
        _loop();
      }
      if (this.component.orderByLabel) {
        options = _lodash.default.orderBy(options, 'label');
        var _orderDesc = this.component.orderByLabel === 'desc';
        if (_orderDesc) {
          options = options.reverse();
        }
      }

      // Iterate through each of the items.
      _lodash.default.each(options, function (eachOption) {
        _this5.addOptionV2(eachOption);
      });
      if (this.choices) {
        this.choices.setChoices(this.selectOptions, 'value', 'label', true);
      } else if (this.treeselect) {
        this.treeselect.options = this.selectOptions;
        this.treeselect.mount();
      }

      // We are no longer loading.
      this.isScrollLoading = false;
      this.loading = false;

      // If a value is provided, then select it.
      if (this.dataValue) {
        this.setValue(this.dataValue, true);
      } else {
        // If a default value is provided then select it.
        var defaultValue = this.defaultValue;
        if (defaultValue) {
          this.setValue(defaultValue);
        }
      }

      // Say we are done loading the items.
      this.itemsLoadedResolve();
    }
  }, {
    key: "solveNestedGroup",
    value: function solveNestedGroup(subItems, groupedOptions) {
      var _iterator = _createForOfIteratorHelper(subItems),
        _step;
      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var subItem = _step.value;
          if (groupedOptions[subItem.value]) {
            subItem.choices = groupedOptions[subItem.value];
            delete groupedOptions[subItem.value];
            this.solveNestedGroup(subItem.choices, groupedOptions);
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    }
    /* eslint-enable max-statements */
  }, {
    key: "loadItems",
    value: function loadItems(url, search, headers, options, method, body) {
      var _this6 = this;
      options = options || {};

      // See if they have not met the minimum search requirements.
      var minSearch = parseInt(this.component.minSearch, 10);
      if (this.component.searchField && minSearch > 0 && (!search || search.length < minSearch)) {
        // Set empty items.
        return this.setItems([]);
      }

      // Ensure we have a method and remove any body if method is get
      method = method || 'GET';
      if (method.toUpperCase() === 'GET') {
        body = null;
      }
      var limit = this.component.limit || 100;
      var skip = this.isScrollLoading ? this.selectOptions.length : 0;
      var query = this.component.dataSrc === 'url' ? {} : {
        limit: limit,
        skip: skip
      };

      // Allow for url interpolation.
      url = this.interpolate(url, {
        formioBase: _Formio.default.getBaseUrl(),
        search: search,
        limit: limit,
        skip: skip,
        page: Math.abs(Math.floor(skip / limit))
      });

      // Add search capability.
      if (this.component.searchField && search) {
        if (Array.isArray(search)) {
          query["".concat(this.component.searchField, "__in")] = search.join(',');
        } else {
          query["".concat(this.component.searchField, "__regex")] = search.replace(/\(/g, '\\(').replace(/\)/g, '\\)').replace(/&amp;/g, '&');
        }
      }

      // If they wish to return only some fields.
      if (this.component.selectFields) {
        query.select = this.component.selectFields;
      }

      // Add sort capability
      if (this.component.sort) {
        query.sort = this.component.sort;
      }
      if (!_lodash.default.isEmpty(query)) {
        // Add the query string.
        url += (!url.includes('?') ? '?' : '&') + _Formio.default.serialize(query, function (item) {
          return _this6.interpolate(item);
        });
      }

      // Add filter capability
      if (this.component.filter) {
        url += (!url.includes('?') ? '?' : '&') + this.interpolate(this.component.filter);
      }

      // Make the request.
      options.header = headers;
      this.loading = true;
      return _Formio.default.makeRequest(this.options.formio, 'select', url, method, body, options).then(function (response) {
        _this6.loading = false;
        _this6.setItems(response, !!search);
      }).catch(function (err) {
        if (_this6.isInfiniteScrollProvided) {
          _this6.setItems([]);
          _this6.disableInfiniteScroll();
        }
        _this6.isScrollLoading = false;
        _this6.loading = false;
        _this6.itemsLoadedResolve();
        _this6.emit('componentError', {
          component: _this6.component,
          message: err.toString()
        });
        console.warn("Unable to load resources for ".concat(_this6.key));
      });
    }

    /**
     * Get the request headers for this select dropdown.
     */
  }, {
    key: "requestHeaders",
    get: function get() {
      var _this7 = this;
      // Create the headers object.
      var headers = new _Formio.default.Headers();
      // Add custom headers to the url.
      if (this.component.data && this.component.data.headers) {
        return Promise.all(this.component.data.headers.filter(function (h) {
          return h.key;
        }).map(function (h) {
          if (typeof h.value === 'function') {
            // if h.value is a promise
            var invoke = h.value();
            if (typeof invoke.then === 'function') {
              return h.value().then(function (value) {
                return {
                  key: h.key,
                  value: value
                };
              });
            }
            return {
              key: h.key,
              value: h.value()
            };
          }
          return {
            key: h.key,
            value: h.value
          };
        })).then(function (headerArray) {
          headerArray.forEach(function (h) {
            headers.set(h.key, _this7.interpolate(h.value));
          });
          return headers;
        }).catch(function () {
          return headers; // fallback value
        });
      }

      return headers; // fallback value
    }
  }, {
    key: "getCustomItems",
    value: function getCustomItems() {
      return this.evaluate(this.component.data.custom, {
        values: []
      }, 'values');
    }
  }, {
    key: "updateCustomItems",
    value: function updateCustomItems() {
      this.setItems(this.getCustomItems() || []);
    }
  }, {
    key: "additionalResourcesAvailable",
    get: function get() {
      return _lodash.default.isNil(this.serverCount) || this.serverCount > this.downloadedResources.length;
    }
  }, {
    key: "serverCount",
    get: function get() {
      if (this.isFromSearch) {
        return this.searchServerCount;
      }
      return this.defaultServerCount;
    },
    set: function set(value) {
      if (this.isFromSearch) {
        this.searchServerCount = value;
      } else {
        this.defaultServerCount = value;
      }
    }
  }, {
    key: "downloadedResources",
    get: function get() {
      if (this.isFromSearch) {
        return this.searchDownloadedResources;
      }
      return this.defaultDownloadedResources;
    },
    set: function set(value) {
      if (this.isFromSearch) {
        this.searchDownloadedResources = value;
      } else {
        this.defaultDownloadedResources = value;
      }
    }

    /* eslint-disable max-statements */
  }, {
    key: "updateItems",
    value: function updateItems(searchInput, forceUpdate) {
      var _this8 = this;
      if (!this.component.data) {
        console.warn("Select component ".concat(this.key, " does not have data configuration."));
        this.itemsLoadedResolve();
        return;
      }

      // Only load the data if it is visible.
      if (!this.checkConditions()) {
        this.itemsLoadedResolve();
        return;
      }
      switch (this.component.dataSrc) {
        case 'values':
          this.component.valueProperty = this.originalComponent.valueProperty = 'value';
          this.setItems(this.component.data.values);
          break;
        case 'json':
          this.setItems(this.component.data.json);
          break;
        case 'custom':
          this.updateCustomItems();
          break;
        case 'resource':
          {
            // If there is no resource, or we are lazyLoading, wait until active.
            if (!this.component.data.resource || !forceUpdate && !this.active) {
              return;
            }
            var resourceUrl = this.options.formio ? this.options.formio.formsUrl : "".concat(_Formio.default.getProjectUrl(), "/form");
            resourceUrl += "/".concat(this.component.data.resource, "/submission");
            if (this.additionalResourcesAvailable) {
              try {
                var requestHeaders = this.requestHeaders;
                // eslint-disable-next-line max-depth
                if (requestHeaders.then) {
                  return requestHeaders.then(function (headers) {
                    _this8.loadItems(resourceUrl, searchInput, headers);
                  });
                }
                return this.loadItems(resourceUrl, searchInput, requestHeaders);
              } catch (err) {
                console.warn("Unable to load resources for ".concat(this.key));
              }
            } else {
              this.setItems(this.downloadedResources);
            }
            break;
          }
        case 'url':
          {
            if (!forceUpdate && !this.active) {
              // If we are lazyLoading, wait until activated.
              return;
            }
            var url = this.component.data.url;
            var method;
            var body;
            if (url.substr(0, 1) === '/') {
              var baseUrl = _Formio.default.getProjectUrl();
              if (!baseUrl) {
                baseUrl = _Formio.default.getBaseUrl();
              }
              url = baseUrl + this.component.data.url;
            }
            if (!this.component.data.method) {
              method = 'GET';
            } else {
              method = this.component.data.method;
              if (method.toUpperCase() === 'POST') {
                body = this.component.data.body;
              } else {
                body = null;
              }
            }
            var options = this.component.authenticate ? {} : {
              noToken: true
            };
            options.ignoreCache = this.component.ignoreCache;
            var _requestHeaders = this.requestHeaders;
            if (_requestHeaders.then) {
              return _requestHeaders.then(function (headers) {
                _this8.loadItems(url, searchInput, headers, options, method, body);
              });
            }
            return this.loadItems(url, searchInput, _requestHeaders, options, method, body);
          }
        case 'catalog':
          {
            if (!forceUpdate && !this.active || !this.component.catalog) {
              // If we are lazyLoading, wait until activated.
              return;
            }
            var _url = "/classification/".concat(this.component.catalog);
            var _method;
            var _body;
            if (_url.substr(0, 1) === '/') {
              var _baseUrl = _Formio.default.getProjectUrl();
              if (!_baseUrl) {
                _baseUrl = _Formio.default.getBaseUrl();
              }
              _url = _baseUrl + _url;
            }
            _method = 'GET';
            if (!this.component.data.method) {
              _method = 'GET';
            } else {
              _method = this.component.data.method;
              if (_method.toUpperCase() === 'POST') {
                _body = this.component.data.body;
              } else {
                _body = null;
              }
            }
            var _options = this.component.authenticate ? {} : {
              noToken: true
            };
            _options.ignoreCache = this.component.ignoreCache;
            var headers = new _Formio.default.Headers();
            if (this.options.getToken) {
              return this.options.getToken().then(function (token) {
                headers.set('Authorization', token);
                _this8.loadItems(_url, searchInput, headers, _options, _method, _body);
              });
            }
            var _requestHeaders2 = this.requestHeaders;
            if (_requestHeaders2.then) {
              return _requestHeaders2.then(function (headers) {
                _this8.loadItems(_url, searchInput, headers, _options, _method, _body);
              });
            }
            return this.loadItems(_url, searchInput, _requestHeaders2, _options, _method, _body);
          }
      }
    }
    /* eslint-enable max-statements */
  }, {
    key: "addPlaceholder",
    value: function addPlaceholder(input) {
      if (!this.component.placeholder || !input) {
        return;
      }
      var placeholder = document.createElement('option');
      placeholder.setAttribute('placeholder', true);
      placeholder.appendChild(this.text(this.component.placeholder));
      input.appendChild(placeholder);
    }

    /**
     * Activate this select control.
     */
  }, {
    key: "activate",
    value: function activate() {
      if (this.active) {
        return;
      }
      this.activated = true;
      if (this.choices) {
        this.choices.setChoices([{
          value: '',
          label: "<i class=\"".concat(this.iconClass('refresh'), "\" style=\"font-size:1.3em;\"></i>"),
          disabled: true
        }], 'value', 'label', true);
      } else {
        this.addOption('', this.t('loading...'));
      }
      this.triggerUpdate();
    }
  }, {
    key: "active",
    get: function get() {
      return !this.component.lazyLoad || this.activated;
    }

    /* eslint-disable max-statements */
  }, {
    key: "addInput",
    value: function addInput(input, container) {
      var _this9 = this;
      _get(_getPrototypeOf(SelectComponent.prototype), "addInput", this).call(this, input, container);
      if (this.component.multiple) {
        input.setAttribute('multiple', true);
      }
      var placeholderValue = this.t(this.component.placeholder);
      var useSearch = this.component.hasOwnProperty('searchEnabled') ? this.component.searchEnabled : true;
      var customOptions = this.component.customOptions || {};
      if (typeof customOptions == 'string') {
        try {
          customOptions = JSON.parse(customOptions);
        } catch (err) {
          console.warn(err.message);
          customOptions = {};
        }
      }
      if (this.component.widget === 'html5') {
        this.triggerUpdate();
        this.focusableElement = input;
        this.addEventListener(input, 'focus', function () {
          return _this9.update();
        });
        this.addEventListener(input, 'keydown', function (event) {
          var keyCode = event.keyCode;
          if ([8, 46].includes(keyCode)) {
            _this9.setValue(null);
          }
        });
        return;
      } else if (this.component.widget === 'treeselectjs') {
        input.style.setProperty('display', 'none');
        var divContainer = this.ce('div');
        input.parentElement.appendChild(divContainer);
        this.treeselect = new _treeselectjs.Treeselect(_objectSpread({
          parentHtmlContainer: divContainer,
          placeholder: placeholderValue,
          disabled: this.component.disabled,
          value: this.dataValue || (this.component.multiple ? [] : ''),
          options: this.selectOptions,
          emptyText: this.t('No results found'),
          searchable: useSearch,
          isSingleSelect: !this.component.multiple,
          clearable: this.component.disabled ? false : _lodash.default.get(this.component, 'removeItemButton', true),
          openCallback: function openCallback() {
            if (_this9.dataValue && _this9.selectOptions.length === 0) {
              _this9.triggerUpdate();
            }
            _this9.update();
          }
        }, customOptions));
        this.treeselect.srcElement.addEventListener('input', function (e) {
          e.preventDefault();
          _this9.setValue(_this9.getValue());
        });
      } else {
        var searchField = this.component.searchField;
        var choicesOptions = _objectSpread({
          searchResultLimit: 15,
          removeItemButton: this.component.disabled ? false : _lodash.default.get(this.component, 'removeItemButton', true),
          itemSelectText: '',
          classNames: {
            containerOuter: 'choices form-group formio-choices',
            containerInner: 'form-control'
          },
          addItemText: false,
          placeholder: !!this.component.placeholder,
          placeholderValue: placeholderValue,
          noResultsText: this.t('No results found'),
          noChoicesText: this.t('No choices to choose from'),
          searchPlaceholderValue: this.t('Type to search'),
          shouldSort: false,
          position: this.component.dropdown || 'auto',
          searchEnabled: useSearch,
          searchChoices: !searchField,
          searchFields: this.component.searchFields || (searchField ? ["value.".concat(searchField)] : ['label']),
          fuseOptions: Object.assign({
            include: 'score',
            threshold: _lodash.default.get(this, 'component.searchThreshold', 0.0),
            useExtendedSearch: true,
            ignoreLocation: true
          }, _lodash.default.get(this, 'component.fuseOptions', {})),
          itemComparer: _lodash.default.isEqual,
          resetScrollPosition: false
        }, customOptions);
        if (this.component.dataType === 'object' && this.component.idPath) {
          choicesOptions.valueComparer = function (a, b) {
            return _lodash.default.isEqual(_lodash.default.get(a, _this9.component.idPath), _lodash.default.get(b, _this9.component.idPath));
          };
        }
        var tabIndex = input.tabIndex;
        this.addPlaceholder(input);
        input.setAttribute('dir', this.i18next.dir());
        this.choices = new _choices.default(input, choicesOptions);
        if (this.component.multiple) {
          this.focusableElement = this.choices.input.element;
        } else {
          this.focusableElement = this.choices.containerInner.element;
          this.choices.containerOuter.element.setAttribute('tabIndex', '-1');
          if (useSearch) {
            this.addEventListener(this.choices.containerOuter.element, 'focus', function () {
              return _this9.focusableElement.focus();
            });
          }
        }
        if (this.isInfiniteScrollProvided) {
          this.scrollList = this.choices.choiceList.element;
          this.onScroll = function () {
            var isLoadingAvailable = !_this9.isScrollLoading && _this9.additionalResourcesAvailable && _this9.scrollList.scrollTop + _this9.scrollList.clientHeight >= _this9.scrollList.scrollHeight;
            if (isLoadingAvailable) {
              _this9.isScrollLoading = true;
              _this9.choices.setChoices([{
                value: "".concat(_this9.id, "-loading"),
                label: 'Loading...',
                disabled: true
              }], 'value', 'label');
              _this9.triggerUpdate(_this9.choices.input.element.value);
            }
          };
          this.scrollList.addEventListener('scroll', this.onScroll);
        }
        this.addFocusBlurEvents(this.focusableElement);
        this.focusableElement.setAttribute('tabIndex', tabIndex);
        this.setInputStyles(this.choices.containerOuter.element);

        // If a search field is provided, then add an event listener to update items on search.
        if (this.component.searchField) {
          // Make sure to clear the search when no value is provided.
          if (this.choices && this.choices.input && this.choices.input.element) {
            this.addEventListener(this.choices.input.element, 'input', function (event) {
              _this9.isFromSearch = !!event.target.value;
              if (!event.target.value) {
                _this9.triggerUpdate();
              } else {
                _this9.serverCount = null;
                _this9.downloadedResources = [];
              }
            });
          }
          this.addEventListener(input, 'search', function (event) {
            return _this9.triggerUpdate(event.detail.value);
          });
          this.addEventListener(input, 'stopSearch', function () {
            return _this9.triggerUpdate();
          });
        }
        this.addEventListener(input, 'showDropdown', function () {
          if (_this9.dataValue) {
            _this9.triggerUpdate();
          }
          _this9.update();
        });
        if (placeholderValue && this.choices._isSelectOneElement) {
          this.addEventListener(input, 'removeItem', function () {
            var items = _this9.choices._store.activeItems;
            if (!items.length) {
              _this9.choices._addItem({
                value: placeholderValue,
                label: placeholderValue,
                choiceId: 0,
                groupId: -1,
                customProperties: null,
                placeholder: true,
                keyCode: null
              });
            }
          });
        }
      }

      // Add value options.
      if (this.addValueOptions()) {
        this.restoreValue();
      }

      // Force the disabled state with getters and setters.
      /* eslint-disable no-self-assign */
      this.disabled = this.disabled;
      /* eslint-enable no-self-assign */
      this.triggerUpdate();
    }
  }, {
    key: "restoreValue",
    value: function restoreValue() {
      _get(_getPrototypeOf(SelectComponent.prototype), "restoreValue", this).call(this);
      if (this.treeselect) {
        this.treeselect.updateValue(this.dataValue);
      } else if (this.choices) {
        this.choices.setChoiceByValue(this.dataValue);
      }
    }

    /* eslint-enable max-statements */
  }, {
    key: "update",
    value: function update() {
      if (this.component.dataSrc === 'custom') {
        this.updateCustomItems();
      }

      // Activate the control.
      this.activate();
    }
  }, {
    key: "disabled",
    set: function set(disabled) {
      _set(_getPrototypeOf(SelectComponent.prototype), "disabled", disabled, this, true);
      if (!this.choices) {
        return;
      }
      if (disabled) {
        this.setDisabled(this.choices.containerInner.element, true);
        this.focusableElement.removeAttribute('tabIndex');
        this.choices.disable();
      } else {
        this.setDisabled(this.choices.containerInner.element, false);
        this.focusableElement.setAttribute('tabIndex', this.component.tabindex || 0);
        this.choices.enable();
      }
    }
  }, {
    key: "show",
    value: function show(_show) {
      // If we go from hidden to visible, trigger a refresh.
      var triggerUpdate = _show && this._visible !== _show;
      _show = _get(_getPrototypeOf(SelectComponent.prototype), "show", this).call(this, _show);
      if (triggerUpdate) {
        this.triggerUpdate();
      }
      return _show;
    }

    /**
     * @param {*} value
     * @param {Array} items
     */
  }, {
    key: "addCurrentChoices",
    value: function addCurrentChoices(values, items, keyValue) {
      var _this10 = this;
      if (!values) {
        return false;
      }
      var notFoundValuesToAdd = [];
      var added = values.reduce(function (defaultAdded, value) {
        if (!value) {
          return defaultAdded;
        }
        var found = false;

        // Make sure that `items` and `this.selectOptions` points
        // to the same reference. Because `this.selectOptions` is
        // internal property and all items are populated by
        // `this.addOption` method, we assume that items has
        // 'label' and 'value' properties. This assumption allows
        // us to read correct value from the item.
        var isSelectOptions = items === _this10.selectOptions;
        if (items && items.length) {
          _lodash.default.each(items, function (choice) {
            if (choice._id && value._id && choice._id === value._id) {
              found = true;
              return false;
            }
            var itemValue = keyValue ? choice.value : _this10.itemValue(choice, isSelectOptions);
            found |= _lodash.default.isEqual(itemValue, value);
            return found ? false : true;
          });
        }

        // Add the default option if no item is found.
        if (!found) {
          notFoundValuesToAdd.push({
            value: _this10.itemValue(value),
            label: _this10.itemTemplate(value)
          });
          return true;
        }
        return found || defaultAdded;
      }, false);
      if (notFoundValuesToAdd.length) {
        if (this.choices) {
          this.choices.setChoices(notFoundValuesToAdd, 'value', 'label');
        } else if (this.treeselect) {
          notFoundValuesToAdd.map(function (notFoundValue) {
            _this10.addOptionV2(notFoundValue);
          });
          return true;
          // notFoundValuesToAdd.map(notFoundValue => {
          //   this.addOption(notFoundValue.value, notFoundValue.label);
          // });
          // this.treeselect.mount();
        } else {
          notFoundValuesToAdd.map(function (notFoundValue) {
            _this10.addOption(notFoundValue.value, notFoundValue.label);
          });
        }
      }
      return added;
    }
  }, {
    key: "getView",
    value: function getView(data) {
      return this.component.multiple && Array.isArray(data) ? data.map(this.asString.bind(this)).join(', ') : this.asString(data);
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (this.viewOnly || this.loading || !this.selectOptions.length) {
        return this.dataValue;
      }
      var value = '';
      if (this.choices) {
        value = this.choices.getValue(true);

        // Make sure we don't get the placeholder
        if (!this.component.multiple && this.component.placeholder && value === this.t(this.component.placeholder)) {
          value = this.emptyValue;
        }
      } else if (this.treeselect) {
        value = this.treeselect.value;
        if (!this.component.multiple && Array.isArray(value)) {
          value = value[0] || '';
        }
      } else {
        var values = [];
        _lodash.default.each(this.selectOptions, function (selectOption) {
          if (selectOption.element && selectOption.element.selected) {
            values.push(selectOption.value);
          }
        });
        value = this.component.multiple ? values : values.shift();
      }
      // Choices will return undefined if nothing is selected. We really want '' to be empty.
      if (value === undefined || value === null) {
        value = this.emptyValue;
      }
      return value;
    }
  }, {
    key: "redraw",
    value: function redraw() {
      _get(_getPrototypeOf(SelectComponent.prototype), "redraw", this).call(this);
      this.triggerUpdate();
    }
  }, {
    key: "setValue",
    value: function setValue(value, flags) {
      flags = this.getFlags.apply(this, arguments);
      var previousValue = this.dataValue;
      if (this.component.multiple && !Array.isArray(value)) {
        value = value ? [value] : [];
      }
      var hasPreviousValue = Array.isArray(previousValue) ? previousValue.length : previousValue;
      var hasValue = Array.isArray(value) ? value.length : value;
      var changed = this.hasChanged(value, previousValue);
      this.dataValue = value;

      // Do not set the value if we are loading... that will happen after it is done.
      if (this.loading) {
        return changed;
      }

      // Determine if we need to perform an initial lazyLoad api call if searchField is provided.
      if (this.component.searchField && this.component.lazyLoad && !this.lazyLoadInit && !this.active && !this.selectOptions.length && hasValue) {
        this.loading = true;
        this.lazyLoadInit = true;
        this.triggerUpdate(this.dataValue, true);
        return changed;
      }

      // Add the value options.
      this.addValueOptions();
      if (this.choices) {
        // Now set the value.
        if (hasValue) {
          this.choices.removeActiveItems();
          // Add the currently selected choices if they don't already exist.
          var currentChoices = Array.isArray(this.dataValue) ? this.dataValue : [this.dataValue];
          if (!this.addCurrentChoices(currentChoices, this.selectOptions, true)) {
            this.choices.setChoices(this.selectOptions, 'value', 'label', true);
          }
          this.choices.setChoiceByValue(value);
        } else if (hasPreviousValue) {
          this.choices.removeActiveItems();
        }
      } else if (this.treeselect) {
        if (hasValue) {
          this.treeselect.updateValue(value);
          // TODO: WTF: treeselect.value is always emptied.
          this.treeselect.value = value;
        } else {
          this.treeselect.updateValue(null);
          this.treeselect.value = [];
        }
      } else {
        if (hasValue) {
          var values = Array.isArray(value) ? value : [value];
          _lodash.default.each(this.selectOptions, function (selectOption) {
            _lodash.default.each(values, function (val) {
              if (_lodash.default.isEqual(val, selectOption.value)) {
                selectOption.element.selected = true;
                selectOption.element.setAttribute('selected', 'selected');
                return false;
              }
            });
          });
        } else {
          _lodash.default.each(this.selectOptions, function (selectOption) {
            selectOption.element.selected = false;
            selectOption.element.removeAttribute('selected');
          });
        }
      }
      this.updateOnChange(flags, changed);
      return changed;
    }

    /**
     * Deletes the value of the component.
     */
  }, {
    key: "deleteValue",
    value: function deleteValue() {
      this.setValue('', {
        noUpdateEvent: true
      });
      _lodash.default.unset(this.data, this.key);
    }

    /**
     * Check if a component is eligible for multiple validation
     *
     * @return {boolean}
     */
  }, {
    key: "validateMultiple",
    value: function validateMultiple() {
      // Select component will contain one input when flagged as multiple.
      return false;
    }

    /**
     * Output this select dropdown as a string value.
     * @return {*}
     */
  }, {
    key: "asString",
    value: function asString(value) {
      var _this11 = this;
      value = value || this.getValue();
      if (['values', 'custom'].includes(this.component.dataSrc)) {
        var _ref = this.component.dataSrc === 'values' ? {
            items: this.component.data.values,
            valueProperty: 'value'
          } : {
            items: this.getCustomItems(),
            valueProperty: this.component.valueProperty
          },
          items = _ref.items,
          valueProperty = _ref.valueProperty;
        value = this.component.multiple && Array.isArray(value) ? _lodash.default.filter(items, function (item) {
          return value.includes(item.value);
        }) : valueProperty ? _lodash.default.find(items, [valueProperty, value]) : value;
      }
      if (_lodash.default.isString(value)) {
        return value;
      }
      if (Array.isArray(value)) {
        var _items = [];
        value.forEach(function (item) {
          return _items.push(_this11.itemTemplate(item));
        });
        return _items.length > 0 ? _items.join('<br />') : '-';
      }
      return !_lodash.default.isNil(value) ? this.itemTemplate(value) : '-';
    }
  }, {
    key: "setupValueElement",
    value: function setupValueElement(element) {
      element.innerHTML = this.asString();
    }
  }, {
    key: "destroy",
    value: function destroy() {
      _get(_getPrototypeOf(SelectComponent.prototype), "destroy", this).call(this);
      if (this.choices) {
        this.choices.destroyed = true;
        this.choices.destroy();
        this.choices = null;
      }
      if (this.treeselect) {
        this.treeselect.destroy();
        this.treeselect = null;
      }
    }
  }, {
    key: "focus",
    value: function focus() {
      this.focusableElement.focus();
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key2 = 0; _key2 < _len; _key2++) {
        extend[_key2] = arguments[_key2];
      }
      return _Base.default.schema.apply(_Base.default, [{
        type: 'select',
        label: 'Catalogue',
        key: 'select',
        data: {
          values: [],
          json: '',
          url: '',
          resource: '',
          custom: ''
        },
        limit: 100,
        dataSrc: 'catalog',
        valueProperty: '',
        filter: '',
        searchEnabled: true,
        searchField: '',
        minSearch: 0,
        readOnlyValue: false,
        authenticate: false,
        template: '<span>{{ item.label }}</span>',
        selectFields: '',
        searchThreshold: 0.0,
        fuseOptions: {},
        customOptions: {}
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Catalogue',
        group: 'basic',
        icon: 'fas fa-server',
        weight: 70,
        documentation: 'http://help.form.io/userguide/#select',
        schema: SelectComponent.schema()
      };
    }
  }]);
  return SelectComponent;
}(_Base.default);
exports.default = SelectComponent;