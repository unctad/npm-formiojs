"use strict";

require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.string.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _Radio = _interopRequireDefault(require("../radio/Radio"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var SelectBoxesComponent = /*#__PURE__*/function (_RadioComponent) {
  _inherits(SelectBoxesComponent, _RadioComponent);
  var _super = _createSuper(SelectBoxesComponent);
  function SelectBoxesComponent(component, options, data) {
    var _this;
    _classCallCheck(this, SelectBoxesComponent);
    _this = _super.call(this, component, options, data);
    // multiple is not supported!
    if (component.multiple) {
      delete component.multiple;
    }
    _this.validators = _this.validators.concat(['minSelectedCount', 'maxSelectedCount']);
    _this.component.inputType = 'checkbox';
    return _this;
  }
  _createClass(SelectBoxesComponent, [{
    key: "defaultSchema",
    get: function get() {
      return SelectBoxesComponent.schema();
    }
  }, {
    key: "elementInfo",
    value: function elementInfo() {
      var info = _get(_getPrototypeOf(SelectBoxesComponent.prototype), "elementInfo", this).call(this);
      info.attr.name += '[]';
      info.attr.type = 'checkbox';
      info.attr.class = 'form-check-input';
      return info;
    }
  }, {
    key: "emptyValue",
    get: function get() {
      return this.component.values.reduce(function (prev, value) {
        prev[value.value] = false;
        return prev;
      }, {});
    }

    /**
     * Only empty if the values are all false.
     *
     * @param value
     * @return {boolean}
     */
  }, {
    key: "isEmpty",
    value: function isEmpty(value) {
      var empty = true;
      for (var key in value) {
        if (value.hasOwnProperty(key) && value[key]) {
          empty = false;
          break;
        }
      }
      return empty;
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (this.viewOnly) {
        return this.dataValue;
      }
      var value = {};
      _lodash.default.each(this.inputs, function (input) {
        value[input.value] = !!input.checked;
      });
      return value;
    }

    /**
     * Set the value of this component.
     *
     * @param value
     * @param flags
     */
  }, {
    key: "setValue",
    value: function setValue(value, flags) {
      value = value || {};
      if (_typeof(value) !== 'object') {
        if (typeof value === 'string') {
          value = _defineProperty({}, value, true);
        } else {
          value = {};
        }
      }
      flags = this.getFlags.apply(this, arguments);
      if (Array.isArray(value)) {
        _lodash.default.each(value, function (val) {
          value[val] = true;
        });
      }
      _lodash.default.each(this.inputs, function (input) {
        if (_lodash.default.isUndefined(value[input.value])) {
          value[input.value] = false;
        }
        input.checked = !!value[input.value];
      });
      return this.updateValue(flags);
    }
  }, {
    key: "checkValidity",
    value: function checkValidity(data, dirty, rowData) {
      var _this2 = this;
      var maxCount = this.component.validate.maxSelectedCount;
      if (maxCount) {
        var count = Object.keys(this.validationValue).reduce(function (total, key) {
          if (_this2.validationValue[key]) {
            total++;
          }
          return total;
        }, 0);
        if (count >= maxCount) {
          this.inputs.forEach(function (item) {
            if (!item.checked) {
              item.disabled = true;
            }
          });
          var message = this.component.maxSelectedCountMessage ? this.component.maxSelectedCountMessage : "You can only select up to ".concat(maxCount, " items to continue.");
          this.setCustomValidity(message);
          return false;
        } else {
          this.inputs.forEach(function (item) {
            item.disabled = false;
          });
        }
      }
      return _get(_getPrototypeOf(SelectBoxesComponent.prototype), "checkValidity", this).call(this, data, dirty, rowData);
    }
  }, {
    key: "validationValue",
    get: function get() {
      return _get(_getPrototypeOf(SelectBoxesComponent.prototype), "validationValue", this);
    }
  }, {
    key: "getView",
    value: function getView(value) {
      if (!value) {
        return '';
      }
      return (0, _lodash.default)(this.component.values || []).filter(function (v) {
        return value[v.value];
      }).map('label').join(', ');
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _Radio.default.schema.apply(_Radio.default, [{
        type: 'selectboxes',
        label: 'Select Boxes',
        key: 'selectBoxes',
        inline: false
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Select Boxes',
        group: 'basic',
        icon: 'fa fa-plus-square',
        weight: 60,
        documentation: 'http://help.form.io/userguide/#selectboxes',
        schema: SelectBoxesComponent.schema()
      };
    }
  }]);
  return SelectBoxesComponent;
}(_Radio.default);
exports.default = SelectBoxesComponent;