"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _NestedComponent2 = _interopRequireDefault(require("../nested/NestedComponent"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var PanelComponent = /*#__PURE__*/function (_NestedComponent) {
  _inherits(PanelComponent, _NestedComponent);
  var _super = _createSuper(PanelComponent);
  function PanelComponent(component, options, data) {
    var _this;
    _classCallCheck(this, PanelComponent);
    _this = _super.call(this, component, options, data);
    _this.lazyLoaded = false;
    return _this;
  }
  _createClass(PanelComponent, [{
    key: "defaultSchema",
    get: function get() {
      return PanelComponent.schema();
    }
  }, {
    key: "getContainer",
    value: function getContainer() {
      return this.panelBody;
    }
  }, {
    key: "className",
    get: function get() {
      return "panel panel-".concat(this.component.theme, " ").concat(_get(_getPrototypeOf(PanelComponent.prototype), "className", this));
    }
  }, {
    key: "getCollapseIcon",
    value: function getCollapseIcon() {
      var collapseIcon = this.getIcon(this.collapsed ? 'angle-down' : 'angle-up');
      this.addClass(collapseIcon, 'formio-collapse-icon');
      return collapseIcon;
    }
  }, {
    key: "setPanelBodyDefaultClass",
    value: function setPanelBodyDefaultClass() {
      return 'card-body panel-body panel-collapse collapse ';
    }
  }, {
    key: "getCollapsedClass",
    value: function getCollapsedClass() {
      var collapseClass = this.collapsed ? '' : 'show';
      return this.setPanelBodyDefaultClass() + collapseClass;
    }
  }, {
    key: "setCollapsed",
    value: function setCollapsed(element) {
      _get(_getPrototypeOf(PanelComponent.prototype), "setCollapsed", this).call(this, element);
      if (this.collapseIcon) {
        var newIcon = this.getCollapseIcon();
        this.panelTitle.replaceChild(newIcon, this.collapseIcon);
        this.collapseIcon = newIcon;
        this.panelBody.className = this.getCollapsedClass();
      }
    }

    /**
     * Return if this panel is lazy loadable.
     * @return {boolean}
     */
  }, {
    key: "lazyLoadable",
    get: function get() {
      return !this.options.builder && this.component.lazyLoad && this.component.collapsible && this.collapsed && !this.lazyLoaded;
    }
  }, {
    key: "checkValidity",
    value: function checkValidity(data, dirty) {
      // Make sure to toggle the collapsed state before checking validity.
      if (dirty && this.lazyLoadable) {
        this.lazyLoaded = true;
        this.addComponents();
      }
      return _get(_getPrototypeOf(PanelComponent.prototype), "checkValidity", this).call(this, data, dirty);
    }
  }, {
    key: "addComponents",
    value: function addComponents(element, data, options, state) {
      // If they are lazy loading, then only add the components if they toggle the collapsed state.
      if (this.lazyLoadable) {
        return;
      }
      return _get(_getPrototypeOf(PanelComponent.prototype), "addComponents", this).call(this, element, data, options, state);
    }
  }, {
    key: "toggleCollapse",
    value: function toggleCollapse(toState) {
      console.log('toggleCollapse');
      if (toState !== undefined && toState !== null) {
        if (toState === !this.collapsed) {
          _get(_getPrototypeOf(PanelComponent.prototype), "toggleCollapse", this).call(this, 'ok');
          this.component.collapsed = this.collapsed;
          this.emit('allPanelsCollapsed', this);
        }
      } else {
        if (this.lazyLoadable) {
          this.lazyLoaded = true;
          this.addComponents();
        }
        _get(_getPrototypeOf(PanelComponent.prototype), "toggleCollapse", this).call(this, null);
        //this.collapsed = !this.collapsed;
        this.component.collapsed = this.collapsed;
        this.emit('aPanelCollapsed', this);
      }
    }
  }, {
    key: "build",
    value: function build(state) {
      var _this2 = this;
      this.component.theme = this.component.theme || 'default';
      var panelClass = 'mb-2 card border ';
      panelClass += "panel panel-".concat(this.component.theme, " ");
      panelClass += this.component.customClass;
      if (this.component.hidden) {
        panelClass += ' hide';
      }
      if (!this.component.title && this.options.builder) {
        panelClass += ' card-no-header ';
      }
      this.element = this.ce('div', {
        id: this.id,
        class: panelClass
      });
      this.element.component = this;
      this.panelBody = this.ce('div', {
        class: 'card-body panel-body'
      });
      if (this.component.title && (this.options.builder || !this.component.hideLabel)) {
        var heading = this.ce('div', {
          class: "card-header bg-".concat(this.component.theme, " panel-heading")
        });
        this.panelTitle = this.ce('h4', {
          class: 'mb-0 card-title panel-title'
        });
        if (this.component.hideLabel) {
          this.panelTitle.classList.add('hidden-label');
        }
        if (this.component.collapsible) {
          this.collapseIcon = this.getCollapseIcon();
          this.panelTitle.appendChild(this.collapseIcon);
          this.panelTitle.appendChild(this.text(' '));
        }
        if (this.options.builder) {
          this.panelTitle.appendChild(this.generateLabelElement(this.component.title, this.component.workingTitle));
        } else {
          this.panelTitle.appendChild(this.text(this.component.title));
        }
        this.createTooltip(this.panelTitle);
        heading.appendChild(this.panelTitle);
        this.setCollapseHeader(heading);
        this.element.appendChild(heading);
      } else {
        this.createTooltip(this.panelBody, this.component, "".concat(this.iconClass('question-sign'), " text-muted formio-hide-label-panel-tooltip"));
      }
      this.addComponents(null, null, null, state);
      this.element.appendChild(this.panelBody);
      this.setCollapsed();
      this.attachLogic();
      this.on('goOpenPanels', function () {
        _this2.toggleCollapse(true);
      });
      this.on('goClosePanels', function () {
        _this2.toggleCollapse(false);
      });
    }
  }, {
    key: "onAnyChanged",
    value: function onAnyChanged() {
      if (this.options.builder && this.panelTitle && this.panelTitle.children[0]) {
        this.panelTitle.innerHTML = '';
        if (this.component.collapsible) {
          this.collapseIcon = this.getCollapseIcon();
          this.panelTitle.appendChild(this.collapseIcon);
          this.panelTitle.appendChild(this.text(' '));
        }
        if (this.options.builder) {
          this.panelTitle.appendChild(this.generateLabelElement(this.component.title, this.component.workingTitle));
        } else {
          this.panelTitle.appendChild(this.text(this.component.title));
        }
        this.createTooltip(this.panelTitle);
      }
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _NestedComponent2.default.schema.apply(_NestedComponent2.default, [{
        label: 'Block',
        type: 'panel',
        key: 'panel',
        title: '',
        theme: 'default',
        breadcrumb: 'default',
        components: [],
        clearOnHide: false,
        input: false,
        tableView: false,
        dataGridLabel: true,
        persistent: false,
        lazyLoad: false,
        collapsible: true,
        collapsed: false
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Block',
        icon: 'fa fa-list-alt',
        group: 'layout',
        documentation: 'http://help.form.io/userguide/#panels',
        weight: 1,
        schema: PanelComponent.schema()
      };
    }
  }]);
  return PanelComponent;
}(_NestedComponent2.default);
exports.default = PanelComponent;