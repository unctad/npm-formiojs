"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  key: 'labelRow',
  hidden: true,
  calculateValue: function calculateValue(context) {
    return context.data.title;
  }
}, {
  weight: 1,
  type: 'textfield',
  input: true,
  placeholder: 'Panel Title',
  label: 'Title',
  key: 'title',
  tooltip: 'The title text that appears in the header of this panel.'
}, {
  weight: 20,
  type: 'textarea',
  input: true,
  key: 'tooltip',
  label: 'Tooltip',
  placeholder: 'To add a tooltip to this field, enter text here.',
  tooltip: 'Adds a tooltip to the side of this field.'
}, {
  weight: 30,
  type: 'select',
  input: true,
  label: 'Theme',
  key: 'theme',
  dataSrc: 'values',
  data: {
    values: [{
      label: 'Default',
      value: 'default'
    }, {
      label: 'Primary',
      value: 'primary'
    }, {
      label: 'Info',
      value: 'info'
    }, {
      label: 'Success',
      value: 'success'
    }, {
      label: 'Danger',
      value: 'danger'
    }, {
      label: 'Warning',
      value: 'warning'
    }]
  }
}, {
  key: 'labelOptionsRow',
  hidden: true,
  ignore: true
}, {
  key: 'titleRow',
  type: 'columns',
  weight: 1,
  columns: [{
    type: 'column',
    components: [{
      weight: 2,
      type: 'textfield',
      input: true,
      placeholder: 'Block Title',
      label: 'Title',
      key: 'title',
      tooltip: 'The title text that appears in the header of this block.'
    }, {
      type: 'column',
      components: [{
        weight: 10,
        type: 'checkbox',
        label: 'Hide Label',
        tooltip: 'Hide the label of this component. This allows you to show the label in the form builder, but not when it is rendered.',
        key: 'hideLabel',
        input: true,
        logic: [{
          name: 'label logic',
          trigger: {
            type: 'javascript',
            javascript: 'result=instance.root.editComponent.inDataGrid;'
          },
          actions: [{
            name: 'set label',
            type: 'property',
            property: {
              label: 'Label',
              value: 'label',
              type: 'string'
            },
            text: 'Hide Column Label'
          }]
        }]
      }]
    }]
  }, {
    type: 'column',
    components: []
  }]
}, {
  key: 'placeholderRow',
  type: 'columns',
  weight: 3,
  ignore: true,
  hidden: true
}, {
  key: 'tooltipRow',
  type: 'columns',
  weight: 3,
  columns: [{
    type: 'column',
    components: [{
      weight: 300,
      type: 'textarea',
      input: true,
      key: 'tooltip',
      label: 'Tooltip',
      rows: 1,
      placeholder: 'To add a tooltip to this field, enter text here.',
      tooltip: 'Adds a tooltip to the side of this field.'
    }]
  }, {
    type: 'column',
    components: [{
      weight: 30,
      type: 'select',
      input: true,
      label: 'Theme',
      key: 'theme',
      dataSrc: 'values',
      data: {
        values: [{
          label: 'Default',
          value: 'default'
        }, {
          label: 'Primary',
          value: 'primary'
        }, {
          label: 'Info',
          value: 'info'
        }, {
          label: 'Success',
          value: 'success'
        }, {
          label: 'Danger',
          value: 'danger'
        }, {
          label: 'Warning',
          value: 'warning'
        }]
      }
    }]
  }, {
    type: 'column',
    components: []
  }]
}, {
  key: 'description',
  customClass: 'advanced-field'
}, {
  key: 'checkboxesRow',
  customClass: 'advanced-field'
}, {
  weight: 40,
  type: 'fieldset',
  input: false,
  components: [{
    type: 'select',
    input: true,
    label: 'Breadcrumb Type',
    key: 'breadcrumb',
    dataSrc: 'values',
    data: {
      values: [{
        label: 'Default',
        value: 'default'
      }, {
        label: 'Condensed',
        value: 'condensed'
      }, {
        label: 'Hidden',
        value: 'none'
      }]
    }
  }, {
    input: true,
    type: 'checkbox',
    label: 'Allow click on Breadcrumb',
    key: 'breadcrumbClickable',
    defaultValue: true,
    conditional: {
      json: {
        '!==': [{
          var: 'data.breadcrumb'
        }, 'none']
      }
    }
  }, {
    weight: 50,
    label: 'Panel Navigation Buttons',
    optionsLabelPosition: 'right',
    values: [{
      label: 'Previous',
      value: 'previous'
    }, {
      label: 'Cancel',
      value: 'cancel'
    }, {
      label: 'Next',
      value: 'next'
    }],
    inline: true,
    type: 'selectboxes',
    key: 'buttonSettings',
    input: true,
    inputType: 'checkbox',
    defaultValue: {
      previous: true,
      cancel: true,
      next: true
    }
  }],
  customClass: 'advanced-field',
  customConditional: function customConditional(context) {
    return context.instance.root.editForm.display === 'wizard';
  }
}, {
  key: 'collapsibleRow',
  type: 'columns',
  weight: 702,
  columns: [{
    type: 'column',
    components: [{
      weight: 650,
      type: 'checkbox',
      label: 'Collapsible',
      tooltip: 'If checked, this will turn this Panel into a collapsible panel.',
      key: 'collapsible',
      input: true
    }]
  }, {
    type: 'column',
    components: [{
      weight: 651,
      type: 'checkbox',
      label: 'Initially Collapsed',
      tooltip: 'Determines the initial collapsed state of this Panel.',
      key: 'collapsed',
      input: true,
      conditional: {
        json: {
          '===': [{
            var: 'data.collapsible'
          }, true]
        }
      }
    }]
  }, {
    type: 'column',
    components: [{
      weight: 652,
      type: 'checkbox',
      label: 'Lazy Load Contents',
      tooltip: 'Lazy loads the contents only when the panel is opened.',
      key: 'lazyLoad',
      input: true,
      customClass: 'advanced-field',
      conditional: {
        json: {
          '===': [{
            var: 'data.collapsed'
          }, true]
        }
      }
    }]
  }]
}];
exports.default = _default;