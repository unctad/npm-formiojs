"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.object.get-prototype-of.js");
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.promise.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
require("core-js/modules/es.symbol.async-iterator.js");
require("core-js/modules/es.symbol.to-string-tag.js");
require("core-js/modules/es.json.to-string-tag.js");
require("core-js/modules/es.math.to-string-tag.js");
require("core-js/modules/web.dom-collections.for-each.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.typed-array.uint8-array.js");
require("core-js/modules/es.typed-array.copy-within.js");
require("core-js/modules/es.typed-array.every.js");
require("core-js/modules/es.typed-array.fill.js");
require("core-js/modules/es.typed-array.filter.js");
require("core-js/modules/es.typed-array.find.js");
require("core-js/modules/es.typed-array.find-index.js");
require("core-js/modules/es.typed-array.for-each.js");
require("core-js/modules/es.typed-array.includes.js");
require("core-js/modules/es.typed-array.index-of.js");
require("core-js/modules/es.typed-array.iterator.js");
require("core-js/modules/es.typed-array.join.js");
require("core-js/modules/es.typed-array.last-index-of.js");
require("core-js/modules/es.typed-array.map.js");
require("core-js/modules/es.typed-array.reduce.js");
require("core-js/modules/es.typed-array.reduce-right.js");
require("core-js/modules/es.typed-array.reverse.js");
require("core-js/modules/es.typed-array.set.js");
require("core-js/modules/es.typed-array.slice.js");
require("core-js/modules/es.typed-array.some.js");
require("core-js/modules/es.typed-array.sort.js");
require("core-js/modules/es.typed-array.subarray.js");
require("core-js/modules/es.typed-array.to-locale-string.js");
require("core-js/modules/es.typed-array.to-string.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.array.splice.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.number.to-fixed.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.regexp.constructor.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.string.search.js");
require("core-js/modules/es.array.slice.js");
var _Base = _interopRequireDefault(require("../base/Base"));
var _utils = require("../../utils/utils");
var _downloadjs = _interopRequireDefault(require("downloadjs"));
var _lodash = _interopRequireDefault(require("lodash"));
var _Formio = _interopRequireDefault(require("../../Formio"));
var _nativePromiseOnly = _interopRequireDefault(require("native-promise-only"));
var _fileProcessor = _interopRequireDefault(require("../../providers/processor/fileProcessor"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, defineProperty = Object.defineProperty || function (obj, key, desc) { obj[key] = desc.value; }, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return defineProperty(generator, "_invoke", { value: makeInvokeMethod(innerFn, self, context) }), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; defineProperty(this, "_invoke", { value: function value(method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; } function maybeInvokeDelegate(delegate, context) { var methodName = context.method, method = delegate.iterator[methodName]; if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel; var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), defineProperty(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (val) { var object = Object(val), keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
// canvas.toBlob polyfill.
if (!HTMLCanvasElement.prototype.toBlob) {
  Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
    value: function value(callback, type, quality) {
      var canvas = this;
      setTimeout(function () {
        var binStr = atob(canvas.toDataURL(type, quality).split(',')[1]),
          len = binStr.length,
          arr = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        callback(new Blob([arr], {
          type: type || 'image/png'
        }));
      });
    }
  });
}
var FileComponent = /*#__PURE__*/function (_BaseComponent) {
  _inherits(FileComponent, _BaseComponent);
  var _super = _createSuper(FileComponent);
  function FileComponent(component, options, data) {
    var _this;
    _classCallCheck(this, FileComponent);
    _this = _super.call(this, component, options, data);

    // Called when our files are ready.
    _this.filesReady = new _nativePromiseOnly.default(function (resolve, reject) {
      _this.filesReadyResolve = resolve;
      _this.filesReadyReject = reject;
    });
    _this.loadingImages = [];
    _this.support = {
      filereader: typeof FileReader != 'undefined',
      dnd: 'draggable' in document.createElement('span'),
      formdata: !!window.FormData,
      progress: 'upload' in new XMLHttpRequest()
    };
    return _this;
  }
  _createClass(FileComponent, [{
    key: "dataReady",
    get: function get() {
      return this.filesReady;
    }
  }, {
    key: "defaultSchema",
    get: function get() {
      return FileComponent.schema();
    }
  }, {
    key: "emptyValue",
    get: function get() {
      return [];
    }
  }, {
    key: "getValue",
    value: function getValue() {
      return this.dataValue;
    }
  }, {
    key: "getView",
    value: function getView(value) {
      if (_lodash.default.isArray(value)) {
        return _lodash.default.map(value, 'originalName').join(', ');
      }
      return _lodash.default.get(value, 'originalName', '');
    }
  }, {
    key: "loadImage",
    value: function loadImage(fileInfo) {
      return this.fileService.downloadFile(fileInfo).then(function (result) {
        return result.url;
      });
    }
  }, {
    key: "setValue",
    value: function setValue(value) {
      var _this2 = this;
      var newValue = value || [];
      var changed = this.hasChanged(newValue, this.dataValue);
      this.dataValue = newValue;
      if (this.component.image) {
        this.loadingImages = [];
        var images = Array.isArray(newValue) ? newValue : [newValue];
        images.map(function (fileInfo) {
          if (fileInfo && Object.keys(fileInfo).length) {
            _this2.loadingImages.push(_this2.loadImage(fileInfo));
          }
        });
        if (this.loadingImages.length) {
          _nativePromiseOnly.default.all(this.loadingImages).then(function () {
            _this2.refreshDOM();
            setTimeout(function () {
              return _this2.filesReadyResolve();
            }, 100);
          }).catch(function () {
            return _this2.filesReadyReject();
          });
        }
      } else {
        this.refreshDOM();
        this.filesReadyResolve();
      }
      return changed;
    }
  }, {
    key: "defaultValue",
    get: function get() {
      var value = _get(_getPrototypeOf(FileComponent.prototype), "defaultValue", this);
      if (_lodash.default.isEqual(value, []) && this.options.flatten) {
        return [{
          storage: '',
          name: '',
          size: 0,
          type: '',
          originalName: ''
        }];
      }
      return Array.isArray(value) ? value : [];
    }
  }, {
    key: "hasTypes",
    get: function get() {
      return this.component.fileTypes && Array.isArray(this.component.fileTypes) && this.component.fileTypes.length !== 0 && (this.component.fileTypes[0].label !== '' || this.component.fileTypes[0].value !== '');
    }

    // File is always an array.
  }, {
    key: "validateMultiple",
    value: function validateMultiple() {
      return false;
    }
  }, {
    key: "build",
    value: function build() {
      // Restore the value.
      this.restoreValue();
      var labelAtTheBottom = this.component.labelPosition === 'bottom';
      this.createElement();
      if (!labelAtTheBottom) {
        this.createLabel(this.element);
      }
      this.inputsContainer = this.ce('div');
      this.errorContainer = this.inputsContainer;
      this.createErrorElement();
      this.listContainer = this.buildList();
      this.inputsContainer.appendChild(this.listContainer);
      this.uploadContainer = this.buildUpload();
      this.hiddenFileInputElement = this.buildHiddenFileInput();
      this.hook('input', this.hiddenFileInputElement, this.inputsContainer);
      this.inputsContainer.appendChild(this.hiddenFileInputElement);
      this.inputsContainer.appendChild(this.uploadContainer);
      this.addWarnings(this.inputsContainer);
      this.buildUploadStatusList(this.inputsContainer);
      this.setInputStyles(this.inputsContainer);
      this.element.appendChild(this.inputsContainer);
      if (labelAtTheBottom) {
        this.createLabel(this.element);
      }
      this.createDescription(this.element);
      this.autofocus();

      // Disable if needed.
      if (this.shouldDisable) {
        this.disabled = true;
      }
      this.attachLogic();
    }
  }, {
    key: "refreshDOM",
    value: function refreshDOM() {
      // Don't refresh before the initial render.
      if (this.listContainer && this.uploadContainer) {
        // Refresh file list.
        var newList = this.buildList();
        this.inputsContainer.replaceChild(newList, this.listContainer);
        this.listContainer = newList;

        // Refresh upload container.
        var newUpload = this.buildUpload();
        this.inputsContainer.replaceChild(newUpload, this.uploadContainer);
        this.uploadContainer = newUpload;
      }
    }
  }, {
    key: "buildList",
    value: function buildList() {
      if (this.component.image) {
        return this.buildImageList();
      } else {
        return this.buildFileList();
      }
    }
  }, {
    key: "buildFileList",
    value: function buildFileList() {
      var _this3 = this;
      var value = this.dataValue;
      return this.ce('ul', {
        class: 'list-group list-group-striped'
      }, [this.ce('li', {
        class: 'list-group-item list-group-header hidden-xs hidden-sm'
      }, this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col-md-1'
      }), this.ce('div', {
        class: "col-md-".concat(this.hasTypes ? '7' : '9')
      }, this.ce('strong', {}, this.text('File Name'))), this.ce('div', {
        class: 'col-md-2'
      }, this.ce('strong', {}, this.text('Size'))), this.hasTypes ? this.ce('div', {
        class: 'col-md-2'
      }, this.ce('strong', {}, this.text('Type'))) : null])), Array.isArray(value) ? value.map(function (fileInfo, index) {
        return _this3.createFileListItem(fileInfo, index);
      }) : null]);
    }
  }, {
    key: "buildHiddenFileInput",
    value: function buildHiddenFileInput() {
      var _this4 = this;
      // Input needs to be in DOM and "visible" (opacity 0 is fine) for IE to display file dialog.
      return this.ce('input', {
        type: 'file',
        style: 'opacity: 0; position: absolute;',
        tabindex: -1,
        // prevent focus
        onChange: function onChange() {
          _this4.upload(_this4.hiddenFileInputElement.files);
          _this4.hiddenFileInputElement.value = '';
        }
      });
    }
  }, {
    key: "createFileListItem",
    value: function createFileListItem(fileInfo, index) {
      var _this5 = this;
      var fileService = this.fileService;
      return this.ce('li', {
        class: 'list-group-item'
      }, this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col-md-1'
      }, !this.disabled && !this.shouldDisable ? this.ce('i', {
        class: this.iconClass('remove'),
        onClick: function onClick(event) {
          if (fileInfo && _this5.component.storage === 'url') {
            fileService.makeRequest('', fileInfo.url, 'delete');
          }
          event.preventDefault();
          _this5.splice(index);
          _this5.refreshDOM();
        }
      }) : null), this.ce('div', {
        class: "col-md-".concat(this.hasTypes ? '7' : '9')
      }, this.createFileLink(fileInfo)), this.ce('div', {
        class: 'col-md-2'
      }, this.fileSize(fileInfo.size)), this.hasTypes ? this.ce('div', {
        class: 'col-md-2'
      }, this.createTypeSelect(fileInfo)) : null]));
    }
  }, {
    key: "createFileLink",
    value: function createFileLink(file) {
      if (this.options.uploadOnly) {
        return file.originalName || file.name;
      }
      return this.ce('a', {
        href: file.url,
        target: '_blank',
        onClick: this.getFile.bind(this, file)
      }, file.originalName || file.name);
    }
  }, {
    key: "createTypeSelect",
    value: function createTypeSelect(file) {
      var _this6 = this;
      return this.ce('select', {
        class: 'file-type',
        onChange: function onChange(event) {
          file.fileType = event.target.value;
          _this6.triggerChange();
        }
      }, this.component.fileTypes.map(function (type) {
        return _this6.ce('option', {
          value: type.value,
          class: 'test',
          selected: type.value === file.fileType ? 'selected' : undefined
        }, type.label);
      }));
    }
  }, {
    key: "buildImageList",
    value: function buildImageList() {
      var _this7 = this;
      var value = this.dataValue;
      return this.ce('div', {}, Array.isArray(value) ? value.map(function (fileInfo, index) {
        return _this7.createImageListItem(fileInfo, index);
      }) : null);
    }
  }, {
    key: "createImageListItem",
    value: function createImageListItem(fileInfo, index) {
      var _this8 = this;
      var image = this.ce('img', {
        alt: fileInfo.originalName || fileInfo.name,
        style: "width:".concat(this.component.imageSize, "px")
      });
      if (this.loadingImages[index]) {
        this.loadingImages[index].then(function (url) {
          image.src = url;
        });
      }
      return this.ce('div', {}, this.ce('span', {}, [image, !this.disabled ? this.ce('i', {
        class: this.iconClass('remove'),
        onClick: function onClick(event) {
          if (fileInfo && _this8.component.storage === 'url') {
            _this8.fileService.makeRequest('', fileInfo.url, 'delete');
          }
          event.preventDefault();
          _this8.splice(index);
          _this8.refreshDOM();
        }
      }) : null]));
    }
  }, {
    key: "startVideo",
    value: function startVideo() {
      var _this9 = this;
      navigator.getMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
      navigator.getMedia({
        video: {
          width: {
            min: 640,
            ideal: 1920
          },
          height: {
            min: 400,
            ideal: 1080
          },
          aspectRatio: {
            ideal: 1.7777777778
          }
        },
        audio: false
      }, function (stream) {
        if (navigator.mozGetUserMedia) {
          _this9.video.mozSrcObject = stream;
        } else {
          _this9.video.srcObject = stream;
        }
        var width = parseInt(_this9.component.webcamSize) || 320;
        _this9.video.setAttribute('width', width);
        _this9.video.play();
      }, function (err) {
        console.log(err);
      });
    }
  }, {
    key: "takePicture",
    value: function takePicture() {
      var _this10 = this;
      this.canvas.setAttribute('width', this.video.videoWidth);
      this.canvas.setAttribute('height', this.video.videoHeight);
      this.canvas.getContext('2d').drawImage(this.video, 0, 0);
      this.canvas.toBlob(function (blob) {
        blob.name = "photo-".concat(Date.now(), ".png");
        _this10.upload([blob]);
      });
    }
  }, {
    key: "buildUpload",
    value: function buildUpload() {
      var _this11 = this;
      // Drop event must change this pointer so need a reference to parent this.
      var element = this;
      // Declare Camera Instace
      var Camera;
      // Implement Camera file upload for WebView Apps.
      if (this.component.image && (navigator.camera || Camera)) {
        var camera = navigator.camera || Camera;
        return this.ce('div', {}, !this.disabled && (this.component.multiple || this.dataValue.length === 0) ? this.ce('div', {
          class: 'fileSelector'
        }, [this.ce('button', {
          class: 'btn btn-primary',
          onClick: function onClick(event) {
            event.preventDefault();
            camera.getPicture(function (success) {
              window.resolveLocalFileSystemURL(success, function (fileEntry) {
                fileEntry.file(function (file) {
                  _this11.upload([file]);
                });
              });
            }, null, {
              sourceType: camera.PictureSourceType.PHOTOLIBRARY
            });
          }
        }, [this.ce('i', {
          class: this.iconClass('book')
        }), this.text('Gallery')]), this.ce('button', {
          class: 'btn btn-primary',
          onClick: function onClick(event) {
            event.preventDefault();
            camera.getPicture(function (success) {
              window.resolveLocalFileSystemURL(success, function (fileEntry) {
                fileEntry.file(function (file) {
                  _this11.upload([file]);
                });
              });
            }, null, {
              sourceType: camera.PictureSourceType.CAMERA,
              encodingType: camera.EncodingType.PNG,
              mediaType: camera.MediaType.PICTURE,
              saveToPhotoAlbum: true,
              correctOrientation: false
            });
          }
        }, [this.ce('i', {
          class: this.iconClass('camera')
        }), this.text('Camera')])]) : this.ce('div'));
      }

      // If this is disabled or a single value with a value, don't show the upload div.
      var render = this.ce('div', {}, !this.disabled && (this.component.multiple || this.dataValue.length === 0) ? !this.cameraMode ? [this.ce('div', {
        class: 'fileSelector',
        onDragover: function onDragover(event) {
          this.className = 'fileSelector fileDragOver';
          event.preventDefault();
        },
        onDragleave: function onDragleave(event) {
          this.className = 'fileSelector';
          event.preventDefault();
        },
        onDrop: function onDrop(event) {
          this.className = 'fileSelector';
          event.preventDefault();
          element.upload(event.dataTransfer.files);
          return false;
        }
      }, [this.ce('i', {
        class: this.iconClass('cloud-upload')
      }), this.text(' '), this.text('Drop files to attach, or'), this.text(' '), this.buildBrowseLink(), this.component.webcam ? [this.text(', or'), this.text(' '), this.ce('a', {
        href: '#',
        title: 'Use Web Camera',
        onClick: function onClick(event) {
          event.preventDefault();
          _this11.cameraMode = !_this11.cameraMode;
          _this11.refreshDOM();
        }
      }, this.ce('i', {
        class: this.iconClass('camera')
      }))] : null, this.buildFileProcessingLoader()])] : [this.ce('div', {}, [this.video = this.ce('video', {
        class: 'video',
        autoplay: true
      }), this.canvas = this.ce('canvas', {
        style: 'display: none;'
      }), this.photo = this.ce('img')]), this.ce('div', {
        class: 'btn btn-primary',
        onClick: function onClick() {
          _this11.takePicture();
        }
      }, 'Take Photo'), this.ce('div', {
        class: 'btn btn-default',
        onClick: function onClick() {
          _this11.cameraMode = !_this11.cameraMode;
          _this11.refreshDOM();
        }
      }, 'Switch to file upload')] : this.ce('div'));
      if (this.cameraMode) {
        this.startVideo();
      }
      return render;
    }
  }, {
    key: "buildBrowseLink",
    value: function buildBrowseLink() {
      var _this12 = this;
      this.browseLink = this.ce('a', {
        href: '#',
        onClick: function onClick(event) {
          event.preventDefault();
          // There is no direct way to trigger a file dialog. To work around this, create an input of type file and trigger
          // a click event on it.
          if (typeof _this12.hiddenFileInputElement.trigger === 'function') {
            _this12.hiddenFileInputElement.trigger('click');
          } else {
            _this12.hiddenFileInputElement.click();
          }
        },
        class: 'browse'
      }, this.text('browse'));
      this.addFocusBlurEvents(this.browseLink);
      return this.browseLink;
    }
  }, {
    key: "buildFileProcessingLoader",
    value: function buildFileProcessingLoader() {
      this.fileProcessingLoader = this.ce('div', {
        class: 'loader-wrapper'
      }, [this.ce('div', {
        class: 'loader'
      })]);
      return this.fileProcessingLoader;
    }
  }, {
    key: "buildUploadStatusList",
    value: function buildUploadStatusList(container) {
      var list = this.ce('div');
      this.uploadStatusList = list;
      container.appendChild(list);
    }
  }, {
    key: "addWarnings",
    value: function addWarnings(container) {
      var hasWarnings = false;
      var warnings = this.ce('div', {
        class: 'alert alert-warning'
      });
      if (!this.component.storage) {
        hasWarnings = true;
        warnings.appendChild(this.ce('p').appendChild(this.text('No storage has been set for this field. File uploads are disabled until storage is set up.')));
      }
      if (!this.support.dnd) {
        hasWarnings = true;
        warnings.appendChild(this.ce('p').appendChild(this.text('File Drag/Drop is not supported for this browser.')));
      }
      if (!this.support.filereader) {
        hasWarnings = true;
        warnings.appendChild(this.ce('p').appendChild(this.text('File API & FileReader API not supported.')));
      }
      if (!this.support.formdata) {
        hasWarnings = true;
        warnings.appendChild(this.ce('p').appendChild(this.text('XHR2\'s FormData is not supported.')));
      }
      if (!this.support.progress) {
        hasWarnings = true;
        warnings.appendChild(this.ce('p').appendChild(this.text('XHR2\'s upload progress isn\'t supported.')));
      }
      if (hasWarnings) {
        container.appendChild(warnings);
      }
    }

    /* eslint-disable max-len */
  }, {
    key: "fileSize",
    value: function fileSize(a, b, c, d, e) {
      return "".concat((b = Math, c = b.log, d = 1024, e = c(a) / c(d) | 0, a / b.pow(d, e)).toFixed(2), " ").concat(e ? "".concat('kMGTPEZY'[--e], "B") : 'Bytes');
    }

    /* eslint-enable max-len */
  }, {
    key: "createUploadStatus",
    value: function createUploadStatus(fileUpload) {
      var _this13 = this;
      var container;
      return container = this.ce('div', {
        class: "file".concat(fileUpload.status === 'error' ? ' has-error' : '')
      }, [this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'fileName control-label col-sm-10'
      }, [fileUpload.originalName, this.ce('i', {
        class: this.iconClass('remove'),
        onClick: function onClick() {
          return _this13.removeChildFrom(container, _this13.uploadStatusList);
        }
      })]), this.ce('div', {
        class: 'fileSize control-label col-sm-2 text-right'
      }, this.fileSize(fileUpload.size))]), this.ce('div', {
        class: 'row'
      }, [this.ce('div', {
        class: 'col-sm-12'
      }, [fileUpload.status === 'progress' ? this.ce('div', {
        class: 'progress'
      }, this.ce('div', {
        class: 'progress-bar',
        role: 'progressbar',
        'aria-valuenow': fileUpload.progress,
        'aria-valuemin': 0,
        'aria-valuemax': 100,
        style: "width:".concat(fileUpload.progress, "%")
      }, this.ce('span', {
        class: 'sr-only'
      }, "".concat(fileUpload.progress, "% Complete")))) : this.ce('div', {
        class: "bg-".concat(fileUpload.status)
      }, fileUpload.message)])])]);
    }

    /* eslint-disable max-depth */
  }, {
    key: "globStringToRegex",
    value: function globStringToRegex(str) {
      var regexp = '',
        excludes = [];
      if (str.length > 2 && str[0] === '/' && str[str.length - 1] === '/') {
        regexp = str.substring(1, str.length - 1);
      } else {
        var split = str.split(',');
        if (split.length > 1) {
          for (var i = 0; i < split.length; i++) {
            var r = this.globStringToRegex(split[i]);
            if (r.regexp) {
              regexp += "(".concat(r.regexp, ")");
              if (i < split.length - 1) {
                regexp += '|';
              }
            } else {
              excludes = excludes.concat(r.excludes);
            }
          }
        } else {
          if (str.indexOf('!') === 0) {
            excludes.push("^((?!".concat(this.globStringToRegex(str.substring(1)).regexp, ").)*$"));
          } else {
            if (str.indexOf('.') === 0) {
              str = "*".concat(str);
            }
            regexp = "^".concat(str.replace(new RegExp('[.\\\\+*?\\[\\^\\]$(){}=!<>|:\\-]', 'g'), '\\$&'), "$");
            regexp = regexp.replace(/\\\*/g, '.*').replace(/\\\?/g, '.');
          }
        }
      }
      return {
        regexp: regexp,
        excludes: excludes
      };
    }

    /* eslint-enable max-depth */
  }, {
    key: "translateScalars",
    value: function translateScalars(str) {
      if (typeof str === 'string') {
        if (str.search(/kb/i) === str.length - 2) {
          return parseFloat(str.substring(0, str.length - 2) * 1024);
        } else if (str.search(/mb/i) === str.length - 2) {
          return parseFloat(str.substring(0, str.length - 2) * 1048576);
        } else if (str.search(/gb/i) === str.length - 2) {
          return parseFloat(str.substring(0, str.length - 2) * 1073741824);
        } else if (str.search(/b/i) === str.length - 1) {
          return parseFloat(str.substring(0, str.length - 1));
        } else if (str.search(/s/i) === str.length - 1) {
          return parseFloat(str.substring(0, str.length - 1));
        } else if (str.search(/m/i) === str.length - 1) {
          return parseFloat(str.substring(0, str.length - 1) * 60);
        } else if (str.search(/h/i) === str.length - 1) {
          return parseFloat(str.substring(0, str.length - 1) * 3600);
        }
      }
      return str;
    }
  }, {
    key: "validatePattern",
    value: function validatePattern(file, val) {
      if (!val) {
        return true;
      }
      var pattern = this.globStringToRegex(val);
      var valid = true;
      if (pattern.regexp && pattern.regexp.length) {
        var regexp = new RegExp(pattern.regexp, 'i');
        valid = file.type != null && regexp.test(file.type) || file.name != null && regexp.test(file.name);
      }
      var len = pattern.excludes.length;
      while (len--) {
        var exclude = new RegExp(pattern.excludes[len], 'i');
        valid = valid && (file.type == null || exclude.test(file.type)) && (file.name == null || exclude.test(file.name));
      }
      return valid;
    }
  }, {
    key: "validateMinSize",
    value: function validateMinSize(file, val) {
      return file.size + 0.1 >= this.translateScalars(val);
    }
  }, {
    key: "validateMaxSize",
    value: function validateMaxSize(file, val) {
      return file.size - 0.1 <= this.translateScalars(val);
    }
  }, {
    key: "upload",
    value: function upload(files) {
      var _this14 = this;
      // Only allow one upload if not multiple.
      if (!this.component.multiple) {
        files = Array.prototype.slice.call(files, 0, 1);
      }
      if (this.component.storage && files && files.length) {
        // files is not really an array and does not have a forEach method, so fake it.
        /* eslint-disable max-statements */
        Array.prototype.forEach.call(files, /*#__PURE__*/function () {
          var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(file) {
            var fileName, fileUpload, dir, fileService, uploadStatus, cssClass, submitComponent, submitButton, array, uploadsInProgress, _this14$component, storage, url, options, processedFile, _originalStatus, fileProcessorHandler, _originalStatus2, originalStatus;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  fileName = (0, _utils.uniqueName)(file.name, _this14.component.fileNameTemplate, _this14.evalContext());
                  fileUpload = {
                    originalName: file.name,
                    name: fileName,
                    size: file.size,
                    status: 'info',
                    message: 'Starting upload'
                  }; // Check file pattern
                  if (_this14.component.filePattern && !_this14.validatePattern(file, _this14.component.filePattern)) {
                    fileUpload.status = 'error';
                    fileUpload.message = "File is the wrong type; it must be ".concat(_this14.component.filePattern);
                  }

                  // Check file minimum size
                  if (_this14.component.fileMinSize && !_this14.validateMinSize(file, _this14.component.fileMinSize)) {
                    fileUpload.status = 'error';
                    fileUpload.message = "File is too small; it must be at least ".concat(_this14.component.fileMinSize);
                  }

                  // Check file maximum size
                  if (_this14.component.fileMaxSize && !_this14.validateMaxSize(file, _this14.component.fileMaxSize)) {
                    fileUpload.status = 'error';
                    fileUpload.message = "File is too big; it must be at most ".concat(_this14.component.fileMaxSize);
                  }

                  // Get a unique name for this file to keep file collisions from occurring.
                  dir = _this14.interpolate(_this14.component.dir || '');
                  fileService = _this14.fileService;
                  if (!fileService) {
                    fileUpload.status = 'error';
                    fileUpload.message = 'File Service not provided.';
                  }
                  uploadStatus = _this14.createUploadStatus(fileUpload);
                  _this14.uploadStatusList.appendChild(uploadStatus);
                  if (!(fileUpload.status !== 'error')) {
                    _context.next = 45;
                    break;
                  }
                  // Track uploads in progress.
                  if (fileService.uploadsInProgress === undefined) {
                    cssClass = 'uploads-in-progress';
                    submitComponent = _this14.root.element.querySelector('.formio-component-submit');
                    submitButton = submitComponent ? submitComponent.querySelector('button') : null;
                    array = new Array();
                    fileService.uploadsInProgress = {
                      array: array,
                      add: function add(item) {
                        if (submitButton && cssClass && array.length === 0) {
                          submitButton.classList.add(cssClass);
                        }
                        array.push(item);
                      },
                      remove: function remove(item) {
                        array.splice(array.indexOf(item), 1);
                        if (submitButton && cssClass && array.length === 0) {
                          submitButton.classList.remove(cssClass);
                        }
                      },
                      report: function report() {
                        var n = array.length;
                        var msg = "Uploads in progress: ".concat(n);
                        if (n > 0) {
                          var keys = array.map(function (x) {
                            return x.component.key;
                          });
                          msg += " (".concat(keys.join(', '), ")");
                        }
                        console.log(msg);
                      }
                    };
                  }
                  uploadsInProgress = fileService.uploadsInProgress;
                  uploadsInProgress.add(_this14);
                  //uploadsInProgress.report();
                  //
                  if (_this14.component.privateDownload) {
                    file.private = true;
                  }
                  _this14$component = _this14.component, storage = _this14$component.storage, url = _this14$component.url, options = _this14$component.options;
                  processedFile = null;
                  if (!_this14.root.options.fileProcessor) {
                    _context.next = 40;
                    break;
                  }
                  fileUpload.message = _this14.t('Starting file processing.');
                  _originalStatus = uploadStatus;
                  uploadStatus = _this14.createUploadStatus(fileUpload);
                  _this14.uploadStatusList.replaceChild(uploadStatus, _originalStatus);
                  _context.prev = 22;
                  _this14.fileProcessingLoader.style.display = 'block';
                  fileProcessorHandler = (0, _fileProcessor.default)(_this14.fileService, _this14.root.options.fileProcessor);
                  _context.next = 27;
                  return fileProcessorHandler(file, _this14.component.properties);
                case 27:
                  processedFile = _context.sent;
                  _context.next = 39;
                  break;
                case 30:
                  _context.prev = 30;
                  _context.t0 = _context["catch"](22);
                  fileUpload.status = 'error';
                  fileUpload.message = _this14.t('File processing has been failed.');
                  _originalStatus2 = uploadStatus;
                  uploadStatus = _this14.createUploadStatus(fileUpload);
                  _this14.uploadStatusList.replaceChild(uploadStatus, _originalStatus2);
                  _this14.fileProcessingLoader.style.display = 'none';
                  return _context.abrupt("return");
                case 39:
                  _this14.fileProcessingLoader.style.display = 'none';
                case 40:
                  fileUpload.message = _this14.t('Starting upload.');
                  originalStatus = uploadStatus;
                  uploadStatus = _this14.createUploadStatus(fileUpload);
                  _this14.uploadStatusList.replaceChild(uploadStatus, originalStatus);
                  fileService.uploadFile(storage, processedFile || file, fileName, dir, function (evt) {
                    fileUpload.status = 'progress';
                    fileUpload.progress = parseInt(100.0 * evt.loaded / evt.total);
                    delete fileUpload.message;
                    var originalStatus = uploadStatus;
                    uploadStatus = _this14.createUploadStatus(fileUpload);
                    _this14.uploadStatusList.replaceChild(uploadStatus, originalStatus);
                  }, url, options).then(function (fileInfo) {
                    _this14.removeChildFrom(uploadStatus, _this14.uploadStatusList);
                    // Default to first type.
                    if (_this14.hasTypes) {
                      fileInfo.fileType = _this14.component.fileTypes[0].value;
                    }
                    fileInfo.originalName = file.name;
                    var files = _this14.dataValue;
                    if (!files || !Array.isArray(files)) {
                      files = [];
                    }
                    files.push(fileInfo);
                    _this14.setValue(_this14.dataValue);
                    // Track uploads in progress.
                    uploadsInProgress.remove(_this14);
                    //uploadsInProgress.report();
                    //
                    _this14.triggerChange();
                  }).catch(function (response) {
                    fileUpload.status = 'error';
                    fileUpload.message = response;
                    delete fileUpload.progress;
                    var originalStatus = uploadStatus;
                    uploadStatus = _this14.createUploadStatus(fileUpload);
                    _this14.uploadStatusList.replaceChild(uploadStatus, originalStatus);
                  });
                case 45:
                case "end":
                  return _context.stop();
              }
            }, _callee, null, [[22, 30]]);
          }));
          return function (_x) {
            return _ref.apply(this, arguments);
          };
        }());
        /* eslint-enable max-statements */
      }
    }
  }, {
    key: "getFile",
    value: function getFile(fileInfo, event) {
      var fileService = this.fileService;
      if (!fileService) {
        return alert('File Service not provided');
      }
      if (this.component.privateDownload) {
        fileInfo.private = true;
      }
      fileService.downloadFile(fileInfo).then(function (file) {
        if (file) {
          if (file.storage === 'base64') {
            (0, _downloadjs.default)(file.url, file.originalName, file.type);
          } else {
            window.open(file.url, '_blank');
          }
        }
      }).catch(function (response) {
        // Is alert the best way to do this?
        // User is expecting an immediate notification due to attempting to download a file.
        alert(response);
      });
      event.preventDefault();
    }
  }, {
    key: "focus",
    value: function focus() {
      this.browseLink.focus();
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _Base.default.schema.apply(_Base.default, [{
        type: 'file',
        label: 'Upload',
        key: 'file',
        image: false,
        privateDownload: false,
        imageSize: '200',
        filePattern: '*',
        fileMinSize: '0KB',
        fileMaxSize: '1GB',
        uploadOnly: false,
        defaultOverlayWidth: 200,
        defaultOverlayHeight: 200
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'File',
        group: 'advanced',
        icon: 'fa fa-file',
        documentation: 'http://help.form.io/userguide/#file',
        weight: 100,
        schema: FileComponent.schema()
      };
    }
  }]);
  return FileComponent;
}(_Base.default);
exports.default = FileComponent;