"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  key: 'label',
  ignore: true
}, {
  'label': '',
  weight: 1,
  'columns': [{
    'components': [{
      type: 'number',
      label: 'Number of Rows',
      key: 'numRows',
      input: true,
      weight: 1,
      placeholder: 'Number of Rows',
      tooltip: 'Enter the number or rows that should be displayed by this table.'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'number',
      label: 'Number of Columns',
      key: 'numCols',
      input: true,
      weight: 2,
      placeholder: 'Number of Columns',
      tooltip: 'Enter the number or columns that should be displayed by this table.'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-17'
}, {
  'label': '',
  weight: 701,
  'columns': [{
    'components': [{
      type: 'checkbox',
      label: 'Striped',
      key: 'striped',
      tooltip: 'This will stripe the table if checked.',
      input: true,
      weight: 701
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'checkbox',
      label: 'Bordered',
      key: 'bordered',
      input: true,
      tooltip: 'This will border the table if checked.',
      weight: 702
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'checkbox',
      label: 'Condensed',
      key: 'condensed',
      input: true,
      tooltip: 'Condense the size of the table.',
      weight: 704
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-15'
}, {
  type: 'checkbox',
  label: 'Hover',
  key: 'hover',
  input: true,
  tooltip: 'Highlight a row on hover.',
  weight: 703
}, {
  key: 'placeholderRow',
  customClass: 'advanced-field'
}];
exports.default = _default;