"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _NestedComponent2 = _interopRequireDefault(require("../nested/NestedComponent"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var TableComponent = /*#__PURE__*/function (_NestedComponent) {
  _inherits(TableComponent, _NestedComponent);
  var _super = _createSuper(TableComponent);
  function TableComponent(component, options, data) {
    var _this;
    _classCallCheck(this, TableComponent);
    var originalRows = _lodash.default.cloneDeep(component.rows);
    _this = _super.call(this, component, options, data);
    if (!_lodash.default.isEqual(originalRows, _this.component.rows)) {
      _this.component.rows = originalRows;
    }
    return _this;
  }
  _createClass(TableComponent, [{
    key: "defaultSchema",
    get: function get() {
      return TableComponent.schema();
    }
  }, {
    key: "schema",
    get: function get() {
      var schema = _lodash.default.omit(_get(_getPrototypeOf(TableComponent.prototype), "schema", this), 'components');
      schema.rows = TableComponent.emptyTable(this.component.numRows, this.component.numCols);
      this.eachComponent(function (component) {
        var row = schema.rows[component.tableRow];
        var col = row && row[component.tableColumn];
        if (!row || !col || !component.components) {
          return;
        }
        var _iterator = _createForOfIteratorHelper(component.components),
          _step;
        try {
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            var childComponent = _step.value;
            schema.rows[component.tableRow][component.tableColumn].components.push(childComponent.schema);
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
      });
      return schema;
    }

    /**
     *
     * @param element
     * @param data
     */
  }, {
    key: "addComponents",
    value: function addComponents(element, data, options, state) {
      var _this2 = this;
      // Build the body.
      this.components = [];
      this.tbody = this.ce('tbody');
      var schemaReadOnly = this.schemaReadOnly;
      _lodash.default.each(this.component.rows, function (row, rowIndex) {
        var tr = _this2.ce('tr');
        _lodash.default.each(row, function (column, colIndex) {
          var td = _this2.ce('td', {
            id: "".concat(_this2.id, "-").concat(rowIndex, "-").concat(colIndex)
          });
          var tdComponent = new _NestedComponent2.default(column, _this2.options, data);
          tdComponent.element = td;
          tdComponent.element.component = tdComponent;
          tdComponent.parent = _this2;
          tdComponent.root = _this2.root || _this2;
          tdComponent.isBuilt = true;
          tdComponent.tableRow = rowIndex;
          tdComponent.tableColumn = colIndex;
          _this2.components.push(tdComponent);
          _lodash.default.each(column.components, function (comp) {
            var component = tdComponent.addComponent(comp, null, data, null, null, state);
            component.tableRow = rowIndex;
            component.tableColumn = colIndex;
          });
          if (_this2.options.builder) {
            if (!column.components || !column.components.length) {
              td.appendChild(_this2.ce('div', {
                id: "".concat(_this2.id, "-").concat(rowIndex, "-").concat(colIndex, "-placeholder"),
                class: 'alert',
                style: 'text-align:center; margin-bottom: 0px;',
                role: 'alert'
              }));
            }
            if (!schemaReadOnly) {
              _this2.root.addDragContainer(td, tdComponent, {
                onSave: function onSave(component) {
                  component.tableRow = rowIndex;
                  component.tableColumn = colIndex;
                }
              });
            }
          }
          tr.appendChild(td);
        });
        _this2.tbody.appendChild(tr);
      });
    }
  }, {
    key: "buildHeader",
    value: function buildHeader() {
      var _this3 = this;
      if (this.component.header && this.component.header.length) {
        var thead = this.ce('thead');
        var thr = this.ce('tr');
        _lodash.default.each(this.component.header, function (header) {
          var th = _this3.ce('th');
          th.appendChild(_this3.text(header));
          thr.appendChild(th);
        });
        thead.appendChild(thr);
        this.table.appendChild(thead);
      }
    }
  }, {
    key: "build",
    value: function build(state) {
      var _this4 = this;
      this.element = this.ce('div', {
        id: this.id,
        class: "".concat(this.className, "  table-responsive")
      });
      this.element.component = this;
      var tableClass = 'table ';
      _lodash.default.each(['striped', 'bordered', 'hover', 'condensed'], function (prop) {
        if (_this4.component[prop]) {
          tableClass += "table-".concat(prop, " ");
        }
      });
      this.createLabel(this.element);
      this.table = this.ce('table', {
        class: tableClass
      });
      this.buildHeader();
      this.addComponents(null, null, null, state);
      this.table.appendChild(this.tbody);
      this.element.appendChild(this.table);
      this.attachLogic();
    }
  }], [{
    key: "emptyTable",
    value: function emptyTable(numRows, numCols) {
      var rows = [];
      for (var i = 0; i < numRows; i++) {
        var cols = [];
        for (var j = 0; j < numCols; j++) {
          cols.push({
            components: [],
            type: 'column'
          });
        }
        rows.push(cols);
      }
      return rows;
    }
  }, {
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _NestedComponent2.default.schema.apply(_NestedComponent2.default, [{
        type: 'table',
        input: false,
        key: 'table',
        numRows: 3,
        numCols: 3,
        rows: TableComponent.emptyTable(3, 3),
        header: [],
        caption: '',
        striped: false,
        bordered: false,
        hover: false,
        condensed: false,
        persistent: false
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Table',
        group: 'layout',
        icon: 'fa fa-table',
        weight: 2,
        documentation: 'http://help.form.io/userguide/#table',
        schema: TableComponent.schema()
      };
    }
  }]);
  return TableComponent;
}(_NestedComponent2.default);
exports.default = TableComponent;