"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.array.slice.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.array.from.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.array.includes.js");
require("core-js/modules/es.string.includes.js");
require("core-js/modules/es.array.find.js");
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.number.is-safe-integer.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _NestedComponent2 = _interopRequireDefault(require("../nested/NestedComponent"));
var _Base = _interopRequireDefault(require("../base/Base"));
var _lodash = _interopRequireDefault(require("lodash"));
var _tooltip = _interopRequireDefault(require("tooltip.js"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var TabsComponent = /*#__PURE__*/function (_NestedComponent) {
  _inherits(TabsComponent, _NestedComponent);
  var _super = _createSuper(TabsComponent);
  function TabsComponent(component, options, data) {
    var _this;
    _classCallCheck(this, TabsComponent);
    _this = _super.call(this, component, options, data);
    _this.currentTab = 0;
    _this.validityTabs = [];
    return _this;
  }
  _createClass(TabsComponent, [{
    key: "defaultSchema",
    get: function get() {
      return TabsComponent.schema();
    }
  }, {
    key: "onAnyChanged",
    value: function onAnyChanged() {
      var tabLabels = {};
      var _iterator = _createForOfIteratorHelper(this.component.components),
        _step;
      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var tab = _step.value;
          if (tab.label && tab.label.includes('{{') && tab.label.includes('}}')) {
            if (!this.options.builder || !tab.workingLabel) {
              tabLabels[tab.key] = tab.label;
            }
          }
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
      var tabHeader = this.element.querySelector(':scope > ul.nav.nav-tabs');
      var tabLinks = tabHeader.querySelectorAll(':scope > li > a');
      for (var i = 0; i < tabLinks.length; i++) {
        var key = tabLinks[i].href.split('#')[1];
        if (key && tabLabels[key]) {
          if (this.options.builder) {
            tabLinks[i].replaceChild(this.generateLabelElement(tabLabels[key]), tabLinks[i].children[0]);
          } else {
            tabLinks[i].innerText = this.interpolate(tabLabels[key]);
          }
        }
      }
    }
  }, {
    key: "schema",
    get: function get() {
      var _this2 = this;
      var schema = _get(_getPrototypeOf(TabsComponent.prototype), "schema", this);
      var currentTab = null;
      if (this.currentTabComp) {
        var currentTabComponent = this.component.components.find(function (c) {
          return c.id && c.id === _this2.currentTabComp.id;
        });
        if (!currentTabComponent) {
          currentTabComponent = this.component.components.find(function (c) {
            return c.key && c.key === _this2.currentTabComp.key;
          });
        }
        if (currentTabComponent) {
          currentTab = this.component.components.indexOf(currentTabComponent);
        }
      }
      if (currentTab == null && Array.isArray(this.components) && this.components.length > 0) {
        var childComp = this.components[0];
        var index = null;
        for (var i = 0; i < this.component.components.length; i++) {
          // eslint-disable-next-line max-depth
          if (this.component.components[i].components) {
            var childComponent = this.component.components[i].components.find(function (c) {
              return c.id === childComp.id;
            });
            // eslint-disable-next-line max-depth
            if (childComponent) {
              index = i;
              break;
            }
          }
        }
        if (index != null) {
          currentTab = index;
        }
      }
      schema.components = this.component.components.map(function (tab, index) {
        if (index === currentTab) {
          tab.components = _this2.getComponents().map(function (component) {
            return component.schema;
          });
        }
        return tab;
      });
      if (Number.isSafeInteger(currentTab)) {
        this.currentTab = currentTab;
      }
      return schema;
    }
  }, {
    key: "build",
    value: function build(state, showLabel) {
      var _this3 = this;
      if (this.options.flatten) {
        this.element = _get(_getPrototypeOf(TabsComponent.prototype), "createElement", this).call(this);
        this.component.components.forEach(function (tab) {
          var body;
          var panel = _this3.ce('div', {
            id: _this3.id,
            class: 'mb-2 card border panel panel-default'
          }, [_this3.ce('div', {
            class: 'card-header bg-default panel-heading'
          }, _this3.ce('h4', {
            class: 'mb-0 card-title panel-title'
          }, tab.label)), body = _this3.ce('div', {
            class: 'card-body panel-body'
          })]);
          tab.components.forEach(function (component) {
            return _this3.addComponent(component, body, _this3.data, null, null, _this3.getComponentState(component, state));
          });
          _this3.element.appendChild(panel);
        });
      } else {
        return _get(_getPrototypeOf(TabsComponent.prototype), "build", this).call(this, state, showLabel);
      }
    }
  }, {
    key: "createElement",
    value: function createElement() {
      var _this4 = this;
      this.tabsBar = this.ce('ul', {
        class: 'nav nav-tabs nav-pill mb-4'
      });
      this.tabsContent = this.ce('div', {
        class: 'tab-content'
      });
      this.tabLinks = [];
      this.tabs = [];
      this.component.components.forEach(function (tab, index) {
        var _this4$options, _this4$options3;
        var tabLabel = tab.label;
        var anchor = [];
        if ((_this4$options = _this4.options) !== null && _this4$options !== void 0 && _this4$options.builder && tab.workingLabel) {
          anchor.push(_this4.generateLabelElement(tabLabel || '', tab.workingLabel));
        } else if (tabLabel) {
          if (tabLabel.includes('{{') && tabLabel.includes('}}')) {
            var _this4$options2;
            if ((_this4$options2 = _this4.options) !== null && _this4$options2 !== void 0 && _this4$options2.builder) {
              anchor.push(_this4.generateLabelElement(tabLabel));
            } else {
              tabLabel = _this4.interpolate(tabLabel);
            }
          }
          if (anchor.length === 0) {
            anchor.push(tabLabel);
          }
        }
        if ((_this4$options3 = _this4.options) !== null && _this4$options3 !== void 0 && _this4$options3.builder && tab.condition) {
          var hasDeterminant = _this4.ce('span', {
            class: "btn btn-xxs btn-default component-configuration-button component-configuration-button-determinant ".concat(tab.condition > 1 ? 'more-than-one' : '')
          }, 'D');
          _this4.root.addEventListener(hasDeterminant, 'click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            document.dispatchEvent(new CustomEvent('formioEditComponent', {
              detail: _this4,
              tab: 'tabTabs'
            }));
            _this4.root.editComponent(_this4, false, 'tabTabs');
          });
          new _tooltip.default(hasDeterminant, {
            trigger: 'hover',
            placement: 'top',
            container: hasDeterminant,
            title: _this4.t('Determinants'),
            popperOptions: {
              positionFixed: true
            }
          });
          anchor.push(hasDeterminant);
        }
        var tabLink = _this4.ce('a', {
          class: 'nav-link',
          href: "#".concat(tab.key)
        }, anchor);
        if (tab.hidden) {
          tabLink.classList.add('hide');
        }
        _this4.addEventListener(tabLink, 'click', function (event) {
          event.preventDefault();
          _this4.setTab(index);
        });
        var tabElement = _this4.ce('li', {
          class: 'nav-item',
          role: 'presentation'
        }, tabLink);
        tabElement.tabLink = tabLink;
        _this4.tabsBar.appendChild(tabElement);
        _this4.tabLinks.push(tabElement);
        var tabPanel = _this4.ce('div', {
          role: 'tabpanel',
          class: 'tab-pane',
          id: tab.key
        });
        _this4.tabsContent.appendChild(tabPanel);
        _this4.tabs.push(tabPanel);
      });
      if (this.element) {
        this.appendChild(this.element, [this.tabsBar, this.tabsContent]);
        this.element.className = this.className;
        return this.element;
      }
      this.element = this.ce('div', {
        id: this.id,
        class: this.className
      }, [this.tabsBar, this.tabsContent]);
      this.element.component = this;
      return this.element;
    }

    /**
     * Set the current tab.
     *
     * @param index
     */
  }, {
    key: "setTab",
    value: function setTab(index, state) {
      var _this5 = this;
      if (!this.tabs || !this.component.components || !this.component.components[this.currentTab] || this.currentTab >= this.tabs.length) {
        return;
      }
      this.currentTab = index;

      // Get the current tab.
      var tab = this.component.components[index];
      this.currentTabComp = tab;
      this.empty(this.tabs[index]);
      this.components.map(function (comp) {
        return comp.destroy();
      });
      this.components = [];
      if (this.tabLinks.length <= index) {
        return;
      }
      this.tabLinks.forEach(function (tabLink) {
        return _this5.removeClass(tabLink, 'active').removeClass(tabLink.tabLink, 'active');
      });
      this.tabs.forEach(function (tab) {
        return _this5.removeClass(tab, 'active');
      });
      this.addClass(this.tabLinks[index], 'active').addClass(this.tabLinks[index].tabLink, 'active').addClass(this.tabs[index], 'active');
      var components = this.hook('addComponents', tab.components, this);
      components.forEach(function (component) {
        return _this5.addComponent(component, _this5.tabs[index], _this5.data, null, null, state);
      });
      this.restoreValue();
      this.triggerChange();
      this.emit('render');
    }

    /**
     * Return all the components within all the tabs.
     */
  }, {
    key: "getAllComponents",
    value: function getAllComponents() {
      // If the validity tabs are set, then this usually means we are getting the components that have
      // triggered errors and need to iterate through these to display them.
      if (this.validityTabs && this.validityTabs.length) {
        var comps = this.validityTabs.reduce(function (components, component) {
          if (component && component.getAllComponents) {
            component = component.getAllComponents();
          }
          return components.concat(component);
        }, []);
        this.validityTabs = [];
        return comps;
      }
      return _get(_getPrototypeOf(TabsComponent.prototype), "getAllComponents", this).call(this);
    }

    /**
     * Checks the validity by checking all tabs validity.
     *
     * @param data
     * @param dirty
     */
  }, {
    key: "checkValidity",
    value: function checkValidity(data, dirty) {
      var _this6 = this;
      if (!dirty) {
        return _get(_getPrototypeOf(TabsComponent.prototype), "checkValidity", this).call(this, data, dirty);
      }
      this.components.forEach(function (component) {
        return component.checkValidity(data, dirty);
      });
      if (!this.checkCondition(null, data)) {
        this.setCustomValidity('');
        return true;
      }
      var isValid = _Base.default.prototype.checkValidity.call(this, data, dirty);
      if (!this.validityTabs || this.validityTabs.length !== this.component.components.length) {
        this.validityTabs = this.component.components.map(function (comp) {
          var tabComp = _lodash.default.clone(comp);
          tabComp.type = 'panel';
          tabComp.internal = true;
          return _this6.createComponent(tabComp);
        });
      }
      return this.validityTabs.reduce(function (check, component) {
        return component.checkValidity(data, dirty) && check;
      }, isValid);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      var state = _get(_getPrototypeOf(TabsComponent.prototype), "destroy", this).call(this) || {};
      state.currentTab = this.currentTab;
      if (this.validityTabs) {
        this.validityTabs.forEach(function (component) {
          component.destroy();
        });
      }
      return state;
    }

    /**
     * Make sure to include the tab on the component as it is added.
     *
     * @param component
     * @param element
     * @param data
     * @param before
     * @return {BaseComponent}
     */
  }, {
    key: "addComponent",
    value: function addComponent(component, element, data, before, noAdd, state) {
      component.tab = this.currentTab;
      return _get(_getPrototypeOf(TabsComponent.prototype), "addComponent", this).call(this, component, element, data, before, noAdd, state);
    }

    /**
     * Only add the components for the active tab.
     */
  }, {
    key: "addComponents",
    value: function addComponents(element, data, options, state) {
      var _ref = state && state.currentTab ? state : this,
        currentTab = _ref.currentTab;
      this.setTab(currentTab, state);
    }
  }, {
    key: "insertBefore",
    value: function insertBefore(compElement, before) {
      var _before$component, _before$component$ele, _before$component2, _before$component2$el;
      if (!before || !((_before$component = before.component) !== null && _before$component !== void 0 && (_before$component$ele = _before$component.element) !== null && _before$component$ele !== void 0 && _before$component$ele.parentElement)) {
        return _get(_getPrototypeOf(TabsComponent.prototype), "insertBefore", this).call(this, compElement, before);
      }
      (_before$component2 = before.component) === null || _before$component2 === void 0 ? void 0 : (_before$component2$el = _before$component2.element) === null || _before$component2$el === void 0 ? void 0 : _before$component2$el.parentElement.insertBefore(compElement, before.component.element);
    }
  }, {
    key: "attachLogic",
    value: function attachLogic() {
      _get(_getPrototypeOf(TabsComponent.prototype), "attachLogic", this).call(this);
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _NestedComponent2.default.schema.apply(_NestedComponent2.default, [{
        label: 'Tabs',
        type: 'tabs',
        input: false,
        key: 'tabs',
        persistent: false,
        components: [{
          label: 'Tab 1',
          key: 'tab0',
          components: []
        }]
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Tabs',
        group: 'layout',
        icon: 'fal fa-folder-tree',
        weight: 50,
        documentation: 'http://help.form.io/userguide/#tabs',
        schema: TabsComponent.schema()
      };
    }
  }]);
  return TabsComponent;
}(_NestedComponent2.default);
exports.default = TabsComponent;