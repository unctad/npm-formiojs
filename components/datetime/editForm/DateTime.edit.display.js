"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  type: 'select',
  input: true,
  key: 'timezone',
  label: 'Select Timezone',
  tooltip: 'Select the timezone you wish to display this Date',
  weight: 31,
  lazyLoad: true,
  defaultValue: '',
  valueProperty: 'name',
  dataSrc: 'url',
  data: {
    url: 'https://formio.github.io/formio.js/resources/timezones.json'
  },
  template: '<span>{{ item.label }}</span>',
  conditional: {
    json: {
      '===': [{
        var: 'data.displayInTimezone'
      }, 'location']
    }
  }
}, {
  weight: 701,
  'label': '',
  'customClass': 'advanced-field',
  'columns': [{
    'components': [{
      type: 'checkbox',
      input: true,
      key: 'useLocaleSettings',
      label: 'Use Locale Settings',
      tooltip: 'Use locale settings to display date and time.',
      weight: 51
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'checkbox',
      input: true,
      key: 'allowInput',
      label: 'Allow Manual Input',
      tooltip: 'Check this if you would like to allow the user to manually enter in the date.',
      weight: 51
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-8'
}, {
  'label': '',
  weight: 699,
  'columns': [{
    'components': [{
      type: 'textfield',
      input: true,
      key: 'format',
      label: 'Format',
      placeholder: 'Format',
      description: 'Use formats provided by <a href="https://github.com/angular-ui/bootstrap/tree/master/src/dateparser/docs#uibdateparsers-format-codes" target="_blank">DateParser Codes</a>',
      tooltip: 'The date format for saving the value of this field. You can use formats provided by <a href="https://github.com/angular-ui/bootstrap/tree/master/src/dateparser/docs#uibdateparsers-format-codes" target="_blank">DateParser Codes</a>',
      weight: 52
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      'customClass': 'advanced-field',
      type: 'select',
      input: true,
      key: 'displayInTimezone',
      label: 'Display in Timezone',
      tooltip: 'This will display the captured date time in the select timezone.',
      weight: 30,
      defaultValue: 'viewer',
      dataSrc: 'values',
      data: {
        values: [{
          label: 'of Viewer',
          value: 'viewer'
        }, {
          label: 'of Submission',
          value: 'submission'
        }, {
          label: 'of Location',
          value: 'location'
        }, {
          label: 'UTC',
          value: 'utc'
        }]
      }
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-7'
}, {
  key: 'column-1',
  ignore: true
}, {
  key: 'column-3',
  ignore: true
}, {
  key: 'customClass',
  ignore: true
}, {
  type: 'select',
  key: 'size',
  label: 'Field size',
  input: true,
  dataSrc: 'values',
  weight: 109,
  data: {
    values: [{
      label: 'Extra Small',
      value: 'xs'
    }, {
      label: 'Small',
      value: 'sm'
    }, {
      label: 'Medium',
      value: 'md'
    }, {
      label: 'Large',
      value: 'lg'
    }]
  }
}];
exports.default = _default;