"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
/* eslint-disable max-len */
var _default = [/* labels row */
{
  key: 'labelRow',
  type: 'columns',
  weight: 2,
  columns: [{
    type: 'column',
    components: [{
      weight: 0,
      type: 'textfield',
      input: true,
      key: 'label',
      label: 'Label',
      placeholder: 'Field Label',
      tooltip: 'The label for this field that will appear next to it.',
      validate: {
        required: true
      }
    }]
  }, {
    type: 'column',
    components: [{
      weight: 10,
      type: 'checkbox',
      label: 'Hide Label',
      tooltip: 'Hide the label of this component. This allows you to show the label in the form builder, but not when it is rendered.',
      key: 'hideLabel',
      input: true,
      logic: [{
        name: 'label logic',
        trigger: {
          type: 'javascript',
          javascript: 'result=instance.root.editComponent.inDataGrid;'
        },
        actions: [{
          name: 'set label',
          type: 'property',
          property: {
            label: 'Label',
            value: 'label',
            type: 'string'
          },
          text: 'Hide Column Label'
        }]
      }]
    }]
  }, {
    type: 'column',
    components: [{
      type: 'select',
      input: true,
      key: 'labelPosition',
      label: 'Label Position',
      customClass: 'advanced-field',
      tooltip: 'Position for the label for this field.',
      weight: 1,
      defaultValue: 'top',
      dataSrc: 'values',
      data: {
        values: [{
          label: 'Top',
          value: 'top'
        }, {
          label: 'Left (Left-aligned)',
          value: 'left-left'
        }, {
          label: 'Left (Right-aligned)',
          value: 'left-right'
        }, {
          label: 'Right (Left-aligned)',
          value: 'right-left'
        }, {
          label: 'Right (Right-aligned)',
          value: 'right-right'
        }, {
          label: 'Bottom',
          value: 'bottom'
        }]
      }
    }]
  }]
}, /* labels options row */
{
  weight: 2,
  'label': '',
  customClass: 'advanced-field',
  'columns': [{
    'components': [{
      type: 'number',
      input: true,
      key: 'labelWidth',
      label: 'Label Width',
      tooltip: 'The width of label on line in percentages.',
      clearOnHide: false,
      weight: 30,
      placeholder: '30',
      suffix: '%',
      validate: {
        min: 0,
        max: 100
      },
      conditional: {
        json: {
          and: [{
            '!==': [{
              var: 'data.labelPosition'
            }, 'top']
          }, {
            '!==': [{
              var: 'data.labelPosition'
            }, 'bottom']
          }]
        }
      }
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'number',
      input: true,
      key: 'labelMargin',
      label: 'Label Margin',
      tooltip: 'The width of label margin on line in percentages.',
      clearOnHide: false,
      weight: 30,
      placeholder: '3',
      suffix: '%',
      validate: {
        min: 0,
        max: 100
      },
      conditional: {
        json: {
          and: [{
            '!==': [{
              var: 'data.labelPosition'
            }, 'top']
          }, {
            '!==': [{
              var: 'data.labelPosition'
            }, 'bottom']
          }]
        }
      }
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'labelOptionsRow'
}, /* placeholder and tooltip */
{
  key: 'placeholderRow',
  type: 'columns',
  weight: 3,
  columns: [{
    type: 'column',
    components: [{
      weight: 100,
      type: 'textfield',
      input: true,
      key: 'placeholder',
      label: 'Placeholder',
      placeholder: 'Placeholder',
      tooltip: 'The placeholder text that will appear when this field is empty.'
    }]
  }, {
    type: 'column',
    components: [{
      weight: 300,
      customClass: 'advanced-field',
      type: 'textarea',
      input: true,
      key: 'tooltip',
      label: 'Tooltip',
      rows: 1,
      placeholder: 'To add a tooltip to this field, enter text here.',
      tooltip: 'Adds a tooltip to the side of this field.'
    }]
  }]
}, {
  weight: 4,
  type: 'textarea',
  input: true,
  customClass: 'advanced-field',
  key: 'description',
  label: 'Description',
  placeholder: 'Description for this field.',
  tooltip: 'The description is text that will appear below the input field.'
}, {
  weight: 500,
  type: 'textfield',
  input: true,
  key: 'customClass',
  label: 'Custom CSS Class',
  placeholder: 'Custom CSS Class',
  customClass: 'advanced-field',
  tooltip: 'Custom CSS class to add to this component.'
}, {
  weight: 699,
  'label': '',
  customClass: 'advanced-field',
  'columns': [{
    'components': [{
      weight: 400,
      type: 'textfield',
      input: true,
      key: 'errorLabel',
      label: 'Error Label',
      placeholder: 'Error Label',
      customClass: 'advanced-field',
      tooltip: 'The label for this field when an error occurs.'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 600,
      type: 'textfield',
      input: true,
      key: 'tabindex',
      label: 'Tab Index',
      placeholder: 'Tab Index',
      customClass: 'advanced-field',
      tooltip: 'Sets the tabindex attribute of this component to override the tab order of the form. See the <a href=\\\'https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex\\\'>MDN documentation</a> on tabindex for more information.'
    }],
    'width': 6,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-3'
}, {
  weight: 700,
  type: 'radio',
  label: 'Persistent',
  tooltip: 'A persistent field will be stored in database when the form is submitted.',
  key: 'persistent',
  customClass: 'advanced-field',
  input: true,
  inline: true,
  values: [{
    label: 'None',
    value: false
  }, {
    label: 'Server',
    value: true
  }, {
    label: 'Client',
    value: 'client-only'
  }]
}, /* checkboxes row */
{
  key: 'checkboxesRow',
  type: 'columns',
  weight: 702,
  columns: [{
    type: 'column',
    components: [{
      weight: 3,
      type: 'checkbox',
      label: 'Disabled',
      tooltip: 'Disable the form input.',
      key: 'disabled',
      input: true
    }]
  }, {
    type: 'column',
    components: [{
      weight: 4,
      type: 'checkbox',
      label: 'Hidden',
      tooltip: 'A hidden field is still a part of the form, but is hidden from view.',
      key: 'hidden',
      input: true
    }]
  }, {
    type: 'column',
    components: [{
      weight: 5,
      type: 'checkbox',
      label: 'Protected',
      tooltip: 'A protected field will not be returned when queried via API.',
      key: 'protected',
      input: true
    }]
  }, {
    type: 'column',
    components: [{
      weight: 6,
      type: 'checkbox',
      label: 'Initial Focus',
      tooltip: 'Make this field the initially focused element on this form.',
      key: 'autofocus',
      input: true
    }]
  }]
}, {
  'label': '',
  customClass: 'advanced-field',
  'columns': [{
    'components': [{
      weight: 805,
      type: 'checkbox',
      label: 'Allow Reordering',
      tooltip: 'Allows changing order of multiple values using drag and drop',
      key: 'reorder',
      customClass: 'advanced-field',
      customConditional: function customConditional(context) {
        return context.data.type === 'datagrid' || context.data.type === 'editgrid';
      },
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 900,
      type: 'checkbox',
      label: 'Clear Value When Hidden',
      key: 'clearOnHide',
      defaultValue: true,
      clearOnHide: true,
      customClass: 'advanced-field',
      tooltip: 'When a field is hidden, clear the value.',
      //customConditional: 'show = !data.hidden;',
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 1300,
      type: 'checkbox',
      label: 'Hide Input',
      customClass: 'advanced-field',
      tooltip: 'Hide the input in the browser. This does not encrypt on the server. Do not use for passwords.',
      key: 'mask',
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 1500,
      type: 'checkbox',
      label: 'Table View',
      tooltip: 'Shows this value within the table view of the submissions.',
      key: 'tableView',
      customClass: 'advanced-field',
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 1550,
      type: 'checkbox',
      label: 'Always enabled',
      tooltip: 'Make this field always enabled, even if the form is disabled',
      key: 'alwaysEnabled',
      customClass: 'advanced-field',
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-10'
}, {
  weight: 1310,
  type: 'checkbox',
  label: 'Show Label in DataGrid',
  tooltip: 'Show the label when in a Datagrid.',
  key: 'dataGridLabel',
  clearOnHide: false,
  input: true,
  customConditional: function customConditional(context) {
    return context.instance.root.editComponent.inDataGrid;
  }
}, {
  type: 'checkbox',
  label: 'Searchable',
  tooltip: 'Search the field when in a Datagrid or Editgrid.',
  key: 'searchable',
  input: true,
  customConditional: function customConditional(context) {
    return context.data.input;
  }
}];
/* eslint-enable max-len */
exports.default = _default;