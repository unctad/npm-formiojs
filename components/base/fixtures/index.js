"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "comp1", {
  enumerable: true,
  get: function get() {
    return _comp.default;
  }
});
Object.defineProperty(exports, "comp2", {
  enumerable: true,
  get: function get() {
    return _comp2.default;
  }
});
Object.defineProperty(exports, "multipleWithDraggableRows", {
  enumerable: true,
  get: function get() {
    return _multipleWithDraggableRows.default;
  }
});
var _comp = _interopRequireDefault(require("./comp1"));
var _comp2 = _interopRequireDefault(require("./comp2"));
var _multipleWithDraggableRows = _interopRequireDefault(require("./multipleWithDraggableRows"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }