"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.object.get-own-property-descriptors.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.array.map.js");
require("core-js/modules/es.object.keys.js");
require("core-js/modules/es.string.trim.js");
require("core-js/modules/es.regexp.exec.js");
require("core-js/modules/es.string.replace.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.join.js");
require("core-js/modules/es.number.max-safe-integer.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _TextField = _interopRequireDefault(require("../textfield/TextField"));
var _Formio = _interopRequireDefault(require("../../Formio"));
var _lodash = _interopRequireDefault(require("lodash"));
var _utils = require("../../utils/utils");
var _nativePromiseOnly = _interopRequireDefault(require("native-promise-only"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); } /* global ace, Quill */
var TextAreaComponent = /*#__PURE__*/function (_TextFieldComponent) {
  _inherits(TextAreaComponent, _TextFieldComponent);
  var _super = _createSuper(TextAreaComponent);
  function TextAreaComponent(component, options, data) {
    var _this2;
    _classCallCheck(this, TextAreaComponent);
    _this2 = _super.call(this, component, options, data);
    _this2.wysiwygRendered = false;
    _this2.editorReady = new _nativePromiseOnly.default(function (resolve) {
      _this2.editorReadyResolve = resolve;
    });
    // Never submit on enter for text areas.
    _this2.options.submitOnEnter = false;
    return _this2;
  }
  _createClass(TextAreaComponent, [{
    key: "defaultSchema",
    get: function get() {
      return TextAreaComponent.schema();
    }
  }, {
    key: "show",
    value: function show(_show, noClear) {
      if (_show && !this.wysiwygRendered) {
        this.enableWysiwyg();
        this.setWysiwygValue(this.dataValue);
        this.wysiwygRendered = true;
      } else if (!_show && this.wysiwygRendered) {
        this.destroyWysiwyg();
        this.wysiwygRendered = false;
      }
      return _get(_getPrototypeOf(TextAreaComponent.prototype), "show", this).call(this, _show, noClear);
    }
  }, {
    key: "setupValueElement",
    value: function setupValueElement(element) {
      var value = this.getValue();
      value = this.isEmpty(value) ? this.defaultViewOnlyValue : this.getView(value);
      if (this.component.wysiwyg) {
        value = this.interpolate(value);
      }
      element.innerHTML = value;
    }
  }, {
    key: "acePlaceholder",
    value: function acePlaceholder() {
      if (!this.component.placeholder || !this.editor) {
        return;
      }
      var shouldShow = !this.editor.session.getValue().length;
      var node = this.editor.renderer.emptyMessageNode;
      if (!shouldShow && node) {
        this.editor.renderer.scroller.removeChild(this.editor.renderer.emptyMessageNode);
        this.editor.renderer.emptyMessageNode = null;
      } else if (shouldShow && !node) {
        node = this.editor.renderer.emptyMessageNode = this.ce('div');
        node.textContent = this.t(this.component.placeholder);
        node.className = 'ace_invisible ace_emptyMessage';
        node.style.padding = '0 9px';
        this.editor.renderer.scroller.appendChild(node);
      }
    }
  }, {
    key: "isPlain",
    get: function get() {
      return !this.component.wysiwyg && !this.component.editor;
    }
  }, {
    key: "htmlView",
    get: function get() {
      return this.options.readOnly && this.component.wysiwyg;
    }
  }, {
    key: "autoExpand",
    get: function get() {
      return this.component.autoExpand;
    }

    /**
     * Updates the editor value.
     *
     * @param newValue
     */
  }, {
    key: "updateEditorValue",
    value: function updateEditorValue(newValue) {
      newValue = this.getConvertedValue(this.removeBlanks(newValue));
      if (newValue !== this.dataValue && (!_lodash.default.isEmpty(newValue) || !_lodash.default.isEmpty(this.dataValue))) {
        this.updateValue({
          modified: !this.autoModified
        }, newValue);
      }
      this.autoModified = false;
    }

    /* eslint-disable max-statements */
  }, {
    key: "createInput",
    value: function createInput(container) {
      if (this.options.readOnly) {
        this.input = this.ce('div', {
          class: 'well'
        });
        container.appendChild(this.input);
        return this.input;
      } else if (this.isPlain) {
        return _get(_getPrototypeOf(TextAreaComponent.prototype), "createInput", this).call(this, container);
      }
      if (!this.errorContainer) {
        this.errorContainer = container;
      }
      if (this.htmlView) {
        this.input = this.ce('div', {
          class: 'well'
        });
        container.appendChild(this.input);
        return this.input;
      }

      // Add the input.
      this.input = this.ce('div', {
        class: 'formio-wysiwyg-editor'
      });
      container.appendChild(this.input);
      this.addCounter(container);
      return this.input;
    }
    /* eslint-enable max-statements */
  }, {
    key: "enableWysiwyg",
    value: function enableWysiwyg() {
      var _this3 = this;
      if (this.isPlain || this.options.readOnly || this.options.htmlView) {
        if (this.autoExpand) {
          this.element.childNodes.forEach(function (element) {
            if (element.nodeName === 'TEXTAREA') {
              _this3.addAutoExpanding(element);
            }
          });
        }
        return;
      }
      if (this.component.editor === 'ace') {
        _Formio.default.requireLibrary('ace', 'ace', 'https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.1/ace.js', true).then(function () {
          var mode = _this3.component.as || 'javascript';
          _this3.editor = ace.edit(_this3.input);
          _this3.editor.on('change', function () {
            return _this3.updateEditorValue(_this3.editor.getValue());
          });
          _this3.editor.getSession().setTabSize(2);
          _this3.editor.getSession().setMode("ace/mode/".concat(mode));
          _this3.editor.on('input', function () {
            return _this3.acePlaceholder();
          });
          setTimeout(function () {
            return _this3.acePlaceholder();
          }, 100);
          _this3.editorReadyResolve(_this3.editor);
          return _this3.editor;
        });
        return this.input;
      }
      if (this.component.editor === 'ckeditor') {
        var ckConfig = this.options.ckConfig || {};
        var settings = _objectSpread(_objectSpread({}, ckConfig), this.component.wysiwyg || {});
        settings.rows = this.component.rows;
        this.addCKE(this.input, settings, function (newValue) {
          return _this3.updateEditorValue(newValue);
        }).then(function (editor) {
          _this3.editor = editor;
          if (_this3.options.readOnly || _this3.component.disabled) {
            _this3.editor.isReadOnly = true;
          }
          _this3.editorReadyResolve(_this3.editor);
          return editor;
        });
        return this.input;
      }

      // Normalize the configurations.
      if (this.component.wysiwyg && (this.component.wysiwyg.hasOwnProperty('toolbarGroups') || this.component.wysiwyg.hasOwnProperty('toolbar'))) {
        console.warn('The WYSIWYG settings are configured for CKEditor. For this renderer, you will need to use configurations for the Quill Editor. See https://quilljs.com/docs/configuration for more information.');
        this.component.wysiwyg = this.wysiwygDefault;
        this.emit('componentEdit', this);
      }
      if (!this.component.wysiwyg || typeof this.component.wysiwyg === 'boolean') {
        this.component.wysiwyg = this.wysiwygDefault;
        this.emit('componentEdit', this);
      }

      // Add the quill editor.
      this.addQuill(this.input, this.component.wysiwyg, function () {
        return _this3.updateEditorValue(_this3.quill.root.innerHTML);
      }).then(function (quill) {
        if (_this3.component.isUploadEnabled) {
          var _this = _this3;
          quill.getModule('toolbar').addHandler('image', function () {
            //we need initial 'this' because quill calls this method with its own context and we need some inner quill methods exposed in it
            //we also need current component instance as we use some fields and methods from it as well
            _this.imageHandler.call(_this, this);
          });
        }
        quill.root.spellcheck = _this3.component.spellcheck;
        if (_this3.options.readOnly || _this3.component.disabled) {
          quill.disable();
        }
        _this3.editorReadyResolve(quill);
        return quill;
      }).catch(function (err) {
        return console.warn(err);
      });
    }
  }, {
    key: "destroyWysiwyg",
    value: function destroyWysiwyg() {
      var _this4 = this;
      if (this.editor) {
        this.editorReady = new _nativePromiseOnly.default(function (resolve) {
          _this4.editorReadyResolve = resolve;
        });
        this.editor.destroy();
      }
    }
  }, {
    key: "imageHandler",
    value: function imageHandler(quillInstance) {
      var _this5 = this;
      var fileInput = quillInstance.container.querySelector('input.ql-image[type=file]');
      if (fileInput == null) {
        fileInput = document.createElement('input');
        fileInput.setAttribute('type', 'file');
        fileInput.setAttribute('accept', 'image/*');
        fileInput.classList.add('ql-image');
        fileInput.addEventListener('change', function () {
          var files = fileInput.files;
          var range = quillInstance.quill.getSelection(true);
          if (!files || !files.length) {
            console.warn('No files selected');
            return;
          }
          quillInstance.quill.enable(false);
          var _this5$component = _this5.component,
            uploadStorage = _this5$component.uploadStorage,
            uploadUrl = _this5$component.uploadUrl,
            uploadOptions = _this5$component.uploadOptions,
            uploadDir = _this5$component.uploadDir;
          var requestData;
          _this5.root.formio.uploadFile(uploadStorage, files[0], (0, _utils.uniqueName)(files[0].name), uploadDir || '',
          //should pass empty string if undefined
          null, uploadUrl, uploadOptions).then(function (result) {
            requestData = result;
            return _this5.root.formio.downloadFile(result);
          }).then(function (result) {
            quillInstance.quill.enable(true);
            var Delta = Quill.import('delta');
            quillInstance.quill.updateContents(new Delta().retain(range.index).delete(range.length).insert({
              image: result.url
            }, {
              alt: JSON.stringify(requestData)
            }), Quill.sources.USER);
            fileInput.value = '';
          }).catch(function (error) {
            console.warn('Quill image upload failed');
            console.warn(error);
            quillInstance.quill.enable(true);
          });
        });
        quillInstance.container.appendChild(fileInput);
      }
      fileInput.click();
    }
  }, {
    key: "setWysiwygValue",
    value: function setWysiwygValue(value, skipSetting, flags) {
      var _this6 = this;
      if (this.isPlain || this.options.readOnly || this.options.htmlView) {
        return;
      }
      if (this.editorReady) {
        this.editorReady.then(function (editor) {
          // This change solves an issue where the change event was erroneously firing.
          // This also negates the need for the "value" parameter, but I am keeping that there for reverse compatibility,
          // and ensure any extended classes do not break.
          value = _this6.dataValue;
          _this6.autoModified = flags && flags.autoModified || (_lodash.default.isNil(_this6.autoModified) ? true : _this6.autoModified);
          if (!skipSetting || flags && flags.autoModified) {
            if (_this6.component.editor === 'ace') {
              editor.setValue(_this6.setConvertedValue(value));
            } else if (_this6.component.editor === 'ckeditor') {
              editor.data.set(_this6.setConvertedValue(value));
            } else {
              if (_this6.component.isUploadEnabled) {
                _this6.setAsyncConvertedValue(value).then(function (result) {
                  editor.setContents(editor.clipboard.convert(result));
                });
              } else {
                editor.setContents(editor.clipboard.convert(_this6.setConvertedValue(value)));
              }
            }
          }
        });
      }
    }
  }, {
    key: "setConvertedValue",
    value: function setConvertedValue(value) {
      if (this.component.as && this.component.as === 'json' && value) {
        try {
          value = JSON.stringify(value, null, 2);
        } catch (err) {
          console.warn(err);
        }
      }
      if (!_lodash.default.isString(value)) {
        value = '';
      }
      return value;
    }
  }, {
    key: "setAsyncConvertedValue",
    value: function setAsyncConvertedValue(value) {
      if (this.component.as && this.component.as === 'json' && value) {
        try {
          value = JSON.stringify(value, null, 2);
        } catch (err) {
          console.warn(err);
        }
      }
      if (!_lodash.default.isString(value)) {
        value = '';
      }
      var htmlDoc = new DOMParser().parseFromString(value, 'text/html');
      var images = htmlDoc.getElementsByTagName('img');
      if (images.length) {
        return this.setImagesUrl(images).then(function () {
          value = htmlDoc.getElementsByTagName('body')[0].firstElementChild;
          return new XMLSerializer().serializeToString(value);
        });
      } else {
        return _nativePromiseOnly.default.resolve(value);
      }
    }
  }, {
    key: "setImagesUrl",
    value: function setImagesUrl(images) {
      var _this7 = this;
      return _nativePromiseOnly.default.all(_lodash.default.map(images, function (image) {
        var requestData;
        try {
          requestData = JSON.parse(image.getAttribute('alt'));
        } catch (error) {
          console.warn(error);
        }
        if (!requestData) {
          return _nativePromiseOnly.default.resolve();
        }
        return _this7.root.formio.downloadFile(requestData).then(function (result) {
          image.setAttribute('src', result.url);
        });
      }));
    }
  }, {
    key: "addAutoExpanding",
    value: function addAutoExpanding(textarea) {
      var heightOffset = null;
      var previousHeight = null;
      var changeOverflow = function changeOverflow(value) {
        var width = textarea.style.width;
        textarea.style.width = '0px';
        textarea.offsetWidth;
        textarea.style.width = width;
        textarea.style.overflowY = value;
      };
      var preventParentScroll = function preventParentScroll(element, changeSize) {
        var nodeScrolls = [];
        while (element && element.parentNode && element.parentNode instanceof Element) {
          if (element.parentNode.scrollTop) {
            nodeScrolls.push({
              node: element.parentNode,
              scrollTop: element.parentNode.scrollTop
            });
          }
          element = element.parentNode;
        }
        changeSize();
        nodeScrolls.forEach(function (nodeScroll) {
          nodeScroll.node.scrollTop = nodeScroll.scrollTop;
        });
      };
      var resize = function resize() {
        if (textarea.scrollHeight === 0) {
          return;
        }
        preventParentScroll(textarea, function () {
          textarea.style.height = '';
          textarea.style.height = "".concat(textarea.scrollHeight + heightOffset, "px");
        });
      };
      var update = _lodash.default.debounce(function () {
        resize();
        var styleHeight = Math.round(parseFloat(textarea.style.height));
        var computed = window.getComputedStyle(textarea, null);
        var currentHeight = textarea.offsetHeight;
        if (currentHeight < styleHeight && computed.overflowY === 'hidden') {
          changeOverflow('scroll');
        } else if (computed.overflowY !== 'hidden') {
          changeOverflow('hidden');
        }
        resize();
        currentHeight = textarea.offsetHeight;
        if (previousHeight !== currentHeight) {
          previousHeight = currentHeight;
          update();
        }
      }, 200);
      var computedStyle = window.getComputedStyle(textarea, null);
      textarea.style.resize = 'none';
      heightOffset = parseFloat(computedStyle.borderTopWidth) + parseFloat(computedStyle.borderBottomWidth) || 0;
      if (window) {
        this.addEventListener(window, 'resize', update);
      }
      this.addEventListener(textarea, 'input', update);
      this.on('initialized', update);
      this.updateSize = update;
      update();
    }
  }, {
    key: "removeBlanks",
    value: function removeBlanks(value) {
      if (!value) {
        return value;
      }
      var removeBlanks = function removeBlanks(input) {
        if (typeof input !== 'string') {
          return input;
        }
        return input.replace(/<p>&nbsp;<\/p>|<p><br><\/p>|<p><br>&nbsp;<\/p>/g, '').trim();
      };
      if (Array.isArray(value)) {
        value.forEach(function (input, index) {
          value[index] = removeBlanks(input);
        });
      } else {
        value = removeBlanks(value);
      }
      return value;
    }
  }, {
    key: "onChange",
    value: function onChange() {
      var _get2;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      (_get2 = _get(_getPrototypeOf(TextAreaComponent.prototype), "onChange", this)).call.apply(_get2, [this].concat(args));
      if (this.updateSize) {
        this.updateSize();
      }
    }
  }, {
    key: "hasChanged",
    value: function hasChanged(newValue, oldValue) {
      return _get(_getPrototypeOf(TextAreaComponent.prototype), "hasChanged", this).call(this, this.removeBlanks(newValue), this.removeBlanks(oldValue));
    }
  }, {
    key: "isEmpty",
    value: function isEmpty(value) {
      return _get(_getPrototypeOf(TextAreaComponent.prototype), "isEmpty", this).call(this, this.removeBlanks(value));
    }
  }, {
    key: "defaultValue",
    get: function get() {
      var defaultValue = _get(_getPrototypeOf(TextAreaComponent.prototype), "defaultValue", this);
      if (this.component.wysiwyg && !defaultValue) {
        defaultValue = '<p><br></p>';
      }
      return defaultValue;
    }
  }, {
    key: "setValue",
    value: function setValue(value, flags) {
      var _this8 = this;
      var skipSetting = _lodash.default.isEqual(value, this.getValue());
      value = value || '';
      if (this.options.readOnly || this.htmlView) {
        // For readOnly, just view the contents.
        if (this.input) {
          if (Array.isArray(value)) {
            value = value.join('<br/><br/>');
          }
          this.input.innerHTML = this.interpolate(value);
        }
        this.dataValue = value;
        return;
      } else if (this.isPlain) {
        value = Array.isArray(value) ? value.map(function (val) {
          return _this8.setConvertedValue(val);
        }) : this.setConvertedValue(value);
        return _get(_getPrototypeOf(TextAreaComponent.prototype), "setValue", this).call(this, value, flags);
      }
      this.setWysiwygValue(value, skipSetting, flags);
      return this.updateValue(flags);
    }
  }, {
    key: "getConvertedValue",
    value: function getConvertedValue(value) {
      if (this.component.as && this.component.as === 'json' && value) {
        try {
          value = JSON.parse(value);
        } catch (err) {
          console.warn(err);
        }
      }
      return value;
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (this.editorReady) {
        this.editorReady.then(function (editor) {
          return editor.destroy();
        });
      }
      if (this.updateSize) {
        this.removeEventListener(window, 'resize', this.updateSize);
      }
      return _get(_getPrototypeOf(TextAreaComponent.prototype), "destroy", this).call(this);
    }
  }, {
    key: "getValue",
    value: function getValue() {
      if (this.viewOnly || this.htmlView || this.options.readOnly) {
        return this.dataValue;
      }
      if (this.isPlain) {
        return this.getConvertedValue(_get(_getPrototypeOf(TextAreaComponent.prototype), "getValue", this).call(this));
      }
      return this.dataValue;
    }
  }, {
    key: "elementInfo",
    value: function elementInfo() {
      var info = _get(_getPrototypeOf(TextAreaComponent.prototype), "elementInfo", this).call(this);
      info.type = 'textarea';
      if (this.component.rows) {
        info.attr.rows = this.component.rows;
      }
      return info;
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len2 = arguments.length, extend = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        extend[_key2] = arguments[_key2];
      }
      return _TextField.default.schema.apply(_TextField.default, [{
        type: 'textarea',
        label: 'Text Area',
        key: 'textArea',
        rows: 3,
        wysiwyg: false,
        editor: '',
        validate: {
          minLength: 0,
          maxLength: Number.MAX_SAFE_INTEGER
        }
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Text Area',
        group: 'basic',
        icon: 'fa fa-font',
        documentation: 'http://help.form.io/userguide/#textarea',
        weight: 40,
        schema: TextAreaComponent.schema()
      };
    }
  }]);
  return TextAreaComponent;
}(_TextField.default);
exports.default = TextAreaComponent;