"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _builder = _interopRequireDefault(require("../../../utils/builder"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var _default = [{
  key: 'labelRow',
  ignore: true
}, {
  key: 'labelRowForCheckbox',
  type: 'columns',
  weight: 2,
  columns: [{
    type: 'column',
    components: [{
      weight: 0,
      type: 'textfield',
      input: true,
      key: 'label',
      label: 'Label',
      placeholder: 'Field Label',
      tooltip: 'The label for this field that will appear next to it.',
      validate: {
        required: true
      }
    }]
  }, {
    type: 'column',
    components: [{
      weight: 10,
      type: 'checkbox',
      label: 'Hide Label',
      tooltip: 'Hide the label of this component. This allows you to show the label in the form builder, but not when it is rendered.',
      key: 'hideLabel',
      input: true,
      logic: [{
        name: 'label logic',
        trigger: {
          type: 'javascript',
          javascript: 'result=instance.root.editComponent.inDataGrid;'
        },
        actions: [{
          name: 'set label',
          type: 'property',
          property: {
            label: 'Label',
            value: 'label',
            type: 'string'
          },
          text: 'Hide Column Label'
        }]
      }]
    }]
  }]
}, {
  'label': '',
  weight: 2,
  customClass: 'advanced-field',
  'columns': [{
    'components': [{
      type: 'select',
      input: true,
      key: 'inputType',
      label: 'Input Type',
      tooltip: 'This is the input type used for this checkbox.',
      dataSrc: 'values',
      weight: 410,
      data: {
        values: [{
          label: 'Checkbox',
          value: 'checkbox'
        }, {
          label: 'Radio',
          value: 'radio'
        }]
      }
    }, {
      type: 'number',
      input: true,
      key: 'labelWidth',
      label: 'Label Width',
      tooltip: 'The width of label on line in percentages.',
      clearOnHide: false,
      weight: 30,
      placeholder: '30',
      suffix: '%',
      validate: {
        min: 0,
        max: 100
      },
      conditional: {
        json: {
          and: [{
            '!==': [{
              var: 'data.labelPosition'
            }, 'top']
          }, {
            '!==': [{
              var: 'data.labelPosition'
            }, 'bottom']
          }]
        }
      }
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-19',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      type: 'select',
      input: true,
      label: 'Label Position',
      key: 'labelPosition',
      tooltip: 'Position for the label for this field.',
      defaultValue: 'right',
      dataSrc: 'values',
      weight: 20,
      data: {
        values: [{
          label: 'Top',
          value: 'top'
        }, {
          label: 'Left',
          value: 'left'
        }, {
          label: 'Right',
          value: 'right'
        }, {
          label: 'Bottom',
          value: 'bottom'
        }]
      }
    }, {
      type: 'number',
      input: true,
      key: 'labelMargin',
      label: 'Label Margin',
      tooltip: 'The width of label margin on line in percentages.',
      clearOnHide: false,
      weight: 30,
      placeholder: '3',
      suffix: '%',
      validate: {
        min: 0,
        max: 100
      },
      conditional: {
        json: {
          and: [{
            '!==': [{
              var: 'data.labelPosition'
            }, 'top']
          }, {
            '!==': [{
              var: 'data.labelPosition'
            }, 'bottom']
          }]
        }
      }
    }],
    'width': 4,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-19',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-12'
}, {
  type: 'textfield',
  input: true,
  key: 'name',
  label: 'Radio Key',
  tooltip: 'The key used to trigger the radio button toggle.',
  weight: 420,
  conditional: {
    json: {
      '===': [{
        var: 'data.inputType'
      }, 'radio']
    }
  }
}, {
  type: 'textfield',
  input: true,
  label: 'Radio Value',
  key: 'value',
  tooltip: 'The value used with this radio button.',
  weight: 430,
  conditional: {
    json: {
      '===': [{
        var: 'data.inputType'
      }, 'radio']
    }
  }
}, {
  key: 'column-1',
  ignore: true
}, {
  key: 'column-3',
  ignore: true
}, {
  'label': '',
  customClass: 'advanced-field',
  'columns': [{
    'components': [{
      weight: 900,
      type: 'checkbox',
      label: 'Clear Value When Hidden',
      key: 'clearOnHide',
      defaultValue: true,
      clearOnHide: true,
      customClass: 'advanced-field',
      tooltip: 'When a field is hidden, clear the value.',
      // customConditional: 'show = !data.hidden;',
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }, {
    'components': [{
      weight: 1550,
      type: 'checkbox',
      label: 'Always enabled',
      tooltip: 'Make this field always enabled, even if the form is disabled',
      key: 'alwaysEnabled',
      customClass: 'advanced-field',
      input: true
    }],
    'width': 3,
    'offset': 0,
    'push': 0,
    'pull': 0,
    'type': 'column',
    'input': false,
    'hideOnChildrenHidden': false,
    'key': 'column-1',
    'tableView': true,
    'label': 'Column'
  }],
  'mask': false,
  'tableView': false,
  'alwaysEnabled': false,
  'type': 'columns',
  'input': false,
  'key': 'column-10-1'
}, {
  key: 'labelOptionsRow',
  hidden: true,
  ignore: true
}, {
  key: 'column-10',
  ignore: true
}];
exports.default = _default;