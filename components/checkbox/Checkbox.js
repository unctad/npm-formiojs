"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
require("core-js/modules/es.reflect.construct.js");
require("core-js/modules/es.reflect.get.js");
require("core-js/modules/es.object.get-own-property-descriptor.js");
require("core-js/modules/es.symbol.to-primitive.js");
require("core-js/modules/es.date.to-primitive.js");
require("core-js/modules/es.symbol.js");
require("core-js/modules/es.symbol.description.js");
require("core-js/modules/es.number.constructor.js");
require("core-js/modules/es.reflect.set.js");
require("core-js/modules/es.symbol.iterator.js");
require("core-js/modules/es.array.iterator.js");
require("core-js/modules/es.string.iterator.js");
require("core-js/modules/web.dom-collections.iterator.js");
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
require("core-js/modules/es.object.to-string.js");
require("core-js/modules/es.regexp.to-string.js");
require("core-js/modules/es.array.filter.js");
require("core-js/modules/es.function.name.js");
require("core-js/modules/es.array.concat.js");
require("core-js/modules/es.array.includes.js");
require("core-js/modules/web.dom-collections.for-each.js");
require("core-js/modules/es.object.get-prototype-of.js");
var _lodash = _interopRequireDefault(require("lodash"));
var _Base = _interopRequireDefault(require("../base/Base"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function set(target, property, value, receiver) { if (typeof Reflect !== "undefined" && Reflect.set) { set = Reflect.set; } else { set = function set(target, property, value, receiver) { var base = _superPropBase(target, property); var desc; if (base) { desc = Object.getOwnPropertyDescriptor(base, property); if (desc.set) { desc.set.call(receiver, value); return true; } else if (!desc.writable) { return false; } } desc = Object.getOwnPropertyDescriptor(receiver, property); if (desc) { if (!desc.writable) { return false; } desc.value = value; Object.defineProperty(receiver, property, desc); } else { _defineProperty(receiver, property, value); } return true; }; } return set(target, property, value, receiver); }
function _set(target, property, value, receiver, isStrict) { var s = set(target, property, value, receiver || target); if (!s && isStrict) { throw new TypeError('failed to set property'); } return value; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
var CheckBoxComponent = /*#__PURE__*/function (_BaseComponent) {
  _inherits(CheckBoxComponent, _BaseComponent);
  var _super = _createSuper(CheckBoxComponent);
  function CheckBoxComponent() {
    _classCallCheck(this, CheckBoxComponent);
    return _super.apply(this, arguments);
  }
  _createClass(CheckBoxComponent, [{
    key: "defaultSchema",
    get: function get() {
      return CheckBoxComponent.schema();
    }
  }, {
    key: "defaultValue",
    get: function get() {
      return this.isRadioCheckbox ? '' : (this.component.defaultValue || false).toString() === 'true';
    }
  }, {
    key: "hasSetValue",
    get: function get() {
      return this.hasValue();
    }
  }, {
    key: "isRadioCheckbox",
    get: function get() {
      return this.component.inputType === 'radio';
    }
  }, {
    key: "getRadioGroupItems",
    value: function getRadioGroupItems() {
      var _this = this;
      if (!this.isRadioCheckbox) {
        return [];
      }
      return this.currentForm ? this.currentForm.getAllComponents().filter(function (c) {
        return c.isRadioCheckbox && c.component.name === _this.component.name;
      }) : [];
    }
  }, {
    key: "getRadioGroupValue",
    value: function getRadioGroupValue() {
      if (!this.isRadioCheckbox) {
        return null;
      }
      var selectedRadios = this.getRadioGroupItems().filter(function (c) {
        return c.input.checked;
      });
      return _lodash.default.get(selectedRadios, '[0].component.value');
    }
  }, {
    key: "elementInfo",
    value: function elementInfo() {
      var info = _get(_getPrototypeOf(CheckBoxComponent.prototype), "elementInfo", this).call(this);
      info.type = 'input';
      info.changeEvent = 'click';
      info.attr.type = this.component.inputType || 'checkbox';
      info.attr.class = 'form-check-input';
      if (this.component.name) {
        info.attr.name = "data[".concat(this.component.name, "][").concat(this.root.id, "]");
      }
      info.attr.value = this.component.value ? this.component.value : 0;
      return info;
    }
  }, {
    key: "build",
    value: function build() {
      // Refresh element info.
      this.info = this.elementInfo();
      if (this.viewOnly) {
        return this.viewOnlyBuild();
      }
      if (!this.component.input) {
        return;
      }
      this.createElement();
      this.input = this.createInput(this.element);
      this.createLabel(this.element, this.input);
      if (!this.labelElement) {
        this.addInput(this.input, this.element);
      }
      this.createDescription(this.element);
      this.restoreValue();
      if (this.shouldDisable) {
        this.forceDisabled = true;
      }
      this.autofocus();
      this.attachLogic();
    }
  }, {
    key: "emptyValue",
    get: function get() {
      return false;
    }
  }, {
    key: "isEmpty",
    value: function isEmpty(value) {
      return _get(_getPrototypeOf(CheckBoxComponent.prototype), "isEmpty", this).call(this, value) || value === false;
    }
  }, {
    key: "createElement",
    value: function createElement() {
      var className = "form-check ".concat(this.className);
      if (!this.labelIsHidden()) {
        className += " ".concat(this.component.inputType || 'checkbox');
      }

      // If the element is already created, don't recreate.
      if (this.element) {
        //update class for case when Logic changed container class (customClass)
        this.element.className = className;
        return this.element;
      }
      this.element = this.ce('div', {
        id: this.id,
        class: className
      });
      this.element.component = this;
    }
  }, {
    key: "labelOnTheTopOrLeft",
    value: function labelOnTheTopOrLeft() {
      return ['top', 'left'].includes(this.component.labelPosition);
    }
  }, {
    key: "labelOnTheTopOrBottom",
    value: function labelOnTheTopOrBottom() {
      return ['top', 'bottom'].includes(this.component.labelPosition);
    }
  }, {
    key: "setInputLabelStyle",
    value: function setInputLabelStyle(label) {
      if (this.component.labelPosition === 'left') {
        _lodash.default.assign(label.style, {
          textAlign: 'center',
          paddingLeft: 0
        });
      }
      if (this.labelOnTheTopOrBottom()) {
        _lodash.default.assign(label.style, {
          display: 'block',
          textAlign: 'center',
          paddingLeft: 0
        });
      }
    }
  }, {
    key: "setInputStyle",
    value: function setInputStyle(input) {
      if (!input) {
        return;
      }
      if (this.component.labelPosition === 'left') {
        _lodash.default.assign(input.style, {
          position: 'initial',
          marginLeft: '7px'
        });
      }
      if (this.labelOnTheTopOrBottom()) {
        _lodash.default.assign(input.style, {
          width: '100%',
          position: 'initial',
          marginLeft: 0
        });
      }
    }
  }, {
    key: "createLabel",
    value: function createLabel(container, input) {
      var isLabelHidden = this.labelIsHidden();
      var className = 'control-label form-check-label';
      if (this.component.input && !this.options.inputsOnly && this.component.validate && this.component.validate.required) {
        className += ' field-required';
      }
      this.labelElement = this.ce('label', {
        class: className
      });
      this.addShortcut();
      var labelOnTheTopOrOnTheLeft = this.labelOnTheTopOrLeft();
      if (!isLabelHidden) {
        // Create the SPAN around the textNode for better style hooks
        this.labelSpan = this.ce('span');
        if (this.info.attr.id) {
          this.labelElement.setAttribute('for', this.info.attr.id);
        }
      }
      if (!isLabelHidden && labelOnTheTopOrOnTheLeft) {
        this.setInputLabelStyle(this.labelElement);
        this.setInputStyle(input);
        if (this.options.builder) {
          this.labelSpan.appendChild(this.generateLabelElement(this.component.label, this.component.workingLabel));
        } else {
          this.labelSpan.appendChild(this.text(this.component.label));
        }
        this.labelElement.appendChild(this.labelSpan);
      }
      this.addInput(input, this.labelElement);
      if (!isLabelHidden && !labelOnTheTopOrOnTheLeft) {
        this.setInputLabelStyle(this.labelElement);
        this.setInputStyle(input);
        this.labelSpan.appendChild(this.generateLabelElement(this.addShortcutToLabel(), this.component.workingLabel));
        this.labelElement.appendChild(this.labelSpan);
      }
      this.createTooltip(this.labelElement);
      container.appendChild(this.labelElement);
    }
  }, {
    key: "createInput",
    value: function createInput(container) {
      if (!this.component.input) {
        return;
      }
      var inputId = this.id;
      if (this.options.row) {
        inputId += "-".concat(this.options.row);
      }
      inputId += "-".concat(this.root.id);
      this.info.attr.id = inputId;
      var input = this.ce(this.info.type, this.info.attr);
      this.errorContainer = container;
      return input;
    }
  }, {
    key: "dataValue",
    get: function get() {
      var getValue = _get(_getPrototypeOf(CheckBoxComponent.prototype), "dataValue", this);
      if (this.isRadioCheckbox) {
        _lodash.default.set(this.data, this.component.key, getValue === this.component.value);
      }
      return getValue;
    },
    set: function set(value) {
      var setValue = _set(_getPrototypeOf(CheckBoxComponent.prototype), "dataValue", value, this, true);
      if (this.isRadioCheckbox) {
        _lodash.default.set(this.data, this.component.key, setValue === this.component.value);
        this.setCheckedState(setValue);
      }
      return setValue;
    }
  }, {
    key: "key",
    get: function get() {
      return this.isRadioCheckbox && this.component.name ? this.component.name : _get(_getPrototypeOf(CheckBoxComponent.prototype), "key", this);
    }
  }, {
    key: "getValueAt",
    value: function getValueAt(index) {
      if (this.isRadioCheckbox) {
        return this.inputs[index].checked ? this.component.value : '';
      }
      return !!this.inputs[index].checked;
    }
  }, {
    key: "getValue",
    value: function getValue() {
      return _get(_getPrototypeOf(CheckBoxComponent.prototype), "getValue", this).call(this);
    }
  }, {
    key: "setCheckedState",
    value: function setCheckedState(value) {
      if (!this.input) {
        return;
      }
      if (this.isRadioCheckbox) {
        this.input.value = value === this.component.value ? this.component.value : 0;
        this.input.checked = value === this.component.value ? 1 : 0;
      } else if (value === 'on') {
        this.input.value = 1;
        this.input.checked = 1;
      } else if (value === 'off') {
        this.input.value = 0;
        this.input.checked = 0;
      } else if (value) {
        this.input.value = 1;
        this.input.checked = 1;
      } else {
        this.input.value = 0;
        this.input.checked = 0;
      }
      return value;
    }
  }, {
    key: "setValue",
    value: function setValue(value, flags) {
      flags = this.getFlags.apply(this, arguments);
      if (this.isRadioCheckbox && !value) {
        return;
      }
      this.setCheckedState(value);
      return this.updateValue(flags, value);
    }
  }, {
    key: "getView",
    value: function getView(value) {
      return value ? 'Yes' : 'No';
    }
  }, {
    key: "destroy",
    value: function destroy() {
      _get(_getPrototypeOf(CheckBoxComponent.prototype), "destroy", this).call(this);
      this.removeShortcut();
    }
  }, {
    key: "updateValue",
    value: function updateValue(flags, value) {
      var _this2 = this;
      if (!this.hasInput) {
        return false;
      }
      if (this.isRadioCheckbox) {
        if (value === undefined && this.input.checked) {
          // Force all siblings elements in radio group to unchecked
          this.getRadioGroupItems().filter(function (c) {
            return c !== _this2 && c.input.checked;
          }).forEach(function (c) {
            return c.input.checked = false;
          });
          value = this.component.value;
        } else {
          value = this.getRadioGroupValue();
        }
      } else if (flags && flags.modified && this.input.checked && value === undefined) {
        value = true;
      }
      var changed = _get(_getPrototypeOf(CheckBoxComponent.prototype), "updateValue", this).call(this, flags, value);
      if (this.input) {
        if (this.input.checked) {
          this.input.setAttribute('checked', true);
          this.addClass(this.element, 'checkbox-checked');
        } else {
          this.input.removeAttribute('checked');
          this.removeClass(this.element, 'checkbox-checked');
        }
      }
      return changed;
    }
  }, {
    key: "onAnyChanged",
    value: function onAnyChanged() {
      if (this.options.builder && this.labelElement && this.labelElement.children[1]) {
        this.labelElement.replaceChild(this.generateLabelElement(this.component.label, this.component.workingLabel), this.labelElement.children[1]);
      }
    }
  }], [{
    key: "schema",
    value: function schema() {
      for (var _len = arguments.length, extend = new Array(_len), _key = 0; _key < _len; _key++) {
        extend[_key] = arguments[_key];
      }
      return _Base.default.schema.apply(_Base.default, [{
        type: 'checkbox',
        inputType: 'checkbox',
        label: 'Checkbox',
        key: 'checkbox',
        dataGridLabel: true,
        labelPosition: 'right',
        value: '',
        name: ''
      }].concat(extend));
    }
  }, {
    key: "builderInfo",
    get: function get() {
      return {
        title: 'Checkbox',
        group: 'basic',
        icon: 'fa fa-check-square',
        documentation: 'http://help.form.io/userguide/#checkbox',
        weight: 50,
        schema: CheckBoxComponent.schema()
      };
    }
  }]);
  return CheckBoxComponent;
}(_Base.default);
exports.default = CheckBoxComponent;